from setuptools import setup

from _version import __version__

setup(
    name='angryplanner',
    version=__version__,
    description='',
    author="discovery",
    packages=['angryplanner', 'angryplanner.settings', 'nld', 'okta_auth', 'templates', 'templates/assets/css'],
    include_package_data=True,
    package_data={'templates': ['*'],
                  'templates/assets/css': ['*']},
    install_requires=['Django',
                      'django-debug-toolbar',
                      'django-admin-rangefilter',
                      'django-crispy-forms',
                      'django-filter',
                      'django-debug-toolbar',
                      'django-allauth',
                      'djangorestframework',
                      'django-viewflow',
                      'django-grappelli',
                      'django-material',
                      'django-storages',
                      'django-import-export',
                      'django-extensions',
                      'psycopg2-binary',
                      'python-jose',
                      'boto3',
                      'botocore',
                      'pycryptodome',
                      'oktaauth',
                      'uWSGI',
                      'channels',
                      'csvkit',
                      'pandas',
                      'jwt',
                      'requests',
                      'django-health-check',
                      'django-environ',
                      'psutil',
                      'newrelic',
                      ],

)
