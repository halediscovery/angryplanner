from django.urls import path, include

from . import views


urlpatterns = (
    # urls for Django Rest Framework API
    path('trs', views.OffRampTRS.as_view(), name='trs'),
    path('drs', views.OffRampDRS.as_view(), name='drs'),
)