import requests
from rest_framework import viewsets, permissions, views, response


class OffRampTRS(views.APIView):
    def get(self, request, format=None):
        headers = {"Authorization": "Bearer AwIooMwQmj3BW+E69S1GZ/mcxzd0Krno0vKSqWteAfihBM8q4RuJFh9zTkHAO+lT"}
        res = requests.get('https://discovery-uat.sdvi.com/api/v2/workflows', {
            "data":  {
                "type": "workflows",
                "attributes":  {
                        "initData" : "{\"submitter\": {\"id\": \"6884\", \"nonLinearType\": \"VOD\", \"nonLinearProfile\": \"M2TS\"}, \"supplyChainMetadata\": {\"mediaPreparationInfo\": {\"video\": {\"isVideoFileTranscoded\": false}, \"closedCaption\": {\"isSccFileGenerated\": false}, \"qcReports\": {\"isQCCompleted\": false, \"qcpassed\": false, \"qccompleted\": false}}}, \"assetMetadata\": {\"scrid\": \"3746812\", \"network\": \"Discovery\", \"orginatingBrand\": \"GAC\", \"trt\": \"00:20:00\"}, \"videoSpecs\": {\"transcodeConfigId\": \"2680f5f6-605d-11e9-8647-d663bd873d93\", \"transcodeConfigChecksum\": \"CUEPOINTS\", \"format\": \"HD\", \"brandName\": \"GAC\", \"stitchConfig\": {\"header\": [{\"sourceType\": \"black\", \"startTime\": \"00:00:00:00\", \"endTime\": \"00:00:03:00\", \"skipLastSegment\": false, \"add_iFrame\": {\"startTime\": \"00:00:00:00\", \"endTime\": \"00:00:01:00\"}}, {\"sourceType\": \"brandOpen\", \"startTime\": \"00:00:01:00\", \"endTime\": \"00:00:05:29\", \"skipLastSegment\": false}], \"footer\": {\"sourceType\": \"black\", \"startTime\": \"00:00:00:00\", \"endTime\": \"00:00:03:00\", \"skipLastSegment\": false, \"add_iFrame\": {\"startTime\": \"00:00:00:00\", \"endTime\": \"00:00:01:00\"}}, \"bumper\": null, \"trailer\": {\"sourceType\": \"black\", \"startTime\": \"00:00:00:00\", \"endTime\": \"00:00:01:00\", \"skipLastSegment\": true, \"add_iFrame\": {\"startTime\": \"00:00:00:00\", \"endTime\": \"00:00:01:00\"}}}, \"bugConfig\": {\"bugUri\": null, \"fadeIn\": 5, \"fadeOut\": 5, \"required\": false, \"brand\": \"GAC\"}, \"suggestedTranscode\": {\"profile\": \"30i\"}, \"webtossDetails\": null}, \"audioSpecs\": {\"tracks\": 1, \"bitDepth\": 16, \"bitRate\": 384000, \"sampleRate\": 48000}}"
                },
                "relationships": {
                    "movie": {
                        "data": {
                            "type": "movies",
                            "attributes": {
                                "name": "USKN_12670_178770_010_02_571_6425bdb8-ba31-4481-9ab0-1173ab51f06c",
                                "version": "latest"
                            }
                        }
                    },
                    "rule": {
                        "data": {
                            "type": "rules",
                            "attributes": {
                                "name": "NL R1000 - MP - Non Linear Media Preparation Workflow"
                            }
                        }
                    }
                }
            }
        }, headers=headers)
        return response.Response(res.content, headers={'Access-Control-Allow-Origin': '*'})


class OffRampDRS(views.APIView):
    def get(self, request, format=None):
        headers = {"Authorization": "Bearer AwIooMwQmj3BW+E69S1GZ/mcxzd0Krno0vKSqWteAfihBM8q4RuJFh9zTkHAO+lT"}
        res = requests.get('https://discovery-uat.sdvi.com/api/v2/workflows', {
            "data":  {
                "type": "workflows",
                "attributes":  {
                        "initData" : "{\"submitter\": {\"id\": \"6884\", \"nonLinearType\": \"VOD\", \"nonLinearProfile\": \"M2TS\"}, \"supplyChainMetadata\": {\"mediaPreparationInfo\": {\"video\": {\"isVideoFileTranscoded\": false}, \"closedCaption\": {\"isSccFileGenerated\": false}, \"qcReports\": {\"isQCCompleted\": false, \"qcpassed\": false, \"qccompleted\": false}}}, \"assetMetadata\": {\"scrid\": \"3746812\", \"network\": \"Discovery\", \"orginatingBrand\": \"GAC\", \"trt\": \"00:20:00\"}, \"videoSpecs\": {\"transcodeConfigId\": \"2680f5f6-605d-11e9-8647-d663bd873d93\", \"transcodeConfigChecksum\": \"CUEPOINTS\", \"format\": \"HD\", \"brandName\": \"GAC\", \"stitchConfig\": {\"header\": [{\"sourceType\": \"black\", \"startTime\": \"00:00:00:00\", \"endTime\": \"00:00:03:00\", \"skipLastSegment\": false, \"add_iFrame\": {\"startTime\": \"00:00:00:00\", \"endTime\": \"00:00:01:00\"}}, {\"sourceType\": \"brandOpen\", \"startTime\": \"00:00:01:00\", \"endTime\": \"00:00:05:29\", \"skipLastSegment\": false}], \"footer\": {\"sourceType\": \"black\", \"startTime\": \"00:00:00:00\", \"endTime\": \"00:00:03:00\", \"skipLastSegment\": false, \"add_iFrame\": {\"startTime\": \"00:00:00:00\", \"endTime\": \"00:00:01:00\"}}, \"bumper\": null, \"trailer\": {\"sourceType\": \"black\", \"startTime\": \"00:00:00:00\", \"endTime\": \"00:00:01:00\", \"skipLastSegment\": true, \"add_iFrame\": {\"startTime\": \"00:00:00:00\", \"endTime\": \"00:00:01:00\"}}}, \"bugConfig\": {\"bugUri\": null, \"fadeIn\": 5, \"fadeOut\": 5, \"required\": false, \"brand\": \"GAC\"}, \"suggestedTranscode\": {\"profile\": \"30i\"}, \"webtossDetails\": null}, \"audioSpecs\": {\"tracks\": 1, \"bitDepth\": 16, \"bitRate\": 384000, \"sampleRate\": 48000}}"
                },
                "relationships": {
                    "movie": {
                        "data": {
                            "type": "movies",
                            "attributes": {
                                "name": "USKN_12670_178770_010_02_571_6425bdb8-ba31-4481-9ab0-1173ab51f06c",
                                "version": "latest"
                            }
                        }
                    },
                    "rule": {
                        "data": {
                            "type": "rules",
                            "attributes": {
                                "name": "NL R1000 - MP - Non Linear Media Preparation Workflow"
                            }
                        }
                    }
                }
            }
        }, headers=headers)
        return response.Response(res.content, headers={'Access-Control-Allow-Origin': '*'})
