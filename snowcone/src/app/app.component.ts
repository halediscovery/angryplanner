import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'offramp';
  trsStatus: any;
  drsStatus: any;
  trsResponse: any;
  drsResponse: any;
  
  constructor(private http: HttpClient) { }

  createContent() {
    console.log('create content');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer AwIooMwQmj3BW+E69S1GZ/mcxzd0Krno0vKSqWteAfihBM8q4RuJFh9zTkHAO+lT'
      })
    };
    this.http.get('http://localhost:8000/offramp/trs').subscribe((res) => {
      console.log(res);
      this.trsStatus = 'active';
      this.trsResponse = res;
    }, 
    (err) => {
      console.log(err);
    });
  }

  deliverContent() {
    console.log('deliver content');
    this.http.get('http://localhost:8000/offramp/drs').subscribe((res) => {
      console.log(res);
      this.drsStatus = 'active';
      this.drsResponse = res;
    }, 
    (err) => {
      console.log(err);
    });
  }
}
