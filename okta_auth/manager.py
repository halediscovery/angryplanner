import requests
import urllib
import calendar
from datetime import datetime, timedelta
from jose import jwt, jws
from requests.auth import HTTPBasicAuth

from .constants import CLOCK_SKEW
from django.conf import settings


class OktaTokenManager(object):
    # stores and manages tokens coming from okta

    def __init__(self, *args, **kwargs):
        self.id_token = None
        self.access_token =  None
        self.claims = None

    def update_tokens(self, id_token, access_token, claims):
        self.id_token = id_token
        self.access_token = access_token
        self.claims = claims

    def get_json(self):
        response = {}
        if self.id_token:
            response['id_token'] = self.id_token

        if self.access_token:
            response['access_token'] = self.access_token

        if self.claims:
            response['claims'] = self.claims
        return response

    def get_tokens(self, url, code, okta_config):
        # Getting token from Okta
        auth = HTTPBasicAuth(okta_config['clientId'], okta_config['clientSecret'])
        header = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Connection': 'close'
        }

        # Query string to pass yakback test
        params = 'grant_type=authorization_code&code={}&redirect_uri={}'.format(
            urllib.parse.quote_plus(code),
            urllib.parse.quote_plus(okta_config['redirectUri'])
        )

        url_encoded = '{}{}'.format(url, params)

        # Send token request
        r = requests.post(url_encoded, auth=auth, headers=header)
        return r.json()

    def validate_token(self, tokens, okta_config, nonce):
        # Perform token validation
        try:
            jwks_with_public_key = self.fetch_jwk_for(tokens['id_token'])

            jwt_kwargs = {
                'algorithms': jwks_with_public_key['alg'],
                'options': {
                    'verify_at_hash': False,
                    # Used for leeway on the 'exp' claim
                    'leeway': CLOCK_SKEW
                },
                'issuer': okta_config['issuer'],
                'audience': okta_config['clientId']
            }

            if 'access_token' in tokens:
                jwt_kwargs['access_token'] = tokens['access_token']

            claims = jwt.decode(
                tokens['id_token'],
                jwks_with_public_key,
                **jwt_kwargs)

            if nonce != claims['nonce']:
                return 'invalid nonce', 401

            # Validate 'iat' claim
            plus_time_now_with_clock_skew = (datetime.utcnow() +
                                             timedelta(seconds=CLOCK_SKEW))
            plus_acceptable_iat = calendar.timegm(
                (plus_time_now_with_clock_skew).timetuple())

            if 'iat' in claims and claims['iat'] > plus_acceptable_iat:
                return 'invalid iat claim', 401

            return claims, 200

        except Exception as err:
            return str(err), 401

    def fetch_jwk_for(self, id_token=None):
        if id_token is None:
            raise NameError('id_token is required')
        unverified_header = jws.get_unverified_header(id_token)
        key_id = None

        if 'kid' in unverified_header:
            key_id = unverified_header['kid']
        else:
            raise ValueError('The id_token header must contain a "kid"')

        if key_id in settings.OKTA_GLOBAL_KEY_CACHE:
            return settings.OKTA_GLOBAL_KEY_CACHE[key_id]

        r = requests.get(settings.OKTA_JWKS_URL)
        jwks = r.json()

        for key in jwks['keys']:
            jwk_id = key['kid']
            settings.OKTA_GLOBAL_KEY_CACHE[jwk_id] = key

        if key_id in settings.OKTA_GLOBAL_KEY_CACHE:
            return settings.OKTA_GLOBAL_KEY_CACHE[key_id]
        else:
            raise RuntimeError('Unable to fetch public key from OKTA_JWKS_URL')

