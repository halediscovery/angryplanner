from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from .manager import OktaTokenManager

class Profile(models.Model):
    # Extended user model to save and use tokens
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    tokens = OktaTokenManager()


class UserValidator(object):
    def __init__(self, claims):
        self.claims = claims

    def validate_user(self):
        user = authenticate(
            username=self.claims['email'],
            password=self.claims['sub']
        )

        if user is None:
            # Creating a new User for Okta verified user
            new_user = User.objects.create_user(
                self.claims['email'],
                self.claims['email'],
                self.claims['sub']
            )
            new_user.is_staff = True
            new_user.is_superuser = True
            new_user.save()
            user = authenticate(
                username=self.claims['email'],
                password=self.claims['sub']
            )

        # Updating the user Profile model
        if not hasattr(user, 'profile'):
            profile = Profile()
            profile.user = user
            profile.save()
        return user