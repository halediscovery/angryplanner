from django.urls import path, include, re_path
from . import views

urlpatterns = [
    re_path(r'^authorization/callback', views.AuthView.as_view(), name='auth_view'),
    #re_path(r'^authorization/custom/login', views.OktaCustomLoginView.as_view(),
    #    name='okta_custom_login'),
    re_path(r'^authorization/custom/logout', views.OktaCustomLogoutView.as_view(),
        name='okta_custom_logout'),
    re_path(r'home/$', views.HomeView.as_view()),
    ]
