from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'okta_auth'
