import urllib
from django.shortcuts import render
from django.views import View
from django.http import HttpResponse
from django.conf import settings
from django.shortcuts import redirect
from django.views.generic.base import TemplateView
from django.contrib.auth.models import User
from django.contrib.auth import logout, login
from django.template.response import TemplateResponse

from.manager import OktaTokenManager
from .constants import NONCE, STATE, SCOPE, RESPONSE_TYPE
from .models import UserValidator

import json
# Create your views here.
class AuthView(View):
    def get(self, request):
        state = request.GET['state']
        if state != STATE:
            return HttpResponse('State mismatch',
                                status=401)
        if 'code' not in request.GET:
            return HttpResponse('Code was not returned', status=401)
        token_endpoint = settings.OKTA_TOKEN_URL
        token_manager = OktaTokenManager()
        tokens = token_manager.get_tokens(token_endpoint, request.GET['code'],
                                          settings.OKTA_OPENID_CONFIG['okta']['oidc_dev'])
        if not tokens:
            return HttpResponse('No tokens found', status=401)

        if 'id_token' not in tokens:
            # id_token was not returned
            return HttpResponse('No id_token in response from /token endpoint',
                                status=401)

        if tokens['id_token'] is None:
            # id_token is set to None
            return HttpResponse('id_token is None', status=401)
        message, status = token_manager.validate_token(tokens, settings.OKTA_OPENID_CONFIG['okta']['oidc_dev'], NONCE)
        if status == 401:
            return HttpResponse(message, status=status)
        claims = message
        user_validator = UserValidator(claims)
        user = user_validator.validate_user()
        if user is None:
            # Verifying if the user is authenticated or not
            return HttpResponse('No user object', status=401)
        user.profile.tokens.update_tokens(tokens['id_token'], tokens['access_token'], claims)
        # Logging the user in
        login(request, user)
        message = "{} is logged in now with okta token".format(request.user.email)
        return redirect("/admin/nld/")


class OktaCustomLoginView(TemplateView):
   pass


class OktaCustomLogoutView(View):
    def get(self, request):
        logout(request)
        url = settings.OKTA_OPENID_CONFIG['okta']['oidc_dev']['okta_url']
        return redirect(url)

class HomeView(View):
    def get(self, request):
        if request.user.is_authenticated:
            message = "{} is logged in now with okta token".format(request.user.email)
            return TemplateResponse(request, "temp_login.html", {'message': message})
        return TemplateResponse(request, "login-custom.html")