import requests
from xml.etree import ElementTree as ET
import pandas as pd
import sys, os
from argparse import ArgumentParser

def download_file_from_url(url):
    if not url:
        raise ValueError("url requried to download xml")
    response = requests.get(url, verify=False)
    if response.status_code == 200:
        data = response.text
        return data
    else:
        print("failed to download the xml from url %s"%url)

if __name__ == '__main__':
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.append(BASE_DIR)
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "angryplanner.settings.base")
    import django
    django.setup()
from nld.models import Plan


def make_dict_from_tree(element_tree):
    def internal_iter(tree, accum):
        if tree is None:
            return accum

        if tree.getchildren():
            accum[tree.tag] = {}
            for each in tree.getchildren():
                result = internal_iter(each, {})
                if each.tag in accum[tree.tag]:
                    if not isinstance(accum[tree.tag][each.tag], list):
                        accum[tree.tag][each.tag] = [
                            accum[tree.tag][each.tag]
                        ]
                    accum[tree.tag][each.tag].append(result[each.tag])
                else:
                    accum[tree.tag].update(result)
        else:
            accum[tree.tag] = tree.text
        return accum

    return internal_iter(element_tree, {})



def parse_xml(data):
    tree = ET.fromstring(data)
    xml_dict = make_dict_from_tree(tree)
    schedules = xml_dict.get('Network',{}).get('schedules', {}).get('Schedule', [])
    for schedule in schedules:
        show = schedule.get('show', {})

        plan_data = {}
        plan_data["pds_scheduledate"] = schedule.get('gmt','')
        plan_data["pds_seriestitle"] = show.get('programTitle','')
        plan_data["pds_title"] = show.get('episode',{}).get('episodeTitle','')
        property_id = plan_data["pds_property"] = show.get('propertyId','')
        episode = plan_data["pds_episode"] = show.get('episode',{}).get('episodeNumber','')
        premiere = schedule.get('networkEpisodePremiereFlag')
        propep_id = property_id.zfill(6) + '.' + episode.zfill(3)
        print(propep_id)
        propeps = Plan.objects.filter(propep=propep_id)
        if propeps:
            propeps.update(**plan_data)
        else:
            print("Skipping %s, property-episode doesn't exists" % (propep_id))
if __name__ == '__main__':

    parser = ArgumentParser()
    parser.add_argument('--f', help="Update planner from a local xml file", nargs="*", type=str, required=False)
    parser.add_argument('--url', help="Update planner from remote xml's", nargs="*", type=str, required=False)
    parser.add_argument('--networkid', help="Update planner from given networkid", nargs=1, type=str, required=False)

    args = parser.parse_args()
    if args.url:
        for url in args.url:
            data = download_file_from_url(url)
            if data:
                parse_xml(data)
    elif args.networkid:
        for networkid in args.networkid:
            url = "http://dsc.discovery.com/TVListingsWeb/ListerFeeds?networkAbbrev=" + networkid
            data = download_file_from_url(url)
            if data:
                parse_xml(data)
    elif args.f:
        for path in args.f:
            if os.path.isfile(path):
                with open(path) as f:
                    data = f.read()
                    parse_xml(data)
            else:
                print('Xml file %s does not exists' % path)