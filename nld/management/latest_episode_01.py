import os
from datetime import datetime
import traceback
import pandas as pd
import botocore, boto3
import logging, sys
from argparse import ArgumentParser
import tempfile, requests

# things to do
# * merge in blank overwrite that you have done this week
# remove all the old "updated metadata and added participant" checks
# cleanup Hale's silly code to fix xml source
# * fix the XML Source overwriting what is already there




if __name__ == '__main__':
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(BASE_DIR)
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "angryplanner.settings.base")
    import django
    django.setup()

from nld.models import Plan


def get_logger(file_name, name=""):
    """Return logger"""
    log_format = ('%(asctime)s- %(levelname)s - %(message)s')
    formatter = logging.Formatter(log_format)
    logger = logging.getLogger(name)
    hdlr = logging.FileHandler(file_name)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)
    return logger


latest_episode_file = 'latest_episode.csv'
time = datetime.now()
HH_MM = time.strftime('%H_%M')
# report_file = "logs/%s.log"%time
# t = get_logger(report_file)
# log_filename = "logs/"+"csv_import_%s.log" % HH_MM
# error_log_file = "logs/"+"csv_error_%s.log" % HH_MM
log_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)),"logs")
log_filename = os.path.join(log_dir,"csv_import_%s.log" % HH_MM)
error_log_file = os.path.join(log_dir,"csv_error_%s.log" % HH_MM)

if os.path.isfile(log_filename):
    os.remove(log_filename)
if os.path.isfile(error_log_file):
    os.remove(error_log_file)

logger = get_logger(log_filename, "log")
error_logger = get_logger(error_log_file, "error_logger")


def download_latest_episode(folder=None, dest_filename="latest_episode.csv"):
    """Downloads latest episode csv from given S3 folder"""
    global logger
    s3 = boto3.resource('s3',
                            aws_access_key_id='AKIAJLTYOTHXC7UKZBAQ',
                            aws_secret_access_key='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')
    BUCKET_NAME = 'dci-prod-librarian-packages-us-east-1'

    csv_bucket = s3.Bucket(BUCKET_NAME)

    if folder:
        key = folder
    else:
        s3object_list = []
        for objectsummary in csv_bucket.objects.filter():
            s3object = s3.Object(BUCKET_NAME, objectsummary.key)
            s3object_list.append((objectsummary.key, s3object.last_modified))
            s3object_list.sort(key=lambda tup: tup[1])

        key = s3object_list[-1][0]
    try:
         print("downloading %s from s3"%(key))
         s3.Bucket(BUCKET_NAME).download_file(key, dest_filename)
         logger.info("Latest episode downloaded sucessfully")
    except botocore.exceptions.ClientError as e:
        global error_logger
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            print("some other error")
        logger.error("Latest episode download failed")

def prepare_propep(propep=""):
    """ transforms given propep to required property.episode standards"""
    if not propep:
        return propep

    elif isinstance(propep, str):

        if "'" in propep:
            propep = propep.replace("'", "")

        if not propep:
            return propep
        property_episode = propep.split(".")
        if len(property_episode) >= 1:
            propep = property_episode[0] + "." + str(
                (property_episode[1] if len(property_episode) > 1 else "") + "000")[:3]
    return propep


def update_planner_from_csv(latest_episode_file="latest_episode.csv", source="Other", report=True):
    report_logger_file = "logs/%s.log" % source
    try:
        os.remove(report_logger_file)
    except FileNotFoundError:
        pass
    report_logger = get_logger(report_logger_file)

    # print(latest_episode_file)
    global logger
    global error_logger

    df = pd.read_csv(latest_episode_file, dtype=str, encoding="ISO-8859-1")
    
    propep_col = 'Prop ID Reference'
    time_format = '%m/%d/%Y'
    xml_source = ""


    csv_model_map = Plan.get_csv_model_map()
    model_date_fields = Plan.get_date_fields()
    model_boolean_fields = Plan.get_boolean_fields()
    participants = Plan.get_participants()
    update_fields = Plan.update_fields()
    
    update_date_fields = ['amazon_date', 'itunes_date', 'google_date', 'vudu_date', 'sony_date', 'microsoft_date',
                          'fandangonow_date', 'comcastest_date', 'mtod_date']
    
    df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
    df = df[df.columns.difference(['space', 'Property ID'])]
    df[propep_col] = df[propep_col].apply(prepare_propep)

    try:
        duplicated_propeps = set(df[df.duplicated(propep_col)][propep_col])
        if duplicated_propeps:
            report_logger.info("Duplicate propeps found in the csv are %s"%duplicated_propeps)
    except Exception as e:
        print(traceback.format_exc())

    df = df.drop_duplicates(subset=propep_col) # drop all duplicates

    for idx, row in df.iterrows():
        cur_row = dict(zip(df.columns, row))
        plan_data = {}
        propep = cur_row.get(propep_col, "")
        if propep and not pd.isna(propep):
            propeps = Plan.objects.filter(propep=propep)
            for col, val in cur_row.items():
                if pd.isna(val):
                    val = ''
                if col in csv_model_map:
                    model_key = csv_model_map[col]
                    if model_key in model_boolean_fields.keys():
                        if val:
                            if val in model_boolean_fields[model_key][1]:
                                plan_data[model_key] = True
                            elif val in model_boolean_fields[model_key][0]:
                                plan_data[model_key] = False
                            else:
                                plan_data[model_key] = False
                    elif model_key in model_date_fields:
                        if val:
                            try:
                                plan_data[model_key] = datetime.strptime(val, time_format).date()
                            except ValueError:
                                try:
                                    plan_data[model_key] = datetime.strptime(val, '%m/%d/%y').date()
                                except ValueError:
                                    logger.error("error in converting %s->%s to date format for %s" % (model_key, val, propep))
                                    # print(traceback.format_exc())
                                    print("error in converting %s->%s to date format for %s" % (model_key, val,propep))
                    else:
                        plan_data[model_key] = val
            for boolfield in model_boolean_fields.keys():
                if boolfield not in plan_data.keys():
                    plan_data[boolfield] = False
            if propeps:
                if len(propeps) > 1:
                    # error these into logfile
                    error_logger.error("duplicate `propep` found with value `%s` in Plan table." % (propep))
                else:
                    existing_propep = propeps[0]
                    is_data_updated = 0
                    is_sent_package = 0
                    is_already_shipped = 0
                    is_already_transfer_complete = True if getattr(existing_propep, 'transfer_complete') else False
                    plan_update_data = {}
                    for col, val in plan_data.items():
                        existing_val = getattr(existing_propep, col)
                        if isinstance(val, float):
                            val = str(val)
                        if col in update_date_fields:
                            if val:
                                is_sent_package = 1
                            if existing_val:
                                is_already_shipped = 1
                        if val != existing_val:
                            if col in participants and not existing_val:
                                plan_update_data['added_participant'] = True
                            if col in update_fields:
                                is_data_updated = 1
                            plan_update_data[col] = val
                    if is_data_updated:
                        # if plan_data.get("deliverable") == "New Source":
                        #     plan_update_data['playlist_posted'] = None
                        #     report_logger.info(
                        #         "%s has been  already shipped, but was updated from %s with a “New Source”" % (propep, source))
                        if plan_data.get('deliverable') == "Full PKG-Research":
                            # plan_update_data['deliverable'] = ""
                            pass
                        elif plan_data.get('deliverable') == "Full PKG-New Source":
                            pass
                        elif plan_data.get("deliverable") == "Avails Only":
                            report_logger.info(
                                "%s  was not imported because it is AvailsOnly and source=%s" % (
                                    propep,source))
                            continue
                        if source == 'AddModify':
                            for participant, participant_date in Plan.participants_datefileds().items():
                                if plan_data.get(participant):
                                    plan_update_data[participant_date] = None
                            plan_update_data['added_participant'] = False
                            plan_update_data['playlist_posted'] = None
                            plan_data['xml_source'] = "Partner Specific"
                            report_logger.info(
                                    "%s has been  already shipped, but was updated from %s" % (propep, source))
                        if not is_sent_package:
                            plan_update_data['added_participant'] = False
                        if plan_data.get("metadata_complete") and plan_data.get("metadata_complete") in model_boolean_fields.get('metadata_complete')[1]:
                            plan_update_data['metadata_complete'] = False
                        if is_sent_package:
                            plan_update_data['updated_metadata'] = True
                            for key, value in plan_update_data.items():
                                propeps[0].__setattr__(key, value)
                            try:
                                propeps[0].save()
                            except Exception as e:
                                report_logger.info("%s not imported because:\n%s" % (propep, traceback.format_exc()))
                            logger.info("Updated metadata on aleady sent %s" % propep)
                        else:
                            for key, value in plan_update_data.items():
                                propeps[0].__setattr__(key, value)
                            try:
                                propeps[0].save()
                            except Exception as e:
                                report_logger.info("%s not imported because:\n%s" % (propep, traceback.format_exc()))
                            logger.info("Updated metadata on %s" % propep)
                    else:
                        for key, value in plan_update_data.items():
                            propeps[0].__setattr__(key, value)
                        try:
                            propeps[0].save()
                        except Exception as e:
                            report_logger.info("%s not imported because:\n%s" % (propep, traceback.format_exc()))
                        if not is_already_shipped:
                            report_logger.info("%s wasn’t shipped and was updated from %s"%(propep, source))
                        if is_already_transfer_complete:
                            report_logger.info("%s has been already shipped, but was updated from %s with updated_metadata=%s and added_participant=%s "%(propep, source, plan_update_data.get("updated_metadata","None"),plan_update_data.get("added_participants","None") ))
                    print(plan_update_data)
            elif plan_data.get("deliverable") == "Avails Only" or plan_data.get("deliverable") == "Shared Asset Metadata":
                print(f"Rejected an Avails Only or Shared Asset Metadata on {plan_data['propep']}")
                continue
            else:
                if source == "NewToMarket":
                    plan_data['xml_source'] = "Multi Partner General"
                    plan_data['dml_action'] = "Full PKG-New Source"
                    print(f"Source is new to Market, dml action is Full PKG-New Source on {plan_data['propep']}")
                elif source == "AddModify":
                    plan_data['xml_source'] = "Partner Specific"
                    if plan_data['deliverable'] == "Full PKG-New Source":
                        print(f"Source is AddModify, Deliverable is Full PKG-New Source, DMPAction is Full PKG-New Source on {plan_data['propep']}")
                        plan_data['dml_action'] = "Full PKG-New Source"
                    elif plan_data['deliverable'] == "Full PKG-Research":
                        plan_data['dml_action'] = ""
                    else:
                        plan_data['dml_action'] = ""
                else:
                    plan_data['xml_source'] = ""
                plan_data['updated_metadata'] = False
                plan_data['added_participant'] = False
                try:
                    Plan.objects.create(**plan_data)
                    report_logger.info("Propep %s created from %s" % (propep, source))
                except Exception as e:
                    error_logger.error(traceback.format_exc())
                    report_logger.info("%s not imported because:\n%s" % (propep, traceback.format_exc()))
                # logger.info("Added propep=%s" % propep)
        else:
            report_logger.info(
                "skipping found invalid propep=%s " % (propep))


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--f',"--file", help="Update planner from a local csv file", nargs=1,type=str, required=False)
    parser.add_argument('--url',"--u", help="Update planner from remote csv's", nargs="*", type=str, required=False)
    parser.add_argument('--s3', help="Update planner from S3's CSVFiles-NewToMarket and CSVFiles-AddModify folders. Use `--s3 True`", nargs=1, type=bool, required=False)
    parser.add_argument("--addmodify", action="store_true")
    parser.add_argument("--newtomarket", action="store_true")

    args = parser.parse_args()
    if args.url:
        for url in args.url:
            try:
                response = requests.get(url, verify=False)
                print("downloaded csv from %s"%url)
                if response.status_code == 200:
                    fd, path = tempfile.mkstemp()
                    try:
                        with os.fdopen(fd, 'w+') as tmp:
                            tmp.write(response.text)
                        update_planner_from_csv(path, source="RemoteFileFromUrl")
                    finally:
                        os.remove(mlpath)
                else:
                    print("Unable to download csv from url=%s.\n%s\n%s"%(url,response.status_code,response.text))
            except:
                import traceback
                print(traceback.format_exc())
    elif args.f:
        if args.addmodify:
            update_planner_from_csv(args.f[0], source="AddModify")
        elif args.newtomarket:
            update_planner_from_csv(args.f[0], source="NewToMarket")
        else:
            update_planner_from_csv(args.f[0], source="LocalCsvFile")
    elif args.s3:
        download_latest_episode("CSVFiles-NewToMarket/episode-NewToMarket.csv", "episode-NewToMarket.csv")
        update_planner_from_csv("episode-NewToMarket.csv", source="NewToMarket")
        download_latest_episode("CSVFiles-AddModify/episode-addModify.csv", "episode-addModify.csv")
        update_planner_from_csv("episode-addModify.csv", source="AddModify")
    else:
        print(parser.print_help())