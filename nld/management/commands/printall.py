from django.core.management.base import BaseCommand, CommandError
from nld.models import Plan

class Command(BaseCommand):
    help = 'Generates a CSV for all records'

    def handle(self, *args, **options):
        for plan in Plan.objects.all().order_by('propep'):
            try:
                for key in plan.__dict__:
                    print(f"The value of {key} is {plan.__dict__[key]}")
                print(f"###")
            except Plan.DoesNotExist:
                raise CommandError(f'Plan "{plan_id}" does not exist')