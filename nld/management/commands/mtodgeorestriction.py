import csv
import datetime
from django.core.management.base import BaseCommand, CommandError
from nld.models import Plan, DCI_LOOKUP
from django.db.models import Q

class Command(BaseCommand):
    help = 'MTOD Geo Restriction'

    def handle(self, *args, **options):
        fields = [
            'propep',
            'asset_network',
            'asset_series',
            'c_seson_num',
            'container_position',
            'asset_episode',
            'linear_premiere',
            'delivery_date',
            'asset_rating',
            'c_ep_description',
            'kaltura_source_id',
            'delivery_date',
            'us_cox_pulldown',
            'us_cox_pulldown_start',
            'us_cox_pulldown_end',
            'uk_svod_holdback',
            'uk_svod_holdback_start',
            'uk_svod_holdback_end',
            'uk_avod_holdback',
            'uk_avod_holdback_start',
            'uk_avod_holdback_end',
            'africa_svod_holdback',
            'africa_svod_holdback_start',
            'africa_svod_holdback_end',
            'africa_avod_holdback',
            'africa_avod_holdback_start',
            'africa_avod_holdback_end',
            'australia_svod_holdback',
            'australia_svod_holdback_start',
            'australia_svod_holdback_end',
            'australia_avod_holdback',
            'australia_avod_holdback_start',
            'australia_avod_holdback_end',
            'austria_svod_holdback',
            'austria_svod_holdback_start',
            'austria_svod_holdback_end',
            'austria_avod_holdback',
            'austria_avod_holdback_start',
            'austria_avod_holdback_end',
            'france_svod_holdback',
            'france_svod_holdback_start',
            'france_svod_holdback_end',
            'france_avod_holdback',
            'france_avod_holdback_start',
            'france_avod_holdback_end',
            'germany_svod_holdback',
            'germany_svod_holdback_start',
            'germany_svod_holdback_end',
            'germany_avod_holdback',
            'germany_avod_holdback_start',
            'germany_avod_holdback_end',
            'italy_svod_holdback',
            'italy_svod_holdback_start',
            'italy_svod_holdback_end',
            'italy_avod_holdback',
            'italy_avod_holdback_start',
            'italy_avod_holdback_end',
            'middleeast_svod_holdback',
            'middleeast_svod_holdback_start',
            'middleeast_svod_holdback_end',
            'middleeast_avod_holdback',
            'middleeast_avod_holdback_start',
            'middleeast_avod_holdback_end',
            'poland_svod_holdback',
            'poland_svod_holdback_start',
            'poland_svod_holdback_end',
            'poland_avod_holdback',
            'poland_avod_holdback_start',
            'poland_avod_holdback_end',
            'spain_svod_holdback',
            'spain_svod_holdback_start',
            'spain_svod_holdback_end',
            'spain_avod_holdback',
            'spain_avod_holdback_start',
            'spain_avod_holdback_end',
            'switzerland_svod_holdback',
            'switzerland_svod_holdback_start',
            'switzerland_svod_holdback_end',
            'switzerland_avod_holdback',
            'switzerland_avod_holdback_start',
            'switzerland_avod_holdback_end',
            'mtod_publish_flag',
            'geo_block',
        ]

        TERRITORY_NAME_LOOKUP = {
            'us_cox_pulldown': 'US',
            'uk_svod_holdback': 'UK',
            'uk_avod_holdback': 'UK',
            'australia_svod_holdback': 'Australia',
            'australia_avod_holdback': 'Australia',
            'germany_svod_holdback': 'Germany',
            'germany_avod_holdback': 'Germany',
            'italy_svod_holdback': 'Italy',
            'italy_avod_holdback': 'Italy',
            'poland_svod_holdback': 'Poland',
            'poland_avod_holdback': 'Poland',
            'middleeast_svod_holdback': 'Middleeast',
            'middleeast_avod_holdback': 'Middleeast',
            'france_svod_holdback': 'France',
            'france_avod_holdback': 'France',
            'africa_svod_holdback': 'Africa',
            'africa_avod_holdback': 'Africa',
            'switzerland_svod_holdback': 'Switzerland',
            'switzerland_avod_holdback': 'Switzerland',
            'spain_svod_holdback': 'Spain',
            'spain_avod_holdback': 'Spain',
            'austria_svod_holdback': 'Austria',
            'austria_avod_holdback': 'Austria',

        }

        now = datetime.datetime.today()
        with open('mtodgeorestriction.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            header_printed = False
            for plan in Plan.objects.filter(mtod=True).only(*fields).order_by('propep'):
                data = []
                headers = []
                territories = set()
                for field in fields:
                    if not header_printed:
                        field_name = Plan._meta.get_field(field).verbose_name
                        if field_name == 'container position':
                            field_name = 'episode number'
                        headers.append(field_name)
                    if field in TERRITORY_NAME_LOOKUP.keys() and getattr(plan, field) in ['Restricted', 'Never Allow']:
                        territories.add(TERRITORY_NAME_LOOKUP[field])
                    data.append(getattr(plan, field))
                terr_str = ', '.join(sorted(territories))
                dci = DCI_LOOKUP.get(terr_str, '')
                data.insert(3, dci)
       
                if not header_printed:
                    headers.insert(3, 'DCI')
                    writer.writerow(headers)
                writer.writerow(data)
                header_printed = True