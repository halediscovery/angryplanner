from django.core.management.base import BaseCommand, CommandError

import psutil

class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument('--pid', nargs=1, type=str, required=True)

    def handle(self, *args, **options):
        pids = options.get("pid")
        if pids:
            pid = pids[0]
            try:
                pid = int(pid)
            except TypeError:
                print("conversion issue")
            if psutil.pid_exists(pid):
                proc = psutil.Process(pid)
                if proc and proc.name().startswith("python"):
                    print("killing process with pid=%s"%pid)
                    proc.kill()
                    print("killed process")
                else:
                    print("process %s not an python application."%proc.name())
            else:
                print("process not running with pid %s"%pid)
        else:
            raise ValueError("Pid not specified")