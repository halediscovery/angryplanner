from django.core.management.base import BaseCommand, CommandError
from nld.models import Plan

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        parser.add_argument('plan_id', nargs='+', type=int)

    def handle(self, *args, **options):
        for plan_id in options['plan_id']:
            self.stdout.write(self.style.SUCCESS(f"Successfully closed poll {plan_id}"))
