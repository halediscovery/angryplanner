from django.core.management.base import BaseCommand, CommandError

import multiprocessing
import sys, time
import os, subprocess
import shutil

if __name__ == '__main__':
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
    sys.path.append(BASE_DIR)
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "angryplanner.settings.base")
    import django
    django.setup()
from nld.management.latest_episode import update_planner_from_csv

class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument('--from', nargs=1, type=str, required=False)
        parser.add_argument('--to', nargs=1, type=str, required=False)
        parser.add_argument('--frequency', nargs=1, type=int, required=False)

    def handle(self, *args, **options):
        freq = options.get("frequency")[0] if options.get("frequency") else 1
        if options.get("from") and options.get('to'):
            from_path = options.get('from')[0]
            to_path = options.get('to')[0]
        else:
            from_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "dropfolder")
            to_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "donefolder")
        try:
            res = subprocess.Popen(['python', os.path.abspath(__file__), from_path, to_path, str(freq)],
                                   close_fds=True)
            self.stdout.write(self.style.SUCCESS('Daemon started with pid %s successfully' % res.pid))
        except:
            import traceback
            raise CommandError('Something went wrong.\n%s'%traceback.format_exc())

if __name__ == "__main__":

    from_path = sys.argv[1]
    to_path = sys.argv[2]
    freq = int(sys.argv[3])
    log_directory = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    while True:
        with open(os.path.join(log_directory, "result.log"),'a+') as log:
            if not os.path.isdir(from_path):
                os.makedirs(from_path)
            if not os.path.isdir(to_path):
                os.makedirs(to_path)
            for f in os.listdir(from_path):
                if f.endswith(".csv") or f.endswith(".xls"):
                    log.write("Processing %s started"%f)
                    update_planner_from_csv(latest_episode_file=os.path.join(from_path, f))
                    log.write("Processing  %s done" % f)
                    try:
                        shutil.move(os.path.join(from_path, f), os.path.join(to_path, f))
                    except shutil.Error as e:
                        import traceback
                        print(traceback.format_exc())
        time.sleep(freq)