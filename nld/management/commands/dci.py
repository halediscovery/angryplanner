import sys
from django.core.management.base import BaseCommand, CommandError
from nld.models import Plan

class Command(BaseCommand):
    help = 'Saves all records along with new geo block field'

    def handle(self, *args, **options):
        num = Plan.objects.count()
        print(num)
        i = 0
        for plan in Plan.objects.all():
            self.progressBar(i, num)
            plan.save()
            i += 1

    def progressBar(self, value, endvalue, bar_length=20):

        percent = float(value) / endvalue
        arrow = '-' * int(round(percent * bar_length)-1) + '>'
        spaces = ' ' * (bar_length - len(arrow))

        sys.stdout.write("\rDCI Field Calculation Progress: [{0}] {1}%".format(arrow + spaces, int(round(percent * 100))))
        sys.stdout.flush()