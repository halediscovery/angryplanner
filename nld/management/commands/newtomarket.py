from django.core.management.base import BaseCommand, CommandError
from nld.management.latest_episode import update_planner_from_csv, download_latest_episode

class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument('--file', nargs=1, type=str, required=False)

    def handle(self, *args, **options):
        if options.get("file"):
            try:
                update_planner_from_csv(latest_episode_file=options.get("file")[0])
            except:
                import traceback
                raise CommandError('Something went wrong \n%s'%traceback.format_exc())

            self.stdout.write(self.style.SUCCESS('Successful'))
        else:
            download_latest_episode("CSVFiles-NewToMarket/", "new_to_market_latest_episode.csv")
            update_planner_from_csv("new_to_market_latest_episode.csv")