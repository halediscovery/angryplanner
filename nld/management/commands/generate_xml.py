import os
import sys
import logging
import logzero
import untangle
from datetime import datetime
from jinja2 import Environment, FileSystemLoader
from django.core.management.base import BaseCommand, CommandError
from nld import models
from nld.xmlgen import XMLGen



class Command(BaseCommand):
    help = 'Generates a XML for all records'

    def add_arguments(self, parser):
        parser.add_argument('--fi', type=str, help='Django filter as json document, ie. {"hulu": true}')
        parser.add_argument('--partner', type=str, help='Participant [sony, apple, itunes, amazon, multi]')
        parser.add_argument('--templatepath', type=str, help='Template directory')
        parser.add_argument('--current_template', type=str, help='Template to use in templatepath')

    def handle(self, *args, **options):
        xmlgen = XMLGen(options)
        xmlgen.generate()