import os
from datetime import datetime
import traceback
import pandas as pd
import botocore, boto3
import logging, sys
from argparse import ArgumentParser
import tempfile, requests

# active version
# things to do
# * merge in blank overwrite that you have done this week
# remove all the old "updated metadata and added participant" checks
# cleanup Hale's silly code to fix xml source
# * fix the XML Source overwriting what is already there


if __name__ == '__main__':
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(BASE_DIR)
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "angryplanner.settings.dev")
    import django

    django.setup()

from nld.models import Plan


def get_logger(file_name, name=""):
    """Return logger"""
    log_format = ('%(asctime)s- %(levelname)s - %(message)s')
    formatter = logging.Formatter(log_format)
    logger = logging.getLogger(name)
    hdlr = logging.FileHandler(file_name)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)
    return logger


latest_episode_file = 'latest_episode.csv'
time = datetime.now()
HH_MM = time.strftime('%H_%M')
log_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "logs")
log_filename = os.path.join(log_dir, "csv_import_%s.log" % HH_MM)
error_log_file = os.path.join(log_dir, "csv_error_%s.log" % HH_MM)

if os.path.isfile(log_filename):
    os.remove(log_filename)
if os.path.isfile(error_log_file):
    os.remove(error_log_file)

logger = get_logger(log_filename, "log")
error_logger = get_logger(error_log_file, "error_logger")


def download_latest_episode(folder=None, dest_filename="latest_episode.csv"):
    """Downloads latest episode csv from given S3 folder"""
    global logger
    s3 = boto3.resource('s3',
                        aws_access_key_id='AKIAJLTYOTHXC7UKZBAQ',
                        aws_secret_access_key='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')
    BUCKET_NAME = 'dci-prod-librarian-packages-us-east-1'

    csv_bucket = s3.Bucket(BUCKET_NAME)

    if folder:
        key = folder
    else:
        s3object_list = []
        for objectsummary in csv_bucket.objects.filter():
            s3object = s3.Object(BUCKET_NAME, objectsummary.key)
            s3object_list.append((objectsummary.key, s3object.last_modified))
            s3object_list.sort(key=lambda tup: tup[1])

        key = s3object_list[-1][0]
    try:
        print("downloading %s from s3" % (key))
        s3.Bucket(BUCKET_NAME).download_file(key, dest_filename)
        logger.info("Latest episode downloaded sucessfully")
    except botocore.exceptions.ClientError as e:
        global error_logger
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            print("some other error")
        logger.error("Latest episode download failed")


def prepare_propep(propep=""):
    """ transforms given propep to required property.episode standards"""
    if not propep:
        return propep

    elif isinstance(propep, str):

        if "'" in propep:
            propep = propep.replace("'", "")

        if not propep:
            return propep
        property_episode = propep.split(".")
        if len(property_episode) >= 1:
            propep = property_episode[0] + "." + str(
                (property_episode[1] if len(property_episode) > 1 else "") + "000")[:3]
    return propep


def update_planner_from_csv(latest_episode_file="latest_episode.csv", source="Other", report=True):
    if not source in ["AddModify","NewToMarket"]:
        raise ValueError("Import Aborted: `source` must be either AddModify or NewToMarket but found %s."%source)
    report_logger_file = "logs/%s.log" % source
    try:
        os.remove(report_logger_file)
    except FileNotFoundError:
        pass
    report_logger = get_logger(report_logger_file)

    global logger
    global error_logger

    df = pd.read_csv(latest_episode_file, dtype=str, encoding="ISO-8859-1")

    propep_col = 'Prop ID Reference'
    time_format = '%m/%d/%Y'

    csv_model_map = Plan.get_csv_model_map()
    model_date_fields = Plan.get_date_fields()
    model_boolean_fields = Plan.get_boolean_fields()

    #logistic and status fields
    addmodify_non_overwrite_fields = ['source_status','source_res','source_framerate','caption_filename',
                                      'caption_status','playlist_posted','amazon_date','itunes_date','google_date',
                                      'vudu_date','sony_date','microsoft_date','fandangonow_date','comcastest_date',
                                      'mtod_date','kaleidescape_date','s3_archive_date','transfer_complete','notes']

    df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
    df = df[df.columns.difference(['space', 'Property ID'])]
    df[propep_col] = df[propep_col].apply(prepare_propep)

    try:
        duplicated_propeps = set(df[df.duplicated(propep_col)][propep_col])
        if duplicated_propeps:
            report_logger.info("Duplicate propeps found in the csv are %s" % duplicated_propeps)
    except Exception as e:
        print(traceback.format_exc())

    df = df.drop_duplicates(subset=propep_col)  #drop all duplicates

    for idx, row in df.iterrows():
        cur_row = dict(zip(df.columns, row))
        plan_data = {}
        propep = cur_row.get(propep_col, "")    #format propep
        if propep and not pd.isna(propep):
            propeps = Plan.objects.filter(propep=propep)
            for col, val in cur_row.items():
                if pd.isna(val):
                    val = ''
                if col in csv_model_map:
                    model_key = csv_model_map[col] #model_key is actual table column

                    #converts strings in csv to boolean based on logic defined in Plan.get_boolean_fields
                    if model_key in model_boolean_fields.keys():
                        if val in model_boolean_fields[model_key][1]:
                            plan_data[model_key] = True
                        elif val in model_boolean_fields[model_key][0]:
                            plan_data[model_key] = False
                        else:
                            plan_data[model_key] = False

                    #converts strings dates in csv to datetime object based on '%m/%d/%Y' or '%m/%d/%y'
                    elif model_key in model_date_fields:
                        if val:
                            try:
                                plan_data[model_key] = datetime.strptime(val, time_format).date()
                            except ValueError:
                                try:
                                    plan_data[model_key] = datetime.strptime(val, '%m/%d/%y').date()
                                except ValueError:
                                    logger.error(
                                        "error in converting %s->%s to date format for %s" % (model_key, val, propep))
                                    # print(traceback.format_exc())
                                    print("error in converting %s->%s to date format for %s" % (model_key, val, propep))

                        #if the date string in csv is empty overwrite date in planner with None
                        else:
                            plan_data[model_key] = None
                    else:
                        plan_data[model_key] = val

            #if deliverable is either availOnly or Shared Asset discard/skip the record
            if plan_data.get("deliverable") == "Avails Only" or plan_data.get("deliverable") == "Shared Asset Metadata":
                print("Rejected an Avails Only or Shared Asset Metadata on %s"%plan_data.get('propep'))
                continue

            #if source is newtomarket logic
            if source == "NewToMarket":
                plan_data['xml_source'] = "Multi Partner General"
                plan_data['dml_action'] = "Full PKG-New Source"
                #plan_data['dmp_action'] = "Full PKG-New Source"
                print("Source is new to Market, dml action and dmp action is 'Full PKG-New Source' on %s"%plan_data.get('propep',''))

            #if source is add/modify logic
            elif source == "AddModify":
                plan_data['xml_source'] = "Partner Specific"
                plan_data['dml_action'] = "Full PKG-Research"
                print("Source is Add/Modify, dml_action is 'Full PKG-Research' and dml_action is unchanged on %s"%plan_data.get('propep',''))
                #dmp action doesn't change if record exist

            #won't be the case because we already validated the source should be either AddModify or NewToMarket
            else:
                plan_data['xml_source'] = ""

            if propeps:
                if len(propeps) == 1:
                    existing_propep = propeps[0]
                    plan_update_data = {}
                    for col, val in plan_data.items():
                        existing_val = getattr(existing_propep, col)
                        if isinstance(val, float):
                            val = str(val)
                        if val != existing_val:
                            if source == "AddModify" and col in addmodify_non_overwrite_fields:
                                continue #don't overwrite addmodify_non_overwrite_fields when source is add/modify
                            plan_update_data[col] = val
                    if propeps[0].dml_action != "Full PKG-Research" and 'dml_action' in plan_update_data:
                        del plan_update_data['dml_action']
                    # Update the object with all the changes came in
                    for key, value in plan_update_data.items():
                        propeps[0].__setattr__(key, value)
                    try:
                        propeps[0].save()
                    except Exception as e:
                        report_logger.info("%s not imported because:\n%s" % (propep, traceback.format_exc()))
                    print(plan_update_data)
                    if source =="AddModify" and existing_propep.dml_action not in ["DMP Archive","DMP New Package"]:
                        plan_data['dml_action'] = "Full PKG-New Source"
                        #DMP action stay the same if it set to "DMP Archive" or "DMP New Package" when importing the modify csv


                #duplicate propeps found in planner
                else:
                    error_logger.error("duplicate `propep` found with value `%s` in Plan table." % (propep))

            #create new record as it is not present in planner
            else:

                # assign False to the boolean fields that are required for in models.py and not present in the csv-row
                for boolfield in model_boolean_fields.keys():
                    if boolfield not in plan_data.keys():
                        plan_data[boolfield] = False

                #reset dmp_action  if source is add/modify and record doesn't exists
                if source == "AddModify":
                    plan_data['dml_action'] = ""
                try:
                    Plan.objects.create(**plan_data)
                    report_logger.info("Propep %s created from %s" % (propep, source))
                except Exception as e:
                    error_logger.error(traceback.format_exc())
                    report_logger.info("%s not imported because:\n%s" % (propep, traceback.format_exc()))
        else:
            report_logger.info(
                "skipping found invalid propep=%s " % (propep))


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--f', "--file", help="Update planner from a local csv file", nargs=1, type=str, required=False)
    parser.add_argument('--url', "--u", help="Update planner from remote csv's", nargs="*", type=str, required=False)
    parser.add_argument('--s3',
                        help="Update planner from S3's CSVFiles-NewToMarket and CSVFiles-AddModify folders. Use `--s3 True`",
                        action='store_true', required=False)
    parser.add_argument("--addmodify", action="store_true")
    parser.add_argument("--newtomarket", action="store_true")

    args = parser.parse_args()
    if args.url:
        for url in args.url:
            try:
                response = requests.get(url, verify=False)
                print("downloaded csv from %s" % url)
                if response.status_code == 200:
                    fd, path = tempfile.mkstemp()
                    try:
                        with os.fdopen(fd, 'w+') as tmp:
                            tmp.write(response.text)
                        update_planner_from_csv(path, source="RemoteFileFromUrl")
                    finally:
                        os.remove(path)
                else:
                    print("Unable to download csv from url=%s.\n%s\n%s" % (url, response.status_code, response.text))
            except:
                import traceback

                print(traceback.format_exc())
    elif args.f:
        if args.addmodify:
            update_planner_from_csv(args.f[0], source="AddModify")
        elif args.newtomarket:
            update_planner_from_csv(args.f[0], source="NewToMarket")
        else:
            update_planner_from_csv(args.f[0], source="LocalCsvFile")
    elif args.s3:
        download_latest_episode("CSVFiles-NewToMarket/episode-NewToMarket.csv", "episode-NewToMarket.csv")
        update_planner_from_csv("episode-NewToMarket.csv", source="NewToMarket")
        download_latest_episode("CSVFiles-AddModify/episode-addModify.csv", "episode-addModify.csv")
        update_planner_from_csv("episode-addModify.csv", source="AddModify")
    else:
        print(parser.print_help())