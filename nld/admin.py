import csv
from django.shortcuts import render
from django.contrib import admin
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.admin.helpers import ActionForm
from django.contrib.admin import DateFieldListFilter
from django.contrib.admin.filters import SimpleListFilter, AllValuesFieldListFilter
from django.utils.translation import gettext_lazy as _
from rangefilter.filter import DateRangeFilter
from datetime import datetime, timedelta
from django.utils import timezone
from import_export.admin import ExportMixin, ImportExportMixin
from import_export import resources
from .models import Plan
from .adminfilters import (
    LogisticsFilter,
    TerritoriesQueryListFilter,
    CustomDateFieldListFilter,
    CustomQueryListFilter,
    FilterParticipants,
    FilterTerritories,
    custom_titled_filter,
    TERRITOR_LOOKUP,
    RestrictionsDateRange,
    RestrictionsUpdatedDateRange
)
from .adminresource import PlanResource
# from .forms import NameForm

logistics_default_values = {
            'transcoded':None,
            'video_quality_check':None,
            'cc_quality_check':None,
            'dumpster':None,
            'in_queue':None,
            'amazon_date':None,
            'itunes_date':None,
            'google_date':None,
            'vudu_date':None,
            'mtod_date':None,
            'sony_date':None,
            'microsoft_date':None,
            'fandangonow_date':None,
            'comcastest_date':None,
            'kaleidescape_date':None,
            'hulu_date':None,
            's3_archive_date':None,
             }

def reset_logistics_fields(modeladmin, request, queryset):
    if request.method == 'POST':
        queryset.update(**logistics_default_values)

def save(modeladmin, request, queryset):
    if request.method == 'POST':
        queryset.save()

ALL_TERRITORY_LOOKUP = (
    ('US', (_('us_cox_pulldown'),)),
    ('UK SVOD', (_('uk_svod_holdback'),)),
    ('UK AVOD', (_('uk_avod_holdback'),)),
    ('Australia SVOD', (_('australia_svod_holdback'),)),
    ('Australia AVOD', (_('australia_avod_holdback'),)),
    ('Germany SVOD', (_('germany_svod_holdback'),)),
    ('Germany AVOD', (_('germany_avod_holdback'),)),
    ('Italy SVOD', (_('italy_svod_holdback'),)),
    ('Italy AVOD', (_('italy_avod_holdback'),)),
    ('Poland SVOD', (_('poland_svod_holdback'),)),
    ('Poland AVOD', (_('poland_avod_holdback'),)),
    ('Middleeast SVOD', (_('middleeast_svod_holdback'),)),
    ('Middleeast AVOD', (_('middleeast_avod_holdback'),)),
    ('France SVOD', (_('france_svod_holdback'),)),
    ('France AVOD', (_('france_avod_holdback'),)),
    ('Africa SVOD', (_('africa_svod_holdback'),)),
    ('Africa AVOD', (_('africa_avod_holdback'),)),
    ('Switzerland SVOD', (_('switzerland_svod_holdback'),)),
    ('Switzerland AVOD', (_('switzerland_avod_holdback'),)),
    ('Spain SVOD', (_('spain_svod_holdback'),)),
    ('Spain AVOD', (_('spain_avod_holdback'),)),
    ('Austria SVOD', (_('austria_svod_holdback'),)),
    ('Austria AVOD', (_('austria_avod_holdback'),)),
)

def set_blank(modeladmin, request, queryset):
    if request.method == 'POST':
        if 'apply' in request.POST:
            if request.POST.get('territory'):
                territories = [x for x in ALL_TERRITORY_LOOKUP if x[0] == request.POST.get('territory')][0][1]
            else:
                territories = [x for x in ALL_TERRITORY_LOOKUP if x[0] == ALL_TERRITORY_LOOKUP[0][0]][0][1]
            fields = {}
            for territory in territories:
                fields[str(territory)] = ''
            fields_keys = fields.keys()
            for x in queryset.all():
                for field in list(fields_keys):
                    setattr(x, field, '')
                    setattr(x, field + '_start', None)
                    setattr(x, field + '_end', None)
                    x.save()
            modeladmin.message_user(request, "Changed status on {} propeps to blank".format(queryset.count()))
            return HttpResponseRedirect(request.get_full_path())
        return render(request,
            'nld/propep_intermediate.html',
            context={
                'propeps':queryset,
                'territories': [x[0] for x in ALL_TERRITORY_LOOKUP],
                'action': 'set_blank',
                'submit_text': 'Set blank'
            }
        )
set_blank.short_description = "Set blank"

def set_not_restricted(modeladmin, request, queryset):
    if request.method == 'POST':
        if 'apply' in request.POST:
            if request.POST.get('territory'):
                territories = [x for x in ALL_TERRITORY_LOOKUP if x[0] == request.POST.get('territory')][0][1]
            else:
                territories = [x for x in ALL_TERRITORY_LOOKUP if x[0] == ALL_TERRITORY_LOOKUP[0][0]][0][1]
            fields = {}
            for territory in territories:
                fields[str(territory)] = 'Not Restricted'
            fields_keys = fields.keys()
            for x in queryset.all():
                for field in list(fields_keys):
                    setattr(x, field, 'Not Restricted')
                    setattr(x, field + '_start', request.POST.get('start_date') or None)
                    setattr(x, field + '_end', request.POST.get('end_date') or None)
                    x.save()
            modeladmin.message_user(request, "Changed status on {} propeps to not restricted".format(queryset.count()))
            return HttpResponseRedirect(request.get_full_path())
        return render(request,
            'nld/propep_intermediate.html',
            context={
                'propeps':queryset,
                'territories': [x[0] for x in ALL_TERRITORY_LOOKUP],
                'action': 'set_not_restricted',
                'submit_text': 'Set not restricted'
            }
        )
set_not_restricted.short_description = "Set not restricted"

def set_never_allow(modeladmin, request, queryset):
    if request.method == 'POST':
        if 'apply' in request.POST:
            if request.POST.get('territory'):
                territories = [x for x in ALL_TERRITORY_LOOKUP if x[0] == request.POST.get('territory')][0][1]
            else:
                territories = [x for x in ALL_TERRITORY_LOOKUP if x[0] == ALL_TERRITORY_LOOKUP[0][0]][0][1]
            fields = {}
            for territory in territories:
                fields[str(territory)] = 'Never Allow'
            fields_keys = fields.keys()
            for x in queryset.all():
                for field in list(fields_keys):
                    setattr(x, field, 'Never Allow')
                    setattr(x, field + '_start', request.POST.get('start_date') or None)
                    setattr(x, field + '_end', request.POST.get('end_date') or None)
                    x.save()
            modeladmin.message_user(request, "Changed status on {} propeps to never allow".format(queryset.count()))
            return HttpResponseRedirect(request.get_full_path())
        return render(request,
            'nld/propep_intermediate.html',
            context={
                'propeps':queryset,
                'territories': [x[0] for x in ALL_TERRITORY_LOOKUP],
                'action': 'set_never_allow',
                'submit_text': 'Set never allow'
            }
        )
set_never_allow.short_description = "Set never allow"

def set_restricted(modeladmin, request, queryset):
    if request.method == 'POST':
        if 'apply' in request.POST:
            if request.POST.get('territory'):
                territories = [x for x in ALL_TERRITORY_LOOKUP if x[0] == request.POST.get('territory')][0][1]
            else:
                territories = [x for x in ALL_TERRITORY_LOOKUP if x[0] == ALL_TERRITORY_LOOKUP[0][0]][0][1]
            fields = {}
            for territory in territories:
                fields[str(territory)] = 'Restricted'
            fields_keys = fields.keys()
            for x in queryset.all():
                for field in list(fields_keys):
                    setattr(x, field, 'Restricted')
                    setattr(x, field + '_start', request.POST.get('start_date') or None)
                    setattr(x, field + '_end', request.POST.get('end_date') or None)
                    x.save()
            queryset.update(**fields)
            modeladmin.message_user(request, "Changed status on {} propeps to restricted".format(queryset.count()))
            return HttpResponseRedirect(request.get_full_path())
        return render(request,
            'nld/propep_intermediate.html',
            context={
                'propeps':queryset,
                'territories': [x[0] for x in ALL_TERRITORY_LOOKUP],
                'action': 'set_restricted',
                'submit_text': 'Set restricted'
            }
        )
set_restricted.short_description = "Set restricted"

class ExportMixinCustom(ExportMixin):
    change_list_template = 'nld/export_button.html'

class PlanAdmin(ExportMixinCustom, admin.ModelAdmin):
    #def has_add_permission(self, request):
    #    return False
    resource_class = PlanResource
    fieldsets = [('Type & Dates', {'fields':[('delivery_date'),
                                             ('deliverable', 'xml_source', 'dml_action'),
                                             ('c_xml_complete', 'c_hulu_ready', 'c_hulu_xml_complete')],'classes':['wide','extrapretty']}),
                 ('Source Information', {'fields':['source_filename',
                                          'subtitle_filename_jp',
                                          'subtitle_filename_nl',
                                          'subtitle_filename_sv',
                                          'subtitle_filename_no',
                                          'subtitle_filename_fi',
                                          'subtitle_filename_da',
                                          'source_res',
                                          'source_framerate',
                                          'source_status',
                                          'caption_filename',
                                          'caption_status',
                                          'playlist_posted',],'classes':['grp-collapse', 'wide','extrapretty']}),
                 ('Status ', {'fields':[('transcoded','video_quality_check', 'cc_quality_check'),
                                        ('dumpster','in_queue'),
                                        ('amazon_date','itunes_date','google_date'),
                                        ('vudu_date', 'mtod_date', 'sony_date'),
                                        ('microsoft_date','fandangonow_date','comcastest_date'),
                                        ('kaleidescape_date', 'hulu_date', 's3_archive_date'),
                                        ('transfer_complete')],'classes': [ 'wide','extrapretty']}),
                 ('Participants', {'fields':[('amazon', 'apple', 'google'),
                                             ('vudu', 'mtod', 'sony'),
                                             ('microsoft', 'fandangonow','comcastest',),('hulu', 'kaleidescape')],
                                             'classes': ['wide','extrapretty']}),
                 ('Asset', {'fields': [('c_asset_id165','c_amazon_id170','c_apple_id','c_sony_id176',),
                                       ('c_google_id','c_vudu_id','c_fandango_id','c_comcastest_id'),
                                       ('c_msoft_id193','c_verizon_id', 'c_google_g2id','c_vudu_v2', 'hulu_id',),
                                       ('kaltura_source_id',)],

                            'classes': ['wide','extrapretty']}),
                 ('Asset Information', {'fields': ['propep',
                                                   'asset_name',
                                                   'asset_series',
                                                   'asset_season',
                                                   'asset_episode',
                                                   'asset_network'],'classes': ['grp-collapse', 'collapse', 'wide', 'extrapretty']}),
                 ('Notes', {'fields': ['notes'], 'classes': ['wide','extrapretty']}),
                 ('Checksum', {'fields': ['video_filesize', 'video_checksum','scc_filesize','scc_checksum'],
                  'classes':['grp-collapse grp-closed','collapse', 'wide','extrapretty']}),
                 ]
    # action_form = MarkUpdatesForm
    # actions = [make_bulk_changes]
    empty_value_display = ''
    search_fields = ('asset_name', 'asset_network', 'propep', 'asset_series', 'asset_episode',
                     'deliverable', 'c_asset_id165','c_amazon_id170','c_apple_id','c_sony_id176',
                     'c_google_id','c_vudu_id','c_fandango_id','c_comcastest_id',
                     'c_msoft_id193', 'c_verizon_id', 'c_google_g2id', 'c_vudu_v2', 'hulu_id')
    list_display_links = ('delivery_date', 'propep',)
    list_display_links = ('delivery_date', 'propep',)
    readonly_fields = ['propep','xml_source', 'deliverable', 'delivery_date', 'c_asset_id165','c_amazon_id170',
                       'c_apple_id', 'c_sony_id176', 'amazon', 'apple', 'google', 'vudu', 'sony',
                       'microsoft', 'mtod', 'fandangonow', 'hulu', 'kaleidescape', 'comcastest', 'c_google_id', 'c_vudu_id',
                       'c_fandango_id', 'c_comcastest_id', 'c_msoft_id193', 'c_verizon_id',
                       'c_google_g2id', 'c_vudu_v2', 'asset_name', 'asset_series', 'asset_season',
                       'asset_episode', 'asset_network', 'hulu_id',
                       'c_xml_complete', 'metadata_complete','transfer_complete', 'kaltura_source_id',]
    actions = [reset_logistics_fields, save, set_restricted, set_not_restricted, set_never_allow, set_blank, 'export_as_csv']
    
    def response_change(self, request, obj):
        if "_reset-logistics" in request.POST:
            for key, value in logistics_default_values.items():
                obj.__setattr__(key, value)
                obj.save()
            self.message_user(request, "logistics reset done")
            return HttpResponseRedirect(".")
        return super().response_change(request, obj)
    
    def get_list_filter(self, request):
        for group in request.user.groups.all():
            if group.name.lower() == 'restrictions':
                return (
                    FilterTerritories,
                    TerritoriesQueryListFilter,
                    'c_hulu_ready', 
                    'c_hulu_xml_complete',
                    ('us_cox_pulldown_start', RestrictionsDateRange),
                    ('restrictions_update', RestrictionsUpdatedDateRange),)
        
        return (('delivery_date', DateRangeFilter),
                    'c_hulu_ready', 
                    'c_hulu_xml_complete',
                   ('us_cox_pulldown_start', RestrictionsDateRange),
                   ('restrictions_update', RestrictionsUpdatedDateRange),
                   CustomQueryListFilter,
                   FilterParticipants,
                   FilterTerritories,
                   TerritoriesQueryListFilter,
                   ('delivery_date', CustomDateFieldListFilter),
                   LogisticsFilter,
                   'geo_block',
                   ('asset_network', AllValuesFieldListFilter),
                    'hulu',
                    'comcastest',
                    'dml_action',
                    'deliverable',)

    
    def get_list_display(self, request):
        for group in request.user.groups.all():
            if group.name.lower() == 'restrictions':                    
                return ('delivery_date', 'propep', 'asset_series', 'c_seson_num', 'asset_episode',
                        'asset_network', 'kaltura_source_id', 'c_hulu_ready', 'c_hulu_xml_complete') # 'metadata_complete'
        
        return ('delivery_date', 'propep', 'deliverable','xml_source','dml_action', 'c_xml_complete', 'asset_series', 'asset_episode',
                    'asset_network', 'source_filename', 'source_status', 'caption_filename', 'caption_status', 'playlist_posted',
                    'transcoded', 'video_quality_check', 'cc_quality_check', 'in_queue', 'transfer_complete', 'c_hulu_ready', 'c_hulu_xml_complete') # 'metadata_complete'

    def change_view(self, request, object_id, form_url='', extra_context=None):
        return super().change_view(
            request,
            object_id,
            form_url,
            extra_context
        )

    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        field_names = []
        field_names.extend(self.get_list_display(request))

        for fields in self.get_fieldsets(request):
            for field in fields[1]['fields']:
                if isinstance(field, tuple):
                    for x in field:
                        if x not in field_names:
                            field_names.append(x)
                else:
                    field_names.append(field)

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])

        return response
    export_as_csv.short_description = "Export Selected"


from .adminmanager import ManagerPlan, ManagerPlanAdmin
from .adminrestriction import RestrictionPlan, RestrictionPlanAdmin
from .adminscheduler import SchedulerPlan, SchedulerPlanAdmin

# register for the admin
admin.site.register(Plan, PlanAdmin)
admin.site.register(RestrictionPlan, RestrictionPlanAdmin)
admin.site.register(ManagerPlan, ManagerPlanAdmin)
admin.site.register(SchedulerPlan, SchedulerPlanAdmin)