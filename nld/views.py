from django.views.generic import DetailView, ListView, UpdateView, CreateView
from .models import Plan
from .forms import PlanForm
from django.http import HttpResponse
from .xmlgen import XMLGen


class PlanListView(ListView):
    model = Plan


class PlanCreateView(CreateView):
    model = Plan
    form_class = PlanForm


class PlanDetailView(DetailView):
    model = Plan


class PlanUpdateView(UpdateView):
    model = Plan
    form_class = PlanForm


def ExcelExport(request):
    return HttpResponse("Hello, world. You're at the excel export")

def ema_xml(request, planner_id):
    return HttpResponse("ema XML for %s." % planner_id)

def apple_xml(request, planner_id):
    xmlg = XMLGen({'fi': 'apple'})
    xmlg.generate()
    return HttpResponse("apple XML for %s." % planner_id)
