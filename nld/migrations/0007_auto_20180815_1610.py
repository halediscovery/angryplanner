# Generated by Django 2.1 on 2018-08-15 16:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nld', '0006_auto_20180815_1606'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plan',
            name='asset_network',
            field=models.CharField(blank=True, choices=[('TLC', 'TLC'), ('ID', 'ID'), ('DSC', 'DSC'), ('DFC', 'DFC'), ('VEL', 'VEL'), ('AHC', 'AHC'), ('DAM', 'DAM'), ('SCI', 'SCI'), ('APL', 'APL')], max_length=100, verbose_name='asset host network'),
        ),
        migrations.AlterField(
            model_name='plan',
            name='container_position',
            field=models.CharField(max_length=100, verbose_name='container position'),
        ),
        migrations.AlterField(
            model_name='plan',
            name='source_res',
            field=models.CharField(blank=True, choices=[('HD', 'HD'), ('SD-Fullscreen', 'SD-Fullscreen'), ('SD-Letterbox', 'SD-Letterbox')], max_length=100, verbose_name='source resolution'),
        ),
    ]
