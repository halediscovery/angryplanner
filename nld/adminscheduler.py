from django.shortcuts import render
from django.contrib import admin
from django import forms
from django.http import HttpResponseRedirect
from django.contrib.admin.helpers import ActionForm
from django.contrib.admin import DateFieldListFilter
from django.contrib.admin.filters import SimpleListFilter, AllValuesFieldListFilter
from django.utils.translation import gettext_lazy as _
from rangefilter.filter import DateRangeFilter
from datetime import datetime, timedelta
from django.utils import timezone
from import_export.admin import ExportMixin, ImportExportMixin
from import_export import resources
from .models import Plan
from .admin import PlanAdmin
from .adminfilters import LogisticsFilter, CustomDateFieldListFilter, CustomQueryListFilter
from .adminresource import PlanResource


class SchedulerPlan(Plan):
    # actions = [update_mtod_fields]
    class Meta:
        verbose_name = "Scheduler Plan"
        verbose_name_plural = "Scheduler Planner"
        proxy = True


class SchedulerPlanAdmin(PlanAdmin):
    #def get_queryset(self, request):
    #    return self.model.objects.filter(mtod = True)
    list_display_links = ('propep',)
    search_fields = ('propep', 'deliverable','xml_source','dml_action', 'c_xml_complete', 'asset_series','asset_season', 'asset_episode','asset_network', 'created', 'last_updated')
    fieldsets = [('Basics', {'fields':[('propep',
                    'deliverable', 'xml_source',
                    'dml_action', 'c_xml_complete',
                    'asset_series', 'asset_season',
                    'asset_episode', 'asset_network',
                    'created', 'last_updated')],
                  'classes': ['grp-collapse', 'wide', 'extrapretty']}),
                 ('Notes', {'fields': ['notes'],
                            'classes': ['wide','extrapretty']}),]
    readonly_fields = ['propep', 'created', 'last_updated']
    #action_form = MarkUpdatesForm
    # actions = []
    # actions = [update_mtod_fields]
    def get_list_display(self, request):
        return ('propep',
            'deliverable', 
            'xml_source',
            'dml_action',
            'c_xml_complete',
            'asset_series',
            'asset_season',
            'asset_episode',
            'asset_network',
            'created',
            'last_updated')