import logzero
import untangle
import babel
import logging
import os
import errno
import glob
import json
from datetime import datetime
from datetime import datetime
from jinja2 import Environment, FileSystemLoader
from nld.models import Plan

class XMLGen(object):

    CONFIG = {
        'amazon_addModify_template_name': 'amazon-addModify.xml',
        'sony_addModify_template_name': 'sony-addModify.xml',
        'multi_addModify_template_name': 'multiproviders-addModify.xml',
        'apple_addModify_template_name': 'apple-locale-changes-addModify.xml',
        'hulu_addModify_template_name': 'hulu-addModify.xml',

        'addModify_location': '\\\\dss-isi-t\\MTPO_Transfer\\svc_vod\\XML\\From_GXG\\XML\\',

        'amazon_addModify_location': '\\\\dss-isi-t\\MTPO_Transfer\\svc_vod\\XML\\From_GXG\\XML\\Amazon\\',
        'sony_addModify_location': '\\\\dss-isi-t\\MTPO_Transfer\\svc_vod\\XML\\From_GXG\\XML\\Sony',
        'multi_addModify_location': '\\\\dss-isi-t\\MTPO_Transfer\\svc_vod\\XML\\From_GXG\\XML\\Multiproviders\\',
        'apple_addModify_location': '\\\\dss-isi-t\\MTPO_Transfer\\svc_vod\\XML\\From_GXG\\XML\\Itunes Only\\',
        'google_addModify_location': '\\\\dss-isi-t\\MTPO_Transfer\\svc_vod\\XML\\From_GXG\\XML\\Google_CSV\\',
        'apple_redelivery_location': '\\\\dss-isi-t\\MTPO_Transfer\\svc_vod\\XML\\From_GXG\\XML\\Multiproviders\\Apple_ReplaceLocale',
        'hulu_addModify_location': '.',

        'csv_addModify_location': '\\\\dss-isi-t\\MTPO_Transfer\\svc_vod\XML\\From_GXG\\XML\\CSV Files'
    }

    def __init__(self, options):
        self.options = options

    def hulu_datetime(self, value, format='medium'):
        if format == 'full':
            format="EEEE, d. MMMM y 'at' HH:mm"
        elif format == 'medium':
            format="EE dd.MM.y HH:mm"
        return babel.dates.format_datetime(value, format)

    def datetimeformat(self, value, sourceformat='%m/%d/%Y', outputformat='%H:%M / %d-%m-%Y'):
        if value == '':
            self.logger.info('You are missing a date that needed to be formatted')
            value='01/01/1901'
        date_time_obj = datetime.strptime(value, sourceformat)
        return date_time_obj.strftime(outputformat)

    def addtovalue(self, value, text_to_add='Added-this-'):
        return text_to_add + value

    def replacevalue(self, value):
        return 'Replaced Value'

    def centerpoint(self, value, pathtocenterpoints='data/centerpoints/'):
        # return back segment data
        filename = value.replace('.', '_') + '.ply'
        centerpointfile = pathtocenterpoints + filename
        self.logger.debug(f'the centerpointfile for {value} is {centerpointfile}')
        with open(centerpointfile, 'r') as o:
            o = untangle.parse(centerpointfile)
            self.logger.debug(f"for {centerpointfile} the data is {o.segments.cdata}")
            return o.segments.cdata

    def investigate(self, current_template, templatepath, debug, verbose):
        self.logger.info("Investigate")
        env = Environment(loader=FileSystemLoader('templates'))
        hulutemplate = env.get_template('hulu_sample.xml')
        env.lex(hulutemplate)

    def collectcenterpoint(self, centerpath, centerpointoutfile, debug, verbose):
        self.logger.info('Combining Centerpoints into something usable...')
        self.logger.debug(f"centerpoint path is {centerpath} and outputfile is {centerpointoutfile}")
        with open(centerpointoutfile, 'w') as outputfile:
            outputfile.write('"packageid","file_segments"\n')
            for centerpointfile in glob.glob(centerpath + "/*"):
                o = untangle.parse(centerpointfile)
                outputfile.write('"' + centerpointfile.split('\\')[1].split('.')[0].replace('_', '.') + '","' + o.segments.cdata + '"\n')
                self.logger.debug(f"for {centerpointfile} the data is {o.segments.cdata}")


    def chronicle(self, chronpath, chronicleoutfile, debug, verbose):
        self.logger.info('Combining the chronicle files into something usable...')
        self.logger.debug(f"chronpath is {chronpath} and output file is {chronicleoutfile}")

    def generate(self):
        # current_template, templatepath, datafile, datapath, namefield, outputpath, debug, verbose
        self.logger = logging.getLogger()
        templatepath = self.options.get('templatepath') or self.options.get('tp', os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), *['nld', 'management', 'commands', 'templates']))
        current_template = self.options.get('current_template') or self.options.get('t', self.CONFIG[self.options['partner'] + '_addModify_template_name'])
        outputpath = self.options.get('outputpath') or self.options.get('op', self.CONFIG[self.options['partner'] + '_addModify_location'])
        namefield = self.options.get('namefield') or self.options.get('n', 'propep')

        obj = json.loads(self.options.get('fi', '{}'))
        collection = Plan.objects.filter(**obj)
        verbose = 3
        logger = logzero.setup_logger(logfile="xml_generate.log", level=logging.DEBUG)

        env = Environment(loader=FileSystemLoader(templatepath))
        env.filters['datetimeformat'] = self.datetimeformat
        env.filters['addtovalue'] = self.addtovalue
        env.filters['replacevalue'] = self.replacevalue
        env.filters['centerpoint'] = self.centerpoint
        output_template = env.get_template(current_template)

        for row in collection:
            row_dict = row.__dict__
            for key, value in row_dict.items():
                if value=="":
                    logger.info(f'For {row_dict[namefield]} there is missing data for {key}')
            if verbose==3: logger.debug(row_dict)

            output_from_parsed_template = output_template.render(metadata=row_dict)
            # to save the results
            filename = outputpath + '/' + current_template[:-4] +'_' + datetime.now().strftime('%Y%m%d_%H%M') + '/' + row_dict[namefield] + current_template[-4:]  
            if not os.path.exists(os.path.dirname(filename)):
                try:
                    os.makedirs(os.path.dirname(filename))
                except OSError as exc: # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise
            with open(filename, "w", encoding="utf-8") as fh:
                logger.debug(f"Creating {filename}")
                fh.write(output_from_parsed_template)
            
            row.c_hulu_xml_complete = True
            row.save()