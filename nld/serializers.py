from . import models

from rest_framework import serializers


class PlanSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Plan
        fields = (
            'slug', 
            'name', 
            'created', 
            'last_updated', 
        )


