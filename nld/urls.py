from django.urls import path, include
from rest_framework import routers

from . import api
from . import views

router = routers.DefaultRouter()
router.register(r'plan', api.PlanViewSet)


urlpatterns = (
    # urls for Django Rest Framework API
    path('api/v1/', include(router.urls)),
    path('api/v1/plans', api.ListPlans.as_view(), name='nld_plan_list_api'),
)

urlpatterns += (
    # urls for Plan
    path('nld/plan/', views.PlanListView.as_view(), name='nld_plan_list'),
    path('nld/plan/create/', views.PlanCreateView.as_view(), name='nld_plan_create'),
    path('nld/plan/detail/<slug:slug>/', views.PlanDetailView.as_view(), name='nld_plan_detail'),
    path('nld/plan/update/<slug:slug>/', views.PlanUpdateView.as_view(), name='nld_plan_update'),
    path('nld/plan/export', views.ExcelExport, name='ExcelExport'),
    path('nld/plan/apple-xml/<planner_id>/', views.apple_xml, name="apple-xml"),
)

