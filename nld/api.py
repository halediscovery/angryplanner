from . import models
from . import serializers
from rest_framework import viewsets, permissions, views, response


class PlanViewSet(viewsets.ModelViewSet):
    """ViewSet for the Plan class"""

    queryset = models.Plan.objects.all()
    serializer_class = serializers.PlanSerializer
    permission_classes = [permissions.IsAuthenticated]

class ListPlans(views.APIView):
    def get(self, request, format=None):
        res = models.Plan.objects.filter(**request.query_params.items()).values()
        return response.Response(res)

    def post(self, request, format=None):
        models.Plan.objects.create(**request.data.items())
        return response.Response('ok')

    def put(self, request, format=None):
        try:
            plan = models.Plan.objects.filter(id=request.data.get('id'))
            plan.update(**request.data)
            return response.Response('ok')
        except Exception as e:
            return response.Response(e, 404)
