from django.db import models
from datetime import datetime

# Create your models here.
DCI_LOOKUP = {
    'Africa, Australia, Austria, France, Italy, Poland, Spain, Switzerland': 'DCI 0',
    'Africa, Australia, France, Middle East, Poland, Spain, UK': 'DCI 1',
    'Africa, Australia, France, Middle East, Poland, UK': 'DCI 2',
    'Australia, France, Middle East, Poland, UK': 'DCI 3',
    'Africa, Australia, Austria, Middle East, Poland, Spain, Switzerland, UK': 'DCI 4',
    'Africa, Australia, Middle East, Poland, Spain, UK': 'DCI 5',
    'Africa, Australia, Middle East, Poland, UK': 'DCI 6',
    'Australia, Middle East, Poland, UK': 'DCI 7',
    'Africa, Australia, France, Poland, UK': 'DCI 8',
    'Australia, France, Poland, Spain, UK': 'DCI 9',
    'Australia, France, Poland, UK': 'DCI 10',
    'Africa, Australia, Poland, UK': 'DCI 11',
    'Australia, Poland, UK': 'DCI 12',
    'Africa, Australia, France, Middle East, Poland, Spain': 'DCI 13',
    'Africa, Australia, France, Middle East, Poland': 'DCI 14',
    'Africa, Australia, Austria, Middle East, Poland, Spain, Switzerland': 'DCI 15',
    'Africa, Australia, Middle East, Poland, Spain': 'DCI 16',
    'Africa, Australia, Middle East, Poland': 'DCI 17',
    'Africa, Australia, France, Middle East, Poland, Spain': 'DCI 19',
    'Africa, Australia, Canada, France, Middle East, Poland, Spain, UK': 'DCI 20',
    'Africa, Australia, Canada, France, Middle East, Poland, Spain': 'DCI 21',
    'Africa, Australia, Canada, France, Germany, Middle East, Poland, Spain': 'DCI 22',
    'Australia, Poland, France, Spain': 'DCI 23',
    'Africa, Australia, France, Italy, Middle East, Poland, UK': 'DCI 24',
    'Africa, Australia, France, Italy, Middle East, Poland, Spain': 'DCI 25',
    'Australia, Austria, France, Italy, Poland, Spain, Switzerland, UK': 'DCI 26',
    'Africa, Australia, Austria, France, Middle East, Poland, Spain, Switzerland, UK': 'DCI 27',
    'Australia, Austria, France, Middle East, Poland, Spain, Switzerland, UK': 'DCI 28',
    'Africa, Australia, Austria, Italy, Middle East, Poland, Spain': 'DCI 29',
    'Africa, Australia, France, Italy, Middle East, Poland, Spain, UK': 'DCI 30',
    'Africa, Australia, Austria, France, Italy, Middle East, Poland, Spain, Switzerland, UK': 'DCI 31',
    'Australia, Austria, France, Poland, Spain, Switzerland, UK': 'DCI 32',
    'Africa, Australia, Austria, France, Germany, Italy, Middle East, Poland, Spain, Switzerland, UK': 'DCI 33',
    'France': 'DCI 34'
}

class Plan(models.Model):
    class Meta:
        verbose_name = "Deliverable"
        verbose_name_plural = "Operations and Logistics Planner"

    OFFER_TYPES         = (('AVOD', 'AVOD'),('TVOD', 'TVOD'),('SVOD', 'SVOD'),('DLTO', 'DLTO'),)  # librarian
    offer_type          = models.CharField('NLD type', max_length=100, choices=OFFER_TYPES, default='DLTO') # librarian

    # going to become deliverable
    # deliverable used to be called action - don't import avail only
    deliverable_TYPES   = (('Full PKG-New Source', 'Full PKG-New Source'),  # would be this for all NewToMarket
                           ('Full PKG-Research', 'Full PKG-Research'),  # this might be for either NewToMarket or AddModify
                           ('', '----------'),)  # blank means do some research 
    deliverable         = models.CharField('Deliverable', max_length=1000, default='')  # Deliverable

    DML_ACTION_TYPES    = (('Full PKG-New Source', 'Full PKG-New Source'),
                           ('Full PKG-Research', 'Full PKG-Research'),
                           ('Full PKG-Archive', 'Full PKG-Archive'),
                           ('', '----------'),) # blank means unset
    dml_action          = models.CharField('DMP Action', max_length=1000, choices=DML_ACTION_TYPES, default='')  # librarian 

    metadata_complete   = models.BooleanField('metadata complete', blank=True, default=False) # librarian
    updated_metadata    = models.BooleanField('updated metadata', blank=True, default=False) # librarian
    added_participant   = models.BooleanField('added participant', blank=True, default=False) # librarian

    # based on CSV source of the data
    XML_SOURCE_CHOICES  = (('Multi Partner General', 'Multi Partner General'),  # if from NewToMarket tab
                           ('Partner Specific', 'Partner Specific'),  # if from AddModify tab
                           ('', '--------'),)  # if from anywhere else (all the other methods drop folder, etc)
    xml_source          = models.CharField('XML Source', max_length=1000, default='')
    hulu_xml_gen        = models.BooleanField('Hulu XML Generated', blank=True, default=False) # librarian
    ema_xml_gen        = models.BooleanField('Amazon XML Generated', blank=True, default=False) # librarian
    itun_xml_gen        = models.BooleanField('iTunes XML Generated', blank=True, default=False) # librarian

    #dates
    delivery_date       = models.DateField('Due to be Shipped', blank=True, null=True)  # Due to be Shipped
    market_start_date   = models.DateField('date available to the market', blank=True, null=True) # DLTO Start
    linear_premiere     = models.DateField('Linear Premiere', blank=True, null=True) # Linear Premiere
    apple_start_date    = models.DateField('date available on Apple', blank=True, null=True) # Apple Start

    #asset information
    propep              = models.CharField('property.episode', max_length=1000, blank=True) # Prop ID Reference
    property_id         = models.CharField('Property ID', max_length=1000, blank=True) # Property ID
    asset_name          = models.CharField('asset name', max_length=1000, blank=True) # 
    asset_series        = models.CharField('series name', max_length=1000, blank=True) # Series Name
    asset_season        = models.CharField('season name', max_length=1000, blank=True) # Season
    asset_season_id     = models.CharField('asset season id', max_length=1000, blank=True) # Season ID (Hidden)
    asset_episode       = models.CharField('episode name', max_length=1000, blank=True) # Episode Name
    asset_network       = models.CharField('original channel',max_length=100, blank=True) # Source Net
    container_position  = models.CharField('container position', max_length=100) # Container Position / Episode number
    priority_asset      = models.BooleanField('priority', blank=True, default=False) # Promotable List
    free_episode        = models.BooleanField('free episode', blank=True, default=False) # Sale Type
    promo               = models.BooleanField('promo', blank=True, default=False) # 
    total_run_time      = models.CharField('total run time', max_length=1000, blank=True) # TRT
    asset_rating        = models.CharField('asset rating', max_length=1000, blank=True) # Rating
    not_in_pds          = models.CharField('not in pds', max_length=1000, blank=True) # Not in PDS
    versions_created    = models.CharField('source filename', max_length=1000, blank=True) # Version Created
    kaltura_source_id   = models.CharField('Kaltura Source ID', max_length=1000, blank=True) # Kaltura Source ID
    subtitle_filename   = models.CharField('Source Subtitle Filename', max_length=1000, blank=True) # Source Subtitle Filename
    subtitle_filename_jp   = models.CharField('Subtitle Filename JP', max_length=1000, blank=True) # Source Subtitle Filename JP
    subtitle_filename_nl   = models.CharField('Subtitle Filename NL', max_length=1000, blank=True) # Source Subtitle Filename NL
    subtitle_filename_sv   = models.CharField('Subtitle Filename SV', max_length=1000, blank=True) # Source Subtitle Filename SV
    subtitle_filename_no   = models.CharField('Subtitle Filename NO', max_length=1000, blank=True) # Source Subtitle Filename NO
    subtitle_filename_fi   = models.CharField('Subtitle Filename FI', max_length=1000, blank=True) # Source Subtitle Filename FI
    subtitle_filename_da   = models.CharField('Subtitle Filename DA', max_length=1000, blank=True) # Source Subtitle Filename DA

    #source
    source_filename     = models.CharField('source filename', max_length=1000, blank=True) # logistics - source filename
    source_status       = models.CharField('source status', max_length=1000, blank=True) # logistics - 
    RESOLUTIONS         = (('HD', 'HD'), ('SD-Fullscreen', 'SD-Fullscreen'), ('SD-Letterbox', 'SD-Letterbox')) #  logistics
    source_res          = models.CharField('source resolution', max_length=100, choices=RESOLUTIONS, blank=True) #  logistics
    FRAMERATES          = (('29.97','29.97'),('59.94','59.94'),('23.98','23.98'),('50.00','50.00'),) #  logistics
    source_framerate    = models.CharField('source framerate', max_length=10, choices=FRAMERATES, blank=True) #  logistics
    caption_filename    = models.CharField('caption Filename', max_length=1000, blank=True) #  logistics
    caption_status      = models.CharField('caption status', max_length=1000, blank=True) #  logistics
    playlist_posted     = models.DateField('date playlist posted', blank=True, null=True) #  logistics
    
    #participants
    amazon              = models.BooleanField('Amazon', default=False, blank=True) # Amazon
    apple               = models.BooleanField('Apple', default=False,) # Apple
    google              = models.BooleanField('Google', default=False,) # Google
    vudu                = models.BooleanField('VUDU    ', default=False,) # VUDU
    sony                = models.BooleanField('Sony', default=False,) # Sony
    microsoft           = models.BooleanField('Microsoft', default=False,) # Microsoft
    fandangonow         = models.BooleanField('Fandango', default=False,) # FandangoNOW
    comcastest          = models.BooleanField('ComcastEST', default=False,) # Comcast EST
    mtod                = models.BooleanField('MotorTrend', default=False,) # MTOD
    hulu                = models.BooleanField('Hulu', default=False,) # Hulu
    kaleidescape        = models.BooleanField('S3', default=False,) # used to Kaleidescape, had to make it s3 on a shitty day

    #workflow
    transcoded          = models.DateField('date transcoded', blank=True, null=True) # operations
    video_quality_check = models.DateField('video QC', blank=True, null=True) # operations
    cc_quality_check    = models.DateField('SCC QC', blank=True, null=True) # operations
    dumpster            = models.DateField('dumpster', blank=True, null=True) # operations
    in_queue            = models.DateField('in queue', blank=True, null=True) # operations

    # shipped dates (check for import of data, if shipped, then set updated_metadata to True)
    amazon_date         = models.DateField('Amazon Delivered', blank=True, null=True) # operations
    itunes_date         = models.DateField('iTunes Delivered', blank=True, null=True) # operations
    google_date         = models.DateField('Google Delivered', blank=True, null=True) # operations
    vudu_date           = models.DateField('VUDU Delivered', blank=True, null=True) # operations
    sony_date           = models.DateField('Sony Delivered', blank=True, null=True) # operations
    microsoft_date      = models.DateField('Microsoft Delivered', blank=True, null=True) # operations
    fandangonow_date    = models.DateField('Fandangonow Delivered', blank=True, null=True) # operations
    comcastest_date     = models.DateField('ComcastEST Delivered', blank=True, null=True) # operations
    mtod_date           = models.DateField('MTOD Delivered', blank=True, null=True) # operations
    hulu_date           = models.DateField('Hulu Delivered', blank=True, null=True) # operations
    kaleidescape_date   = models.DateField('Kaleidescape Delivered', blank=True, null=True) # operations
    s3_archive_date     = models.DateField('S3 Archive', blank=True, null=True) # operations

    # if all transfers are complete - this is a calculated field
    transfer_complete   = models.BooleanField('transfer complete', default=False) # operations

    # shared with everybody
    notes               = models.TextField(blank=True, null=True) # librarian

    #xml gen
    video_filesize      = models.CharField('video filesize', max_length=1000, blank=True)# librarian
    video_checksum      = models.CharField('video checksum', max_length=1000, blank=True)# librarian
    scc_filesize        = models.CharField('scc filesize', max_length=1000, blank=True)# librarian
    scc_checksum        = models.CharField('scc checksum', max_length=1000, blank=True)# librarian

    created             = models.DateTimeField('created', auto_now_add=True, editable=False) # librarian
    last_updated        = models.DateTimeField('updated', auto_now=True, editable=False) # librarian
    logistics_update    = models.DateTimeField('logistics updated', editable=True, blank=True, null=True)
    operations_update   = models.DateTimeField('operations updated', editable=True, blank=True, null=True)
    restrictions_update = models.DateTimeField('restrictions updated', editable=True, blank=True, null=True)

    # from the CSV
    tms_show_id         = models.CharField('TMS Show ID', max_length=1000, blank=True)# TMS Show ID
    tms_ep_id           = models.CharField('TMS Episode ID', max_length=1000, blank=True)# TMS Episode ID
    hulu_id             = models.CharField('Hulu ID', max_length=1000, blank=True)# Hulu ID
    hulu_genre          = models.CharField('Hulu Genre', max_length=1000, blank=True)# Hulu Genre
    hulu_category       = models.CharField('Hulu Category', max_length=1000, blank=True)# Hulu Genre
    genre               = models.CharField('Genre', max_length=1000, blank=True)# Genre
    vendor_id           = models.CharField('Vendor ID', max_length=1000, blank=True)# Vendor ID
    s3_asset_id         = models.CharField('S3 Asset ID', max_length=1000, blank=True)# S3 Asset ID

    c_title_tbd         = models.CharField('Title TBD', max_length=1000, blank=True)# Title TBD
    c_rating_tbd        = models.CharField('Rating TBD', max_length=1000, blank=True)# Ep Rating
    c_description_tbd   = models.CharField('Description TBD', max_length=1000, blank=True)# Description TBD
    c_other             = models.CharField('Other', max_length=1000, blank=True)# Other
    c_released          = models.CharField('Released', max_length=1000, blank=True)# Released
    c_hulu_ready        = models.BooleanField('Hulu Ready', blank=True, default=False)# Hulu Ready
    c_hulu_xml_complete = models.BooleanField('Hulu XML complete', blank=True, default=False)# Hulu XML Complete
    c_xml_complete      = models.BooleanField('EST XML complete', blank=True, default=False)# XML Complete
    c_version_comments  = models.CharField('Version Comments', max_length=1000, blank=True)# Version Comments
    c_manual_xml        = models.CharField('Manual changes to XMLs', max_length=1000, blank=True)# Manual changes to XMLs
    c_prov_comments     = models.CharField('Provider Comments', max_length=1000, blank=True)# Provider Comments (for Avails)
    c_tbd               = models.CharField('TBD', max_length=1000, blank=True)# TBD
    c_shipped           = models.CharField('Shipped', max_length=1000, blank=True)# Shipped
    c_pricing           = models.CharField('Pricing for Avails', max_length=1000, blank=True)# Pricing for Avails
    c_asset_id165       = models.CharField('Asset ID 165', max_length=1000, blank=True)# Asset ID 165
    c_amazon_id170      = models.CharField('Amazon Asset IDs 170', max_length=1000, blank=True)# Amazon Asset ID's 170
    c_apple_id          = models.CharField('Apple Asset ID', max_length=1000, blank=True)# Apple Asset ID
    c_sony_id176        = models.CharField('SonyAsset ID 176', max_length=1000, blank=True)# SonyAsset ID's 176
    c_msoft_id193       = models.CharField('Microsoft Asset ID 193', max_length=1000, blank=True)# Microsoft Asset ID's 193
    c_google_id         = models.CharField('Google Asset ID', max_length=1000, blank=True)# Google Asset ID
    c_vudu_id           = models.CharField('Vudu Asset ID', max_length=1000, blank=True)# Vudu Asset ID
    c_fandango_id       = models.CharField('FandangoNOW Asset ID', max_length=1000, blank=True)# FandangoNOW Asset ID
    c_comcastest_id     = models.CharField('Comcast EST 1', max_length=1000, blank=True)# Comcast EST 1
    c_verizon_id        = models.CharField('Verizon', max_length=1000, blank=True)# Verizon
    c_google_g2id       = models.CharField('Google G2', max_length=1000, blank=True)# Google G2
    c_vudu_v2           = models.CharField('Vudu V2', max_length=1000, blank=True)# Vudu V2
    c_vendor_build_id   = models.CharField('Vendor ID Build', max_length=1000, blank=True)# Vendor ID Build Reference
    c_territory         = models.CharField('Sales Territory', max_length=1000, blank=True)# Sales Territory
    c_closedcaptioned   = models.CharField('Closed Captioned', max_length=1000, blank=True)# Closed Captioned
    c_impairment        = models.CharField('Impairment', max_length=1000, blank=True)# Impairment
    c_quarter           = models.CharField('Quarter launched', max_length=1000, blank=True)# Quarter launched
    c_charactercount    = models.CharField('Character Count for Ep Description', max_length=1000, blank=True)# Character Count for Ep Description
    c_ep_description    = models.CharField('Episode Description', max_length=1000, blank=True)# Episode Description
    c_start_date        = models.CharField('Start Date', max_length=1000, blank=True)# Start Date
    c_prop_id1          = models.CharField('Property ID 1', max_length=1000, blank=True)# Property ID 1
    c_sony              = models.CharField('Sony (Yes or No)', max_length=1000, blank=True)# Sony (Yes or No)
    c_apple_genre       = models.CharField('Apple Genre', max_length=1000, blank=True)# Apple Genre
    c_sony_genre        = models.CharField('Sony Genre', max_length=1000, blank=True)# Sony Genre
    c_episode_title     = models.CharField('Episode Title', max_length=1000, blank=True)# Episode Title
    c_container_pos1    = models.CharField('Container Position 1', max_length=1000, blank=True)# Container Position 1
    c_premier_date      = models.CharField('Premiere Date', max_length=1000, blank=True)# Premiere Date
    c_sale_type1        = models.CharField('Sale Type 1', max_length=1000, blank=True)# Sale Type 1
    c_needs_ca_release  = models.CharField('Needs CA Release', max_length=1000, blank=True)# Needs CA Release
    c_complete          = models.CharField('Complete Canada', max_length=1000, blank=True)# Complete Canada
    c_container_id      = models.CharField('Container ID', max_length=1000, blank=True)# Container ID
    c_amazon_season_sku = models.CharField('Amazon Season Sku', max_length=1000, blank=True)# Amazon Season Sku
    c_amazon_series_sku = models.CharField('Amazon Series SKU', max_length=1000, blank=True)# Amazon Series SKU
    c_studio_name       = models.CharField('Studio Name', max_length=1000, blank=True)# Studio Name
    c_series_start      = models.CharField('Series Start Year', max_length=1000, blank=True)# Series Start Year
    c_season1           = models.CharField('Season', max_length=1000, blank=True)# Season 1
    c_seasondescript    = models.CharField('Season Description', max_length=1000, blank=True)# Season Description
    c_seriesdescript    = models.CharField('Series Description', max_length=1000, blank=True)# Series Description
    c_amazon_genres     = models.CharField('Amazon Genres', max_length=1000, blank=True)# Amazon Genres
    c_microsoft_genres  = models.CharField('Microsoft Genres', max_length=1000, blank=True)# Microsoft Genres
    c_ASIN              = models.CharField('ASIN', max_length=1000, blank=True)# ASIN
    c_blank1            = models.CharField('Blank', max_length=1000, blank=True)# Blank
    c_disc_formula      = models.CharField('Formula for DISCs', max_length=1000, blank=True)# Formula for DISCs
    c_blank2            = models.CharField('blank 2', max_length=1000, blank=True)# blank 2
    c_blank3            = models.CharField('blank 3', max_length=1000, blank=True)# blank 3
    c_ema1              = models.CharField('FOR EMA 1', max_length=1000, blank=True)# FOR EMA 1
    c_ema2              = models.CharField('FOR EMA 2', max_length=1000, blank=True)# FOR EMA 2
    c_ema3              = models.CharField('FOR EMA 3', max_length=1000, blank=True)# FOR EMA 3
    c_ema4              = models.CharField('FOR EMA 4', max_length=1000, blank=True)# FOR EMA 4
    c_blank4            = models.CharField('blank 4', max_length=1000, blank=True)# blank 4
    c_vendid_form       = models.CharField('Formula for Vendor ID on EMA', max_length=1000, blank=True)# Formula for Vendor ID on EMA
    c_disc_formula1     = models.CharField('Formula for DISCs 1', max_length=1000, blank=True)# Formula for DISCs 1
    c_disc_formula2     = models.CharField('Formula for DISCs 2', max_length=1000, blank=True)# Formula for DISCs 2
    c_move_inventory    = models.CharField('Move to Inventory', max_length=1000, blank=True)# Move to Inventory
    c_amaz_genre1       = models.CharField('amazon Genre for XML-1', max_length=1000, blank=True)# amazon Genre for XML-1
    c_amaz_genre2       = models.CharField('amazon Genre for XML-2', max_length=1000, blank=True)# amazon Genre for XML-2
    c_amaz_genre3       = models.CharField('amazon Genre for XML-3', max_length=1000, blank=True)# amazon Genre for XML-3
    c_amaz_genre4       = models.CharField('amazon Genre for XML-4', max_length=1000, blank=True)# amazon Genre for XML-4
    c_amaz_genre5       = models.CharField('amazon Genre for XML-5', max_length=1000, blank=True)# amazon Genre for XML-5
    c_amaz_date_xml1    = models.CharField('amazon date for XML 1', max_length=1000, blank=True)# amazon date for XML 1
    c_amaz_date_xml2    = models.CharField('amazon date for XML', max_length=1000, blank=True)# amazon date for XML
    c_seson_num         = models.CharField('Season Number', max_length=1000, blank=True)# Season Number
    c_plan_status       = models.CharField('planner status', max_length=1000, blank=True)# planner status

    # from PDS-HIVE for MTOD
    pds_scheduledate    = models.CharField('PDS Scheduledate', max_length=1000, blank=True)# PDS scheduledates
    pds_seriestitle     = models.CharField('PDS Seriestitle', max_length=1000, blank=True)# PDS series title
    pds_title           = models.CharField('PDS Title', max_length=1000, blank=True)# PDS Title
    pds_property        = models.CharField('PDS Property', max_length=1000, blank=True)# PDS Property
    pds_episode         = models.CharField('PDS Episode', max_length=1000, blank=True)# PDS Episode
    pds_premiere        = models.CharField('PDS Premiere', max_length=1000, blank=True)# PDS Premiere Flag
    #pds_rating
    #dci_territory

    # Georestriction Data
    mtod_start_date     = models.DateField('MTOD Start', blank=True, null=True) # MTOD ship date
    mtod_end_date       = models.DateField('MTOD End', blank=True, null=True) # MTOD shipping end
    mtod_id             = models.CharField('MTOD ID', max_length=1000, blank=True)# MTOD Asset ID
    geo_start_date      = models.DateField('Geo Restriction Start', blank=True, null=True) # Librarian GEO start date
    geo_end_date        = models.DateField('Geo Restriction End', blank=True, null=True) # Librarian GEO end date

    # Georestriction HOLDBACKS
    HOLDBACK_FLAGS                  =  (('', '------'),('Not Restricted', 'Not Restricted'),('Never Allow', 'Never Allow'), ('Restricted', 'Restricted'),)
    
    GEO_BLOCK_CHOICES                      = (
                                        ('DCI 0 - Block Africa, Australia, Austria, France, Italy, Poland, Spain, Switzerland', 'DCI 0 - Block Africa, Australia, Austria, France, Italy, Poland, Spain, Switzerland'),
                                        ('DCI 1 - Block Africa, Australia, France, Middle East, Poland, Spain, UK', 'DCI 1 - Block Africa, Australia, France, Middle East, Poland, Spain, UK'),
                                        ('DCI 2 - Block Africa, Australia, France, Middle East, Poland, UK', 'DCI 2 - Block Africa, Australia, France, Middle East, Poland, UK'),
                                        ('DCI 4 - Block Africa, Australia, Austria, Middle East, Poland, Spain, Switzerland, UK', 'DCI 4 - Block Africa, Australia, Austria, Middle East, Poland, Spain, Switzerland, UK'),
                                        ('DCI 5 - Block Africa, Australia, Middle East, Poland, Spain, UK', 'DCI 5 - Block Africa, Australia, Middle East, Poland, Spain, UK'),
                                        ('DCI 6 - Block Africa, Australia, Middle East, Poland, UK', 'DCI 6 - Block Africa, Australia, Middle East, Poland, UK'),
                                        ('DCI 7 - Block Australia, Middle East, Poland, UK', 'DCI 7 - Block Australia, Middle East, Poland, UK'),
                                        ('DCI 8 - Block Africa, Australia, France, Poland, UK', 'DCI 8 - Block Africa, Australia, France, Poland, UK'),
                                        ('DCI 9 - Block Australia, France, Poland, Spain, UK', 'DCI 9 - Block Australia, France, Poland, Spain, UK'),
                                        ('DCI 10 - Block Australia, France, Poland, UK', 'DCI 10 - Block Australia, France, Poland, UK'),
                                        ('DCI 11 - Block Africa, Australia, Poland, UK', 'DCI 11 - Block Africa, Australia, Poland, UK'),
                                        ('DCI 12 - Block Australia, Poland, UK', 'DCI 12 - Block Australia, Poland, UK'),
                                        ('DCI 13 - Block Africa, Australia, France, Middle East, Poland, Spain', 'DCI 13 - Block Africa, Australia, France, Middle East, Poland, Spain'),
                                        ('DCI 14 - Block Africa, Australia, France, Middle East, Poland', 'DCI 14 - Block Africa, Australia, France, Middle East, Poland'),
                                        ('DCI 15 - Block Africa, Australia, Austria, Middle East, Poland, Spain, Switzerland', 'DCI 15 - Block Africa, Australia, Austria, Middle East, Poland, Spain, Switzerland'),
                                        ('DCI 16 - Block Africa, Australia, Middle East, Poland, Spain', 'DCI 16 - Block Africa, Australia, Middle East, Poland, Spain'),
                                        ('DCI 17 - Block Africa, Australia, Middle East, Poland', 'DCI 17 - Block Africa, Australia, Middle East, Poland'),
                                        ('DCI 19 - Block Africa, Australia, France, Middle East, Poland, Spain', 'DCI 19 - Block Africa, Australia, France, Middle East, Poland, Spain'),
                                        ('DCI 20 - Block Africa, Australia, Canada, France, Middle East, Poland, Spain, UK', 'DCI 20 - Block Africa, Australia, Canada, France, Middle East, Poland, Spain, UK'),
                                        ('DCI 21 - Block, Africa, Australia, Canada, France, Middle East, Poland, Spain', 'DCI 21 - Block, Africa, Australia, Canada, France, Middle East, Poland, Spain'),
                                        ('DCI 22 - Block Africa, Australia, Canada, France, Germany, Middle East, Poland, Spain', 'DCI 22 - Block Africa, Australia, Canada, France, Germany, Middle East, Poland, Spain'),
                                        ('DCI 23 - Block Australia, Poland, France, Spain', 'DCI 23 - Block Australia, Poland, France, Spain'),
                                        ('DCI 24 - Block Africa, Australia, France, Italy, Middle East, Poland, UK', 'DCI 24 - Block Africa, Australia, France, Italy, Middle East, Poland, UK'),
                                        ('DCI 25 - Block Africa, Australia, France, Italy, Middle East, Poland, Spain', 'DCI 25 - Block Africa, Australia, France, Italy, Middle East, Poland, Spain'),
                                        ('DCI 26 - Block Australia, Austria, France, Italy, Poland, Spain, Switzerland, UK', 'DCI 26 - Block Australia, Austria, France, Italy, Poland, Spain, Switzerland, UK'),
                                        ('DCI 27 - Block Africa, Australia, Austria, France, Middle East, Poland, Spain, Switzerland, UK', 'DCI 27 - Block Africa, Australia, Austria, France, Middle East, Poland, Spain, Switzerland, UK'),
                                        ('DCI 28 - Block Australia, Austria, France, Middle East, Poland, Spain, Switzerland, UK', 'DCI 28 - Block Australia, Austria, France, Middle East, Poland, Spain, Switzerland, UK'),
                                        ('DCI 29 - Block Africa, Australia, Austria, Italy, Middle East, Poland, Spain', 'DCI 29 - Block Africa, Australia, Austria, Italy, Middle East, Poland, Spain'),
                                        ('DCI 30 - Block Africa, Australia, France, Italy, Middle East, Poland, Spain, UK', 'DCI 30 - Block Africa, Australia, France, Italy, Middle East, Poland, Spain, UK'),
                                        ('DCI 31 - Block Africa, Australia, Austria, France, Italy, Middle East, Poland, Spain, Switzerland, UK', 'DCI 31 - Block Africa, Australia, Austria, France, Italy, Middle East, Poland, Spain, Switzerland, UK'),
                                        ('DCI 32 - Block Australia, Austria, France, Poland, Spain, Switzerland, UK', 'DCI 32 - Block Australia, Austria, France, Poland, Spain, Switzerland, UK'),
                                        ('DCI 33 - Block Africa, Australia, Austria, France, Germany, Italy, Middle East, Poland, Spain, Switzerland, UK', 'DCI 33 - Block Africa, Australia, Austria, France, Germany, Italy, Middle East, Poland, Spain, Switzerland, UK'),
                                        ('DCI 34 - Block France', 'DCI 34 - Block France')
                                        )
    us_cox_pulldown                 = models.CharField('US Cox Pulldown', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    us_cox_pulldown_start           = models.DateField('Start Date', blank=True, null=True)
    us_cox_pulldown_end             = models.DateField('End Date', blank=True, null=True)
    uk_svod_holdback                = models.CharField('UK SVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    uk_svod_holdback_start          = models.DateField('Start Date', blank=True, null=True)
    uk_svod_holdback_end            = models.DateField('End Date', blank=True, null=True)
    uk_avod_holdback                = models.CharField('UK AVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    uk_avod_holdback_start          = models.DateField('Start Date', blank=True, null=True)
    uk_avod_holdback_end            = models.DateField('End Date', blank=True, null=True)
    australia_svod_holdback         = models.CharField('Australia SVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    australia_svod_holdback_start   = models.DateField('Start Date', blank=True, null=True)
    australia_svod_holdback_end     = models.DateField('End Date', blank=True, null=True)
    australia_avod_holdback         = models.CharField('Australia AVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    australia_avod_holdback_start   = models.DateField('Start Date', blank=True, null=True)
    australia_avod_holdback_end     = models.DateField('End Date', blank=True, null=True)
    germany_svod_holdback           = models.CharField('Germany SVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    germany_svod_holdback_start     = models.DateField('Start Date', blank=True, null=True)
    germany_svod_holdback_end       = models.DateField('End Date', blank=True, null=True)
    germany_avod_holdback           = models.CharField('Germany AVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    germany_avod_holdback_start     = models.DateField('Start Date', blank=True, null=True)
    germany_avod_holdback_end       = models.DateField('End Date', blank=True, null=True)
    italy_svod_holdback             = models.CharField('Italy SVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    italy_svod_holdback_start       = models.DateField('Start Date', blank=True, null=True)
    italy_svod_holdback_end         = models.DateField('End Date', blank=True, null=True)
    italy_avod_holdback             = models.CharField('Italy AVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    italy_avod_holdback_start       = models.DateField('Start Date', blank=True, null=True)
    italy_avod_holdback_end         = models.DateField('End Date', blank=True, null=True)
    poland_svod_holdback            = models.CharField('Poland SVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    poland_svod_holdback_start      = models.DateField('Start Date', blank=True, null=True)
    poland_svod_holdback_end        = models.DateField('End Date', blank=True, null=True)
    poland_avod_holdback            = models.CharField('Poland AVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    poland_avod_holdback_start      = models.DateField('Start Date', blank=True, null=True)
    poland_avod_holdback_end        = models.DateField('End Date', blank=True, null=True)
    middleeast_svod_holdback        = models.CharField('MiddleEast SVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    middleeast_svod_holdback_start  = models.DateField('Start Date', blank=True, null=True)
    middleeast_svod_holdback_end    = models.DateField('End Date', blank=True, null=True)
    middleeast_avod_holdback        = models.CharField('MiddleEast AVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    middleeast_avod_holdback_start  = models.DateField('Start Date', blank=True, null=True)
    middleeast_avod_holdback_end    = models.DateField('End Date', blank=True, null=True)
    france_svod_holdback            = models.CharField('France SVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    france_svod_holdback_start      = models.DateField('Start Date', blank=True, null=True)
    france_svod_holdback_end        = models.DateField('End Date', blank=True, null=True)
    france_avod_holdback            = models.CharField('France AVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    france_avod_holdback_start      = models.DateField('Start Date', blank=True, null=True)
    france_avod_holdback_end        = models.DateField('End Date', blank=True, null=True)
    africa_svod_holdback            = models.CharField('Africa SVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    africa_svod_holdback_start      = models.DateField('Start Date', blank=True, null=True)
    africa_svod_holdback_end        = models.DateField('End Date', blank=True, null=True)
    africa_avod_holdback            = models.CharField('Africa AVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    africa_avod_holdback_start      = models.DateField('Start Date', blank=True, null=True)
    africa_avod_holdback_end        = models.DateField('End Date', blank=True, null=True)
    switzerland_svod_holdback       = models.CharField('Swiss SVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    switzerland_svod_holdback_start = models.DateField('Start Date', blank=True, null=True)
    switzerland_svod_holdback_end   = models.DateField('End Date', blank=True, null=True)
    switzerland_avod_holdback       = models.CharField('Swiss AVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    switzerland_avod_holdback_start = models.DateField('Start Date', blank=True, null=True)
    switzerland_avod_holdback_end   = models.DateField('End Date', blank=True, null=True)
    spain_svod_holdback             = models.CharField('Spain SVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    spain_svod_holdback_start       = models.DateField('Start Date', blank=True, null=True)
    spain_svod_holdback_end         = models.DateField('End Date', blank=True, null=True)
    spain_avod_holdback             = models.CharField('Spain AVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    spain_avod_holdback_start       = models.DateField('Start Date', blank=True, null=True)
    spain_avod_holdback_end         = models.DateField('End Date', blank=True, null=True)
    austria_svod_holdback           = models.CharField('Austria SVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    austria_svod_holdback_start     = models.DateField('Start Date', blank=True, null=True)
    austria_svod_holdback_end       = models.DateField('End Date', blank=True, null=True)
    austria_avod_holdback           = models.CharField('Austria AVOD Hold', blank=True, max_length=100, choices=HOLDBACK_FLAGS, default='')
    austria_avod_holdback_start     = models.DateField('Start Date', blank=True, null=True)
    austria_avod_holdback_end       = models.DateField('End Date', blank=True, null=True)
    mtod_publish_flag               = models.NullBooleanField('MTOD Publish')
    geo_block                       = models.CharField('GeoBlock Setting', default='', blank=True, max_length=200, choices=GEO_BLOCK_CHOICES)

    def __str__(self):
        return self.propep

    def save(self, *args, **kwargs):
        operation_fields = ["transcoded", "video_quality_check", "cc_quality_check", "dumpster",
                        "in_queue", "amazon_date", "itunes_date", "google_date", "vudu_date",
                        "sony_date", "microsoft_date", "fandangonow_date", "comcastest_date",
                        "mtod_date", "kaleidescape_date", "hulu_date", "s3_archive_date"]
        logistics_fields = ["source_filename", "source_status", "source_res", "source_framerate",
                        "caption_filename", "caption_status", "playlist_posted"]
        georestrictions_fields = [
            "geo_start_date",
            'us_cox_pulldown',
            'us_cox_pulldown_start',
            'us_cox_pulldown_end',
            'uk_svod_holdback',
            'uk_svod_holdback_start',
            'uk_svod_holdback_end',
            'uk_avod_holdback',
            'uk_avod_holdback_start',
            'uk_avod_holdback_end',
            'australia_svod_holdback',
            'australia_svod_holdback_start',
            'australia_svod_holdback_end',
            'australia_avod_holdback',
            'australia_avod_holdback_start',
            'australia_avod_holdback_end',
            'germany_svod_holdback',
            'germany_svod_holdback_start',
            'germany_svod_holdback_end',
            'germany_avod_holdback',
            'germany_avod_holdback_start',
            'germany_avod_holdback_end',
            'italy_svod_holdback',
            'italy_svod_holdback_start',
            'italy_svod_holdback_end',
            'italy_avod_holdback',
            'italy_avod_holdback_start',
            'italy_avod_holdback_end',
            'poland_svod_holdback',
            'poland_svod_holdback_start',
            'poland_svod_holdback_end',
            'poland_avod_holdback',
            'poland_avod_holdback_start',
            'poland_avod_holdback_end',
            'middleeast_svod_holdback',
            'middleeast_svod_holdback_start',
            'middleeast_svod_holdback_end',
            'middleeast_avod_holdback',
            'middleeast_avod_holdback_start',
            'middleeast_avod_holdback_end',
            'france_svod_holdback',
            'france_svod_holdback_start',
            'france_svod_holdback_end',
            'france_avod_holdback',
            'france_avod_holdback_start',
            'france_avod_holdback_end',
            'africa_svod_holdback',
            'africa_svod_holdback_start',
            'africa_svod_holdback_end',
            'africa_avod_holdback',
            'africa_avod_holdback_start',
            'africa_avod_holdback_end',
            'switzerland_svod_holdback',
            'switzerland_svod_holdback_start',
            'switzerland_svod_holdback_end',
            'switzerland_avod_holdback',
            'switzerland_avod_holdback_start',
            'switzerland_avod_holdback_end',
            'spain_svod_holdback',
            'spain_svod_holdback_start',
            'spain_svod_holdback_end',
            'spain_avod_holdback',
            'spain_avod_holdback_start',
            'spain_avod_holdback_end',
            'austria_svod_holdback',
            'austria_svod_holdback_start',
            'austria_svod_holdback_end',
            'austria_avod_holdback',
            'austria_avod_holdback_start',
            'austria_avod_holdback_end'
        ]

        TERRITORY_NAME_LOOKUP = {
            'us_cox_pulldown': 'US',
            'uk_svod_holdback': 'UK',
            'uk_avod_holdback': 'UK',
            'australia_svod_holdback': 'Australia',
            'australia_avod_holdback': 'Australia',
            'germany_svod_holdback': 'Germany',
            'germany_avod_holdback': 'Germany',
            'italy_svod_holdback': 'Italy',
            'italy_avod_holdback': 'Italy',
            'poland_svod_holdback': 'Poland',
            'poland_avod_holdback': 'Poland',
            'middleeast_svod_holdback': 'Middle East',
            'middleeast_avod_holdback': 'Middle East',
            'france_svod_holdback': 'France',
            'france_avod_holdback': 'France',
            'africa_svod_holdback': 'Africa',
            'africa_avod_holdback': 'Africa',
            'switzerland_svod_holdback': 'Switzerland',
            'switzerland_avod_holdback': 'Switzerland',
            'spain_svod_holdback': 'Spain',
            'spain_avod_holdback': 'Spain',
            'austria_svod_holdback': 'Austria',
            'austria_avod_holdback': 'Austria',

        }
        if not self.geo_block or self.geo_block == '':
            territories = set()
            for field in georestrictions_fields:
                if (
                    field in TERRITORY_NAME_LOOKUP.keys() and 
                    getattr(self, field) in ['Restricted', 'Never Allow'] and
                    TERRITORY_NAME_LOOKUP[field] != 'US'
                ):
                    territories.add(TERRITORY_NAME_LOOKUP[field])
            terr_str = ', '.join(sorted(territories))
            if DCI_LOOKUP.get(terr_str, ''):
                self.geo_block = DCI_LOOKUP.get(terr_str, '') + ' - Block ' + terr_str
        else:
            self.geo_block.replace('–', '-')
        
        if self.pk is not None:
            original = Plan.objects.get(pk=self.pk)
            for field in logistics_fields:
                try:
                    if getattr(original,field) != getattr(self, field):
                        self.__setattr__("logistics_update", datetime.now())
                    #print(field,"updated from", getattr(original,field), "to", getattr(self, field))
                except AttributeError:
                    pass
            for field in operation_fields:
                try:
                    if getattr(original,field) != getattr(self, field):
                        self.__setattr__("operations_update", datetime.now())
                        #print(field,"updated from", getattr(original,field), "to", getattr(self, field))
                except AttributeError:
                    pass
            for field in georestrictions_fields:
                try:
                    if getattr(original,field) != getattr(self, field):
                        self.__setattr__("restrictions_update", datetime.now())
                        #print(field,"updated from", getattr(original,field), "to", getattr(self, field))
                except AttributeError:
                    pass
        # for key,value in data.items():
        #     self.__setattr__(key,value)
        for participant, date_field in Plan.participants_datefileds().items():
            if getattr(self, participant):
                if not getattr(self, date_field):
                    self.transfer_complete = False
                    break
                else:
                    self.transfer_complete = True
            # if self.transfer_complete and not self.s3_archive_date:
            #     self.transfer_complete = False
        super(Plan, self).save(*args, **kwargs)

    @classmethod
    def get_csv_model_map(cls):
        return {
            "Sale Type 1": "c_sale_type1",
            "amazon Genre for XML-2": "c_amaz_genre2",
            "blank 3": "c_blank3",
            # "source framerate": "source_framerate",
            "in queue": "in_queue",
            "Container Position": "container_position",
            "Deliverable": "deliverable",
            "Property ID 1": "c_prop_id1",
            "S3": "kaleidescape",
            "Hulu ID": "propep",
            "Hulu": "hulu",
            "Formula for DISCs": "c_disc_formula",
            "Premiere Date": "c_premier_date",
            "Container ID": "c_container_id",
            "itunes delivered": "itunes_date",
            "FandangoNOW Asset ID": "c_fandango_id",
            "Apple Start": "apple_start_date",
            "MTOD shipping date": "mtod_start_date",
            "Closed Captioned": "c_closedcaptioned",
            "FOR EMA 4": "c_ema4",
            "Series Description": "c_seriesdescript",
            "VUDU": "vudu",
            "Manual changes to XMLs": "c_manual_xml",
            "Provider Comments (for Avails)": "c_prov_comments",
            "amazon Genre for XML-5": "c_amaz_genre5",
            "Source Net": "asset_network",
            "Move to Inventory": "c_move_inventory",
            "amazon Genre for XML-3": "c_amaz_genre3",
            "Microsoft Asset ID's 193": "c_msoft_id193",
            "Episode Description": "c_ep_description",
            "Linear Premiere": "linear_premiere",
            "ID": "id",
            "sony delivered": "sony_date",
            "Blank": "c_blank1",
            "Amazon Genres": "c_amazon_genres",
            "planner status": "c_plan_status",
            "amazon date for XML": "c_amaz_date_xml2",
            "microsoft delivered": "microsoft_date",
            "vudu delivered": "vudu_date",
            "Released": "c_released",
            "dumpster": "dumpster",
            "amazon Genre for XML-4": "c_amaz_genre4",
            "Series Name": "asset_series",
            "Character Count for Ep Description": "c_charactercount",
            "Season 1": "c_season1",
            "NLD type": "offer_type",
            "last updated": "last_updated",
            "Version Comments": "c_version_comments",
            "Due to be Shipped": "delivery_date",
            "Rating": "asset_rating",
            "TBD": "c_tbd",
            "Rating TBD": "c_rating_tbd",
            "Not in PDS": "not_in_pds",
            "Microsoft": "microsoft",
            "SonyAsset ID's 176": "c_sony_id176",
            "Episode Name": "asset_episode",
            "Prop ID Reference": "propep",
            "Apple Genre": "c_apple_genre",
            "notes": "notes",
            "amazon delivered": "amazon_date",
            "fandangonow delivered": "fandangonow_date",
            "Season": "asset_season",
            "Quarter launched": "c_quarter",
            "Vudu Asset ID": "c_vudu_id",
            "Needs CA Release": "c_needs_ca_release",
            "Sony (Yes or No)": "c_sony",
            "Sony Genre": "c_sony_genre",
            "caption Filename": "caption_filename",
            "date playlist posted": "playlist_posted",
            "Sony": "sony",
            "Amazon": "amazon",
            "ASIN": "c_ASIN",
            "Impairment": "c_impairment",
            "FOR EMA 1": "c_ema1",
            "Asset ID 165": "c_asset_id165",
            "Amazon Series SKU": "c_amazon_series_sku",
            "Promotable List": "priority_asset",
            "Amazon Asset ID's 170": "c_amazon_id170",
            "Sales Territory": "c_territory",
            "Microsoft Genres": "c_microsoft_genres",
            "Title TBD": "c_title_tbd",
            "Description TBD": "c_description_tbd",
            "Verizon": "c_verizon_id",
            "Season Number": "c_seson_num",
            "DLTO Start": "market_start_date",
            "Vendor ID Build Reference": "c_vendor_build_id",
            "blank 2": "c_blank2",
            "Start Date": "c_start_date",
            "Apple": "apple",
            "comcast delivered": "comcastest_date",
            "Comcast EST": "c_comcastest_id",
            "Vudu V2": "c_vudu_v2",
            "Container Position 1": "c_container_pos1",
            "XML Complete": "c_xml_complete",
            "Apple Asset ID": "c_apple_id",
            "date transcoded": "transcoded",
            "FOR EMA 2": "c_ema2",
            "Google Asset ID": "c_google_id",
            "created": "created",
            "Pricing for Avails": "c_pricing",
            "source resolution": "source_res",
            # "promo": "promo",
            "source filename": "source_filename",
            "Version Created": "versions_created",
            "MTOD": "mtod",
            "Formula for DISCs 1": "c_disc_formula1",
            "Studio Name": "c_studio_name",
            "Season ID": "asset_season_id",
            "kaleidescape delivered": "kaleidescape_date",
            "Amazon Season Sku": "c_amazon_season_sku",
            "verizon delivered": "mtod_date",
            "asset name": "asset_name",
            "Shipped": "c_shipped",
            "Google G2": "c_google_g2id",
            "Series Start Year": "c_series_start",
            "Formula for DISCs 2": "c_disc_formula2",
            "Google": "google",
            "Complete Canada": "c_complete",
            "amazon Genre for XML-1": "c_amaz_genre1",
            "ComcastEST": "comcastest",
            "FandangoNOW": "fandangonow",
            "transfer complete": "transfer_complete",
            "Episode Title": "c_episode_title",
            "TRT": "total_run_time",
            "quality check": "quality_check",
            "Other": "c_other",
            "caption status": "caption_status",
            #"free episode": "free_episode",
            "Season Description": "c_seasondescript",
            "google delivered": "google_date",
            "source status": "source_status",
            "blank 4": "c_blank4",
            "Formula for Vendor ID on EMA": "c_vendid_form",
            "amazon date for XML 1": "c_amaz_date_xml1",
            "FOR EMA 3": "c_ema3",
            "Sale Type": "free_episode",
            "Metadata complete": "metadata_complete",
            "Kaltura ID":"kaltura_source_id",
            "UK AVOD Flag":"uk_avod_holdback",
            "UK AVOD Start Date":"uk_avod_holdback_start",
            "UK AVOD End Date":"uk_avod_holdback_end",
            "UK SVOD Flag":"uk_svod_holdback",
            "UK SVOD Start Date":"uk_svod_holdback_start",
            "UK SVOD End Date":"uk_svod_holdback_end",
            "Australia AVOD Flag":"australia_avod_holdback",
            "Australia AVOD Start Date":"australia_avod_holdback_start",
            "Australia AVOD End Date":"australia_avod_holdback_end",
            "Australia SVOD Flag":"australia_svod_holdback",
            "Australia SVOD Start Date":"australia_svod_holdback_start",
            "Australia SVOD End Date":"australia_svod_holdback_end",
            "Germany AVOD Flag":"germany_avod_holdback",
            "Germany AVOD Start Date":"germany_avod_holdback_start",
            "Germany AVOD End Date":"germany_avod_holdback_end",
            "Germany SVOD Flag":"germany_svod_holdback",
            "Germany SVOD Start Date":"germany_svod_holdback_start",
            "Germany SVOD End Date":"germany_svod_holdback_end",
            "Italy AVOD Flag":"italy_avod_holdback",
            "Italy AVOD Start Date":"italy_avod_holdback_start",
            "Italy AVOD End Date":"italy_avod_holdback_end",
            "Italy SVOD Flag":"italy_svod_holdback",
            "Italy SVOD Start Date":"italy_svod_holdback_start",
            "Italy SVOD End Date":"italy_svod_holdback_end",
            "Poland AVOD Flag":"poland_avod_holdback",
            "Poland AVOD Start Date":"poland_avod_holdback_start",
            "Poland AVOD End Date":"poland_avod_holdback_end",
            "Poland SVOD Flag":"poland_svod_holdback",
            "Poland SVOD Start Date":"poland_svod_holdback_start",
            "Poland SVOD End Date":"poland_svod_holdback_end",
            "Middle East AVOD Flag":"middleeast_avod_holdback",
            "Middle East AVOD Start Date":"middleeast_avod_holdback_start",
            "Middle East AVOD End Date":"middleeast_avod_holdback_end",
            "Middle East SVOD Flag":"middleeast_svod_holdback",
            "Middle East SVOD Start Date":"middleeast_svod_holdback_start",
            "Middle East SVOD End Date":"middleeast_svod_holdback_end",
            "France AVOD Flag":"france_avod_holdback",
            "France AVOD Start Date":"france_avod_holdback_start",
            "France AVOD End Date":"france_avod_holdback_end",
            "France SVOD Flag":"france_svod_holdback",
            "France SVOD Start Date":"france_svod_holdback_start",
            "France SVOD End Date":"france_svod_holdback_end",
            "Africa AVOD Flag":"africa_avod_holdback",
            "Africa AVOD Start Date":"africa_avod_holdback_start",
            "Africa AVOD End Date":"africa_avod_holdback_end",
            "Africa SVOD Flag":"africa_svod_holdback",
            "Africa SVOD Start Date":"africa_svod_holdback_start",
            "Africa SVOD End Date":"africa_svod_holdback_end",
            "Switzerland AVOD Flag":"switzerland_avod_holdback",
            "Switzerland AVOD Start Date":"switzerland_avod_holdback_start",
            "Switzerland AVOD End Date":"switzerland_avod_holdback_end",
            "Switzerland SVOD Flag":"switzerland_svod_holdback",
            "Switzerland SVOD Start Date":"switzerland_svod_holdback_start",
            "Switzerland SVOD End Date":"switzerland_svod_holdback_end",
            "Spain AVOD Flag":"spain_avod_holdback",
            "Spain AVOD Start Date":"spain_avod_holdback_start",
            "Spain AVOD End Date":"spain_avod_holdback_end",
            "Spain SVOD Flag":"spain_svod_holdback",
            "Spain SVOD Start Date":"spain_svod_holdback_start",
            "Spain SVOD End Date":"spain_svod_holdback_end",
            "Austria AVOD Flag":"austria_avod_holdback",
            "Austria AVOD Start Date":"austria_avod_holdback_start",
            "Austria AVOD End Date":"austria_avod_holdback_end",
            "Austria SVOD Flag":"austria_svod_holdback",
            "Austria SVOD Start Date":"austria_svod_holdback_start",
            "Austria SVOD End Date":"austria_svod_holdback_end",
        }

    @classmethod
    def get_date_fields(cls):
        return [
            'delivery_date', 'market_start_date', 'linear_premiere',
            'mtod_start_date', 'apple_start_date', 'playlist_posted',
            'transcoded', 'quality_check', 'dumpster', 'in_queue',
            'amazon_date', 'itunes_date', 'google_date', 'vudu_date',
            'sony_date', 'microsoft_date', 'fandangonow_date',
            'comcastest_date', 'mtod_date', 'hulu_date', 'kaleidescape_date',
            'uk_svod_holdback_start','uk_svod_holdback_end',
            'uk_avod_holdback_start','uk_avod_holdback_end',
            'australia_svod_holdback_start','australia_svod_holdback_end',
            'australia_avod_holdback_start','australia_avod_holdback_end',
            'germany_svod_holdback_start','germany_svod_holdback_end',
            'germany_avod_holdback_start','germany_avod_holdback_end',
            'italy_svod_holdback_start','italy_svod_holdback_end',
            'italy_avod_holdback_start','italy_avod_holdback_end',
            'poland_svod_holdback_start','poland_svod_holdback_end',
            'poland_avod_holdback_start','poland_avod_holdback_end',
            'middleeast_svod_holdback_start','middleeast_svod_holdback_end',
            'middleeast_avod_holdback_start','middleeast_avod_holdback_end',
            'france_svod_holdback_start','france_svod_holdback_end',
            'france_avod_holdback_start','france_avod_holdback_end',
            'africa_svod_holdback_start','africa_svod_holdback_end',
            'africa_avod_holdback_start','africa_avod_holdback_end',
            'switzerland_svod_holdback_start','switzerland_svod_holdback_end',
            'switzerland_avod_holdback_start','switzerland_avod_holdback_end',
            'spain_svod_holdback_start','spain_svod_holdback_end',
            'spain_avod_holdback_start','spain_avod_holdback_end',
            'austria_svod_holdback_start','austria_svod_holdback_end',
            'austria_avod_holdback_start','austria_avod_holdback_end',
        ]

    @classmethod
    def get_boolean_fields(cls):
        return {
            'priority_asset':
                {1: ['Y'], 0: ['N']},
            'free_episode':
                {1: ['Free Episode'], 0: ['Season Only', 'Paid']},
            'promo':
                {1: ['HD'], 0: []},
            'amazon':
                {1: ['HD'], 0: []},
            'apple':
                {1: ['HD'], 0: []},
            'google':
                {1: ['HD'], 0: []},
            'vudu':
                {1: ['HD'], 0: []},
            'sony':
                {1: ['HD'], 0: []},
            'microsoft':
                {1: ['HD'], 0: []},
            'fandangonow':
                {1: ['HD'], 0: []},
            'comcastest':
                {1: ['HD'], 0: []},
            'mtod':
                {1: ['HD'], 0: []},
            'hulu':
                {1: ['HD'], 0: []},
            'kaleidescape':
                {1: ['HD'], 0: []},
            'metadata_complete':
                {1:['1','YES','Yes','yes','true','True','TRUE','Y','y'], 0:[]},
            'c_xml_complete':
                {1:['1','YES','Yes','yes','true','True','TRUE','Y','y', 'x', 'X'], 0:[]},
        }

    @classmethod
    def get_participants(cls):
        return ["amazon", "apple", "google", "vudu", "sony", "microsoft", "fandangonow", "comcastest", "mtod", "hulu", "kaleidescape"]

    @classmethod
    def update_fields(cls):
        return ["delivery_date", "market_start_date", "linear_premiere", "mtod_start_date", "apple_start_date","propep",
                "asset_name", "asset_series", "asset_season", "asset_season_id", "asset_episode", "asset_network",
                "container_position", "priority_asset", "free_episode", "promo", "total_run_time", "asset_rating",
                "not_in_pds", "versions_created", "c_title_tbd", "c_rating_tbd", "c_description_tbd", "c_other","c_released",
                "c_xml_complete", "c_version_comments", "c_manual_xml", "c_prov_comments", "c_tbd", "c_shipped","c_pricing",
                "c_territory", "c_closedcaptioned", "c_impairment", "c_quarter", "c_charactercount", "c_ep_description",
                "c_start_date", "c_prop_id1", "c_sony", "c_apple_genre", "c_sony_genre", "c_episode_title",
                "c_container_pos1", "c_premier_date", "c_sale_type1", "c_needs_ca_release", "c_complete","c_container_id",
                "c_amazon_season_sku", "c_amazon_series_sku", "c_studio_name", "c_series_start", "c_season1",
                "c_seasondescript", "c_seriesdescript", "c_amazon_genres", "c_microsoft_genres", "c_ASIN", "c_blank1",
                "c_disc_formula", "c_blank2", "c_blank3", "c_ema1", "c_ema2", "c_ema3", "c_ema4", "c_blank4","c_vendid_form",
                "c_disc_formula1", "c_disc_formula2", "c_move_inventory", "c_amaz_genre1", "c_amaz_genre2","c_amaz_genre3",
                "c_amaz_genre4", "c_amaz_genre5", "c_amaz_date_xml1", "c_amaz_date_xml2", "c_seson_num","c_plan_status"]

    @classmethod
    def get_shipped_fields(cls):
        return ["amazon_date", "itunes_date", "google_date", "vudu_date", "sony_date", "microsoft_date", "fandangonow_date",
                "comcastest_date", "mtod_date", "kaleidescape_date", "hulu_date", "s3_archive_date", "transfer_complete"]

    @classmethod
    def participants_datefileds(cls):
        return {
            "apple": "itunes_date",
            "amazon": "amazon_date",
            "google": "google_date",
            "vudu": "vudu_date",
            "sony": "sony_date",
            "microsoft": "microsoft_date",
            "fandangonow": "fandangonow_date",
            "comcastest": "comcastest_date",
            "mtod": "mtod_date",
            "hulu": "hulu_date",
            "kaleidescape": "s3_archive_date"
        }