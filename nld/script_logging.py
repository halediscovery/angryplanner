import logging

logger = None

def get_logger(file_name):
    """Return logger"""
    global logger
    # LOGGER
    if logger is not None:
        return logger
    log_format = ('%(asctime)s- %(levelname)s - %(message)s')
    formatter = logging.Formatter(log_format)
    logger = logging.getLogger('nld_planner')
    hdlr = logging.FileHandler(file_name)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)
    return logger

if __name__ == '__main__':
    get_logger()


