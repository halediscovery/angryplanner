from django.contrib import admin
from django.db.models import Q
from django.contrib.admin import DateFieldListFilter
from django.contrib.admin.filters import SimpleListFilter, AllValuesFieldListFilter
from django.utils.translation import gettext_lazy as _
from rangefilter.filter import DateRangeFilter
from datetime import datetime, timedelta, time as dtime
from django.utils import timezone


class LogisticsFilter(SimpleListFilter):
    title = _('Logistics')
    parameter_name = 'Work next 7 days'

    def lookups(self, request, model_admin):
        return (
            ('sourceset', _('Source Set')),
            ('nosourceset',  _('Ready for Source')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(source_filename__isnull=False)
        if self.value() == 'no':
            return queryset.filter(source_filename__isnull=True)


class CustomDateFieldListFilter(DateFieldListFilter):
    def __init__(self, *args, **kwargs):
        super(CustomDateFieldListFilter, self).__init__(*args, **kwargs)#

        now = timezone.now()
        # When time zone support is enabled, convert "now" to the user's time
        # zone so Django's definition of "Today" matches what the user expects.
        if timezone.is_aware(now):
            now = timezone.localtime(now)

        today = now.date()

        self.links += ((
            (_('Next 7 days'), {
                self.lookup_kwarg_since: str(today),
                self.lookup_kwarg_until: str(today + timedelta(days=7)),
            }),
        ))

from datetime import date

from django.contrib import admin
from django.utils.translation import gettext_lazy as _


class CustomQueryListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('Operations')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'query'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            ('Ready for Transcode', _('Ready for Transcode')),
            ('Ready for Video QC', _('Ready for Video QC')),
            ('Ready for SCC QC', _('Ready for SCC QC')),
            ('Ready to Prep', _('Ready to Prep')),
            ('Ready to Send', _('Ready to Send')),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        todays_date = datetime.now().date()
        date_future_one_week = datetime.now() + timedelta(days=7)
        date_prior_three_weeks = datetime.now() - timedelta(days=21)
        if self.value() == 'Ready for Transcode':

            return queryset.filter(playlist_posted__isnull=False,
                                   transcoded__isnull=True,
                                   delivery_date__gte=date_prior_three_weeks,
                                   delivery_date__lte=date_future_one_week,
                                   )

        if self.value() == 'Ready for Video QC':
            return queryset.filter(playlist_posted__isnull=False,
                                   transcoded__isnull=False,
                                   video_quality_check__isnull=True,
                                   delivery_date__gte=date_prior_three_weeks,
                                   delivery_date__lte=date_future_one_week)

        if self.value() == 'Ready for SCC QC':
            return queryset.filter(playlist_posted__isnull=False,
                                   transcoded__isnull=False,
                                   video_quality_check__isnull=False,
                                   cc_quality_check__isnull=True,
                                   source_filename__isnull=False,
                                   delivery_date__gte=date_prior_three_weeks,
                                   delivery_date__lte=date_future_one_week,
                                   )

        if self.value() == 'Ready to Prep':
            return queryset.filter(playlist_posted__isnull=False,
                                   transcoded__isnull=False,
                                   video_quality_check__isnull=False,
                                   cc_quality_check__isnull=False,
                                   in_queue__isnull=True,
                                   metadata_complete=True,
                                   transfer_complete=False,
                                   delivery_date__gte=date_prior_three_weeks,
                                   delivery_date__lte=date_future_one_week,
                                   )

        if self.value() == 'Ready to Send':
            return queryset.filter(playlist_posted__isnull=False,
                                   transcoded__isnull=False,
                                   video_quality_check__isnull=False,
                                   cc_quality_check__isnull=False,
                                   in_queue__isnull=False,
                                   metadata_complete=True,
                                   transfer_complete=False,
                                   delivery_date__gte=date_prior_three_weeks,
                                   delivery_date__lte=date_future_one_week,
                                   )


class RestrictionsDateRange(DateRangeFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('Restrictions Date Range')

    parameter_name = 'query'

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        self.title = 'Restrictions Date Range'
        start_date = request.GET.get('us_cox_pulldown_start__gte')
        end_date = request.GET.get('us_cox_pulldown_start__lte')

        queries = []
        if start_date:
            for terr in TERRITOR_LOOKUP:
                if terr[0] == request.GET.get('territories', 'US'):
                    for attr in terr[1]:
                        q = {str(attr)+'_start': start_date}
                        if request.GET.get('status') is not None:
                            q[str(attr)] = request.GET.get('status')
                        queries.append(Q(**q))
                    break
        
        if end_date:
            for terr in TERRITOR_LOOKUP:
                if terr[0] == request.GET.get('territories', 'US'):
                    for attr in terr[1]:
                        q = {str(attr)+'_end': end_date}
                        if request.GET.get('status') is not None:
                            q[str(attr)] = request.GET.get('status')
                        queries.append(Q(**q))
                    break

        if start_date and end_date:
            for terr in TERRITOR_LOOKUP:
                if terr[0] == request.GET.get('territories', 'US'):
                    for attr in terr[1]:
                        q = {str(attr)+'_start__gte': start_date, str(attr)+'_start__lte': end_date}
                        if request.GET.get('status') is not None:
                            q[str(attr)] = request.GET.get('status')
                        queries.append(Q(**q))
                    break

            for terr in TERRITOR_LOOKUP:
                if terr[0] == request.GET.get('territories', 'US'):
                    for attr in terr[1]:
                        q = {str(attr)+'_end__gte': start_date, str(attr)+'_end__lte': end_date}
                        if request.GET.get('status') is not None:
                            q[str(attr)] = request.GET.get('status')
                        queries.append(Q(**q))
                    break
        
        if not start_date and not end_date:
            for terr in TERRITOR_LOOKUP:
                if terr[0] == request.GET.get('territories', 'US'):
                    for attr in terr[1]:
                        if request.GET.get('status') is not None:
                            q = {str(attr): request.GET.get('status')}
                            queries.append(Q(**q))
                    break

        if queries:
            query = queries.pop()

            for item in queries:
                query |= item
            return queryset.filter(query)


class RestrictionsUpdatedDateRange(DateRangeFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('Restrictions Updated')

    parameter_name = 'query'

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        self.title = 'Restrictions Updated Date'
        start_date = request.GET.get('restrictions_update__gte')
        end_date = request.GET.get('restrictions_update__lte')

        if start_date:
            start_date = datetime.strptime(start_date, '%Y-%m-%d')

        if end_date:
            end_date = datetime.strptime(end_date, '%Y-%m-%d')

        if start_date and not end_date:
            return queryset.filter(restrictions_update__gte=datetime.combine(start_date, dtime.min),)
        
        if end_date and not start_date:
            return queryset.filter(restrictions_update__lte=datetime.combine(end_date, dtime.max),)

        if start_date and end_date:
            return queryset.filter(restrictions_update__range=(datetime.combine(start_date, dtime.min),
                                                               datetime.combine(end_date, dtime.max),))


class TerritoriesQueryListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('GeoRestrictions Status')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'status'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            ('', _('Blank')),
            ('Not Restricted', _('Not Restricted')),
            ('Never Allow', _('Never Allow')),
            ('Restricted', _('Restricted')),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if request.GET.get('territories') == 'Show Blank':

            return queryset.filter(
                us_cox_pulldown='',
                uk_svod_holdback='',
                uk_avod_holdback='',
                australia_svod_holdback='',
                australia_avod_holdback='',
                germany_svod_holdback='',
                germany_avod_holdback='',
                italy_svod_holdback='',
                italy_avod_holdback='',
                poland_svod_holdback='',
                poland_avod_holdback='',
                middleeast_svod_holdback='',
                middleeast_avod_holdback='',
                france_svod_holdback='',
                france_avod_holdback='',
                africa_svod_holdback='',
                africa_avod_holdback='',
                switzerland_svod_holdback='',
                switzerland_avod_holdback='',
                spain_svod_holdback='',
                spain_avod_holdback='',
                austria_svod_holdback='',
                austria_avod_holdback=''
            )
        else:
            return queryset

TERRITOR_LOOKUP = (
    ('US', (_('us_cox_pulldown'),)),
    ('UK', (_('uk_svod_holdback'),_('uk_avod_holdback'))),
    ('Australia', (_('australia_svod_holdback'),_('australia_avod_holdback'))),
    ('Germany', (_('germany_svod_holdback'),_('germany_avod_holdback'))),
    ('Italy', (_('italy_svod_holdback'),_('italy_avod_holdback'))),
    ('Poland', (_('poland_svod_holdback'),_('poland_avod_holdback'))),
    ('Middleeast', (_('middleeast_svod_holdback'),_('middleeast_avod_holdback'))),
    ('France', (_('france_svod_holdback'),_('france_avod_holdback'))),
    ('Africa', (_('africa_svod_holdback'),_('africa_avod_holdback'))),
    ('Switzerland', (_('switzerland_svod_holdback'),_('switzerland_avod_holdback'))),
    ('Spain', (_('spain_svod_holdback'),_('spain_avod_holdback'))),
    ('Austria', (_('austria_svod_holdback'),_('austria_avod_holdback'))),
)

class FilterTerritories(admin.SimpleListFilter):
    title = _('Territories')
    parameter_name = "territories"

    def lookups(self, request, model_admin):
        return TERRITOR_LOOKUP

    def choices(self, changelist):

        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == str(lookup),
                'query_string': changelist.get_query_string({self.parameter_name: lookup}, []),
                'display': lookup,
            }

    def queryset(self, request, queryset):
        all_territories = [
                'US',
                'UK',
                'Australia',
                'Germany',
                'Italy',
                'Poland',
                'Middleeast',
                'France',
                'Africa',
                'Switzerland',
                'Spain',
                'Austria']
        if self.value() and request.GET.get('status'):
            print(f'self.value = {self.value}')
            excluded_territories = self.value().split(",")
            queries = []
            for p in set(all_territories)&set(excluded_territories):
                for choice in self.lookup_choices:
                    if choice[0] == p:
                        for attr in choice[1]:
                            queries.append(Q(**{str(attr): request.GET.get('status')}))

            query = queries.pop()

            for item in queries:
                query |= item
            return queryset.filter(query)

def custom_titled_filter(title):
    class Wrapper(admin.FieldListFilter):
        def __new__(cls, *args, **kwargs):
            instance = admin.FieldListFilter.create(*args, **kwargs)
            instance.title = title
            return instance
    return Wrapper

class FilterParticipants(admin.SimpleListFilter):
    template = "participants_filter_template.html"
    title = _('Partners')
    parameter_name = "participants"

    def lookups(self, request, model_admin):
        return (
            ("amazon", _("amazon")),
            ("apple", _("apple")),
            ("google", _("google")),
            ("vudu", _("vudu")),
            ("sony", _("sony")),
            ("microsoft", _("microsoft")),
            ("fandangonow", _("fandangonow")),
            ("comcastest", _("comcastest")),
            ("mtod", _("mtod")),
            ("hulu", _("hulu")),
            ("kaleidescape", _("kaleidescape")),
        )

    def choices(self, changelist):

        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == str(lookup),
                'query_string': changelist.get_query_string({self.parameter_name: lookup}, []),
                'display': title,
            }

    def queryset(self, request, queryset):
        included_participants = request.GET.getlist('participants')
        queries = []
        for p in included_participants:
            if not p: continue
            queries.append(Q(**{p: True}))
        if queries:
            query = queries.pop()
            for item in queries:
                query |= item
            return queryset.filter(query)