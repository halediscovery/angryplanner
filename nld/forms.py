from django import forms
from .models import Plan
from .fields import ListTextWidget

class PlanForm(forms.ModelForm):
    char_field_with_list = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(PlanForm, self).__init__(*args, **kwargs)
        self.fields['char_field_with_list'].widget = ListTextWidget(data_list=Plan.GEO_BLOCK_CHOICES, name='geo-block-list')

    class Meta:
        model = Plan
        fields = ['asset_name']


