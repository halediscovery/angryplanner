# XML Generation Guide

## Base command
`python manage.py generate_xml`

# Options
`--settings` Django settings file to use, type: string, ie. angryplanner.settings.dev
`--fi` Django queryset filter, type: string, ie. {"hulu": true}
`--partner` Pertner name, type: string ie. hulu, sony, apple
`--templatepath` Directory of xml templates to use, type: string, ie. /home/user/xml_templates, default: ./nld/management/commands/templates
`--current_template` File name of template to use in templatepath directory for xml gen, type: string, ie. sony_addModify.xml, default [partner]_addModify_template_name
`--outputpath` Output directory for generated xml files, type: string, ie. /home/user/xml_output, default [partner]_addModify_location