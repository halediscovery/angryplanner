from django.shortcuts import render
from django.contrib import admin
from django import forms
from django.http import HttpResponseRedirect
from django.contrib.admin.helpers import ActionForm
from django.contrib.admin import DateFieldListFilter
from django.contrib.admin.filters import SimpleListFilter, AllValuesFieldListFilter
from django.utils.translation import gettext_lazy as _
from rangefilter.filter import DateRangeFilter
from datetime import datetime, timedelta
from django.utils import timezone
from import_export.admin import ExportMixin, ImportExportMixin
from import_export import resources
from .fields import ListTextWidget
from .models import Plan
from .admin import PlanAdmin
from .adminfilters import LogisticsFilter, CustomDateFieldListFilter, CustomQueryListFilter
from .adminresource import PlanResource, RestrictionResource

class RestrictionPlan(Plan):
    # actions = [update_mtod_fields]
    class Meta:
        verbose_name = "GeoRestriction"
        verbose_name_plural = "GeoRestrictions Scheduler"
        proxy = True

class PlanAdminForm(forms.ModelForm):
  class Meta:
    model = RestrictionPlan
    widgets = {
      'geo_block': ListTextWidget(data_list=Plan.GEO_BLOCK_CHOICES, name="geo-block"),
    }
    fields = '__all__'

class RestrictionPlanAdmin(PlanAdmin):
    def get_queryset(self, request):
        return self.model.objects.filter(mtod = True)

    form = PlanAdminForm
    resource_class = RestrictionResource
    list_display_links = ('propep',)
    search_fields = ('asset_name', 'asset_network', 'propep', 'asset_series', 'asset_episode', 'deliverable')
    fieldsets = [('Basics', {'fields':[('propep',),
                                        ('kaltura_source_id',),
                                        ( 'restrictions_update',),
                                       ('mtod',),
                                       ('mtod_start_date',)],'classes': ['grp-collapse', 'wide', 'extrapretty']}),
                 ('Restrictions', {'fields':[('us_cox_pulldown', 'us_cox_pulldown_start', 'us_cox_pulldown_end',),
                                             ('uk_svod_holdback', 'uk_svod_holdback_start', 'uk_svod_holdback_end',),
                                             ('uk_avod_holdback', 'uk_avod_holdback_start', 'uk_avod_holdback_end',),
                                             ('australia_svod_holdback', 'australia_svod_holdback_start', 'australia_svod_holdback_end',),
                                             ('australia_avod_holdback', 'australia_avod_holdback_start', 'australia_avod_holdback_end',),
                                             ('germany_svod_holdback', 'germany_svod_holdback_start', 'germany_svod_holdback_end',),
                                             ('germany_avod_holdback', 'germany_avod_holdback_start', 'germany_avod_holdback_end',),
                                             ('italy_svod_holdback', 'italy_svod_holdback_start', 'italy_svod_holdback_end',), 
                                             ('italy_avod_holdback', 'italy_avod_holdback_start', 'italy_avod_holdback_end',),
                                             ('poland_svod_holdback', 'poland_svod_holdback_start', 'poland_svod_holdback_end',),
                                             ('poland_avod_holdback', 'poland_avod_holdback_start', 'poland_avod_holdback_end',),
                                             ('middleeast_svod_holdback', 'middleeast_svod_holdback_start', 'middleeast_svod_holdback_end',),
                                             ('middleeast_avod_holdback', 'middleeast_avod_holdback_start', 'middleeast_avod_holdback_end',),
                                             ('france_svod_holdback', 'france_svod_holdback_start', 'france_svod_holdback_end',),
                                             ('france_avod_holdback', 'france_avod_holdback_start', 'france_avod_holdback_end',),
                                             ('africa_svod_holdback', 'africa_svod_holdback_start', 'africa_svod_holdback_end',),
                                             ('africa_avod_holdback', 'africa_avod_holdback_start', 'africa_avod_holdback_end',),
                                             ('switzerland_svod_holdback', 'switzerland_svod_holdback_start', 'switzerland_svod_holdback_end',),
                                             ('switzerland_avod_holdback', 'switzerland_avod_holdback_start', 'switzerland_avod_holdback_end',),
                                             ('spain_svod_holdback', 'spain_svod_holdback_start', 'spain_svod_holdback_end',),
                                             ('spain_avod_holdback', 'spain_avod_holdback_start', 'spain_avod_holdback_end',),
                                             ('austria_svod_holdback', 'austria_svod_holdback_start', 'austria_svod_holdback_end',),
                                             ('austria_avod_holdback', 'austria_avod_holdback_start', 'austria_avod_holdback_end',),
                                             ('mtod_publish_flag'), ('geo_block'), ('restrictions_update')], 'classes': ['grp-collapse', 'wide', 'extrapretty']}),
                 ('Notes', {'fields': ['notes'],
                            'classes': ['wide','extrapretty']}),]
    readonly_fields = ['propep', 'mtod', 'restrictions_update',]
    #action_form = MarkUpdatesForm
    actions = []
    # actions = [update_mtod_fields]

    def get_list_display(self, request):
        return ('delivery_date', 'propep', 'asset_series', 'c_seson_num', 'asset_episode',
                        'asset_network', 'kaltura_source_id', 'geo_block', 'restrictions_update',) 