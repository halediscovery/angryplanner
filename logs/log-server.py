#!/usr/bin/env python3
from http.server import HTTPServer, BaseHTTPRequestHandler
import os
 
 
class StaticServer(BaseHTTPRequestHandler):
 
    def do_GET(self):
        currentfolder = os.path.dirname(os.path.abspath(__file__))
        parentfolder = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        #print(f"parentfolder is {parentfolder}")
        #print(f"currentfolder is {currentfolder}")
        #print(f"file path is os.path.abspath is {os.path.abspath(__file__)}")
        #print(self.path)
        if self.path == '/':
            filename = currentfolder + '/index.html'
        else:
            filename = parentfolder + self.path
 
        self.send_response(200)
        if filename[-4:] == '.log':
            self.send_header('Content-type', 'text/text')
        elif filename[-4:] == '.csv':
            self.send_header('Content-type', 'text/plain')
        elif filename[-4:] == '.css':
            self.send_header('Content-type', 'text/css')
        elif filename[-5:] == '.json':
            self.send_header('Content-type', 'application/javascript')
        elif filename[-3:] == '.js':
            self.send_header('Content-type', 'application/javascript')
        elif filename[-4:] == '.ico':
            self.send_header('Content-type', 'image/x-icon')
        else:
            self.send_header('Content-type', 'text/html')
        self.end_headers()
        with open(filename, 'rb') as fh:
            html = fh.read()
            #html = bytes(html, 'utf8')
            self.wfile.write(html)
 
def run(server_class=HTTPServer, handler_class=StaticServer, port=8080):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print('Starting httpd on port {}'.format(port))
    httpd.serve_forever()
 
run()