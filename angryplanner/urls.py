"""dj110 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.urls import path, include, re_path
from django.views.generic.base import RedirectView


urlpatterns = [
    path('grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='/admin')),
    url(r'^nld/', include('nld.urls')),
    path('okta_auth/', include('okta_auth.urls')),
    url(r'^health_check/', include('health_check.urls')),
    #('^activity/', include('actstream.urls')),
    url(r'^snowcone/', include('snowcone.urls')),
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        re_path(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns


# Change admin site title
admin.site.site_header = _("Angry Planner")
admin.site.site_title = _("Angry Planner")