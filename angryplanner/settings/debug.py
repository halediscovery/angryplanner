# Debug settings

from .base import *
import os
import environ

env = environ.Env(
    SECRET_KEY=str,
    DEBUG=(bool, False),
    ALLOWED_HOSTS=(list, ['*']),
    DATABASE_URL=str,)

env.read_env(os.environ['DJANGO_ENV_FILE']) # grabbing the local env

DEBUG = env('DEBUG')
ALLOWED_HOSTS = env('ALLOWED_HOSTS')
INTERNAL_IPS = ['127.0.0.1']
SECRET_KEY = env('SECRET_KEY') # Raises ImproperlyConfigured exception if SECRET_KEY not in os.environ


# add the debug toolbar for ease of debugging and development
INSTALLED_APPS += [
        'debug_toolbar',
    ]

MIDDLEWARE += [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': env.db(), # Raises ImproperlyConfigured exception if DATABASE_URL not in os.environ
    'extra': env.db('SQLITE_URL', default='sqlite:////tmp/my-tmp-sqlite.db')
}

#
# STATIC_URL = 'http://planner.us.dci.discovery.com:80./static/'