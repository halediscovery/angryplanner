# Debug settings

from .base import *
import os
import environ

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = True
ALLOWED_HOSTS = ['*']
INTERNAL_IPS = ['127.0.0.1']
SECRET_KEY = 't)a7x1@#jgr__5fvz0=u00fe_70(*($(0+wswv$t0hp3tz$2)1' # Raises ImproperlyConfigured exception if SECRET_KEY not in os.environ


# add the debug toolbar for ease of debugging and development
INSTALLED_APPS += [
        'debug_toolbar',
    ]

MIDDLEWARE += [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'angryplanner-uat',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}


# as declared in NginX conf, it must match /opt/services/djangoapp/static/
STATIC_ROOT = os.path.join(os.path.dirname(os.path.dirname(BASE_DIR)), 'static')

# do the same for media files, it must match /opt/services/djangoapp/media/
MEDIA_ROOT = os.path.join(os.path.dirname(os.path.dirname(BASE_DIR)), 'media')

STATIC_URL = '/static/'