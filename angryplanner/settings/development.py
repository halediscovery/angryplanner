from .base import *
import environ
# SECURITY WARNING: don't run with debug turned on in production!


env = environ.Env(
    SECRET_KEY=str,
    DEBUG=(bool, False),
    ALLOWED_HOSTS=(list, ['*']),
    DATABASE_URL=str,)

env.read_env(os.environ['DJANGO_ENV_FILE']) # grabbing the local env

DEBUG = env('DEBUG')
ALLOWED_HOSTS = env('ALLOWED_HOSTS')
INTERNAL_IPS = ['127.0.0.1']
SECRET_KEY = env('SECRET_KEY') # Raises ImproperlyConfigured exception if SECRET_KEY not in os.environ


DATABASES = {
    'default': env.db(), # Raises ImproperlyConfigured exception if DATABASE_URL not in os.environ
}

STATIC_URL = 'http://planner.us.dci.discovery.com:8082/static/'