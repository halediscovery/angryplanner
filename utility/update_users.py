import sys
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

#  create the users in this list.
# run in manage.py shell - exec(open('utility/update_users.py').read())
#  Each tuple represents the username, password, and email of a user.

usergroup = Group()
usergroup.name = 'Users'
usergroup.save()

User = get_user_model()

users = [
    ('Nermin_Gad_Foundas@discovery.com', '1234', 'Nermin_Gad_Foundas@discovery.com'),
    ('James_Reed@discovery.com', '1234', 'James_Reed@discovery.com'),
    ('Anthony_Schach@discovery.com', '1234', 'Anthony_Schach@discovery.com'),
    ('Naveen_Nallathambi@discovery.com', '1234', 'Naveen_Nallathambi@discovery.com'),
    ('Kamesha_Myers@discovery.com', '1234', 'Kamesha_Myers@discovery.com'),
    ('Richard_Chanin@discovery.com', '1234', 'Richard_Chanin@discovery.com'),
    ('Josh_Pasquariello@discovery.com', '1234', 'Josh_Pasquariello@discovery.com'),
    ('Meg_Fletcher@discovery.com', '1234', 'Meg_Fletcher@discovery.com'),
    ('Julia_John@discovery.com', '1234', 'Julia_John@discovery.com'),
    ('Kareema_Gadsden@discovery.com', '1234', 'Kareema_Gadsden@discovery.com'),
    ('Brinton_Miller@discovery.com', '1234', 'Brinton_Miller@discovery.com'),
    ('Alexander_Rybacki@discovery.com', '1234', 'Alexander_Rybacki@discovery.com'),
    ('Alvaro_Velandia@discovery.com', '1234', 'Alvaro_Velandia@discovery.com'),
    ('Azariah_Greene@discovery.com', '1234', 'Azariah_Greene@discovery.com'),
    ('Barbara_Percival@discovery.com', '1234', 'Barbara_Percival@discovery.com'),
    ('Brian_Cannaday@discovery.com', '1234', 'Brian_Cannaday@discovery.com'),
    ('Chantel_Hernandez@discovery.com', '1234', 'Chantel_Hernandez@discovery.com'),
    ('Chris_Bell@discovery.com', '1234', 'Chris_Bell@discovery.com'),
    ('Daniel_Tims@discovery.com', '1234', 'Daniel_Tims@discovery.com'),
    ('Eric_Boehnlein@discovery.com', '1234', 'Eric_Boehnlein@discovery.com'),
    ('Jason_Webb@discovery.com', '1234', 'Jason_Webb@discovery.com'),
    ('Justin_Smith@discovery.com', '1234', 'Justin_Smith@discovery.com'),
    ('Kurt_Walther@discovery.com', '1234', 'Kurt_Walther@discovery.com'),
    ('Lawrence_Jointer@discovery.com', '1234', 'Lawrence_Jointer@discovery.com'),
    ('Liz_Dalmas@discovery.com', '1234', 'Liz_Dalmas@discovery.com'),
    ('Marcia_Garcia@discovery.com', '1234', 'Marcia_Garcia@discovery.com'),
    ('Rachel_Smero@discovery.com', '1234', 'Rachel_Smero@discovery.com'),
    ('Rashida_Preddie@discovery.com', '1234', 'Rashida_Preddie@discovery.com'),
    ('Richard_McNall@discovery.com', '1234', 'Richard_McNall@discovery.com'),
    ('RJ_Loubier@discovery.com', '1234', 'RJ_Loubier@discovery.com'),
    ('Steve_Hernandez@discovery.com', '1234', 'Steve_Hernandez@discovery.com'),
    ('Taylor_Wev@discovery.com', '1234', 'Taylor_Wev@discovery.com'),
    ('Thomas_Manzanares@discovery.com', '1234', 'Thomas_Manzanares@discovery.com'),
    ('Tom_Hale@discovery.com', '1234', 'Tom_Hale@discovery.com'),
    ('Zachary_Smero@discovery.com', '1234', 'Zachary_Smero@discovery.com'),
    ('Sai_Singamneni@discovery.com', '1234', 'Sai_Singamneni@discovery.com'),
    ('Abby_Mergenmeier@discovery.com', '1234', 'Abby_Mergenmeier@discovery.com'),
    ('Amy_Scholley@discovery.com', '1234', 'Amy_Scholley@discovery.com'),
    ('Cory_McLaughlin@discovery.com', '1234', 'Cory_McLaughlin@discovery.com'),
    ('Jamal_Francis@discovery.com', '1234', 'Jamal_Francis@discovery.com'),
    ('Julianne_Maxwell@discovery.com', '1234', 'Julianne_Maxwell@discovery.com'),
    ('Michael_Kramer@discovery.com', '1234', 'Michael_Kramer@discovery.com'),
    ('Russell_Bushey@discovery.com', '1234', 'Russell_Bushey@discovery.com'),
    ('William_Healy@discovery.com', '1234', 'William_Healy@discovery.com'),
    ('Chantel_Ashley@discovery.com', '1234', 'Chantel_Ashley@discovery.com'),
    ('Elyssa_Vogelhut@discovery.com', '1234', 'Elyssa_Vogelhut@discovery.com'),
    ('Eugene_Dixon@discovery.com', '1234', 'Eugene_Dixon@discovery.com'),
    ('Joanna_Lloyd@discovery.com', '1234', 'Joanna_Lloyd@discovery.com'),
    ('Lisa_Servedio@discovery.com', '1234', 'Lisa_Servedio@discovery.com'),
    ('Margaret_Metz@discovery.com', '1234', 'Margaret_Metz@discovery.com'),
    ('Melanie_Catts@discovery.com', '1234', 'Melanie_Catts@discovery.com'),
    ('Michael_Galbraith@discovery.com', '1234', 'Michael_Galbraith@discovery.com'),
    ('Rachel_Reid@discovery.com', '1234', 'Rachel_Reid@discovery.com'),
    ('Nerissa_Turner@discovery.com', '1234', 'Nerissa_Turner@discovery.com'),
    ('Ben_Blomfield@discovery.com', '1234', 'Ben_Blomfield@discovery.com'),
    ('Kathryn_Richardson@discovery.com', '1234', 'Kathryn_Richardson@discovery.com'),
    ('Michael_Hale@discovery.com', '1234', 'Michael_Hale@discovery.com')
]



group = Group.objects.get(name='Users')

for username, password, email in users:
    try:
        print(f'Creating user { username }.')
        user = User.objects.create_user(username=username, email=email, is_staff=True)
        user.set_password(password)
        user.groups.add(group)
        user.save()
        assert authenticate(username=username, password=password)
        print(f'User { username } successfully created.')

    except:
        print(f'There was a problem creating the user: {username}.  Error: {sys.exc_info()[1]}.')

User.objects.create_superuser('admin', 'angryplanneradmin@discovery.com', 'B0gu5dayz')
User.objects.create_superuser('mhale', 'm@m.com', '4InterneT1')