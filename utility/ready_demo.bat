del debug-sqlite3.db
rem this doesn't in Jenkins
manage.py makemigrations
pause
rem run during build build
manage.py migrate
pause
rem build
manage.py collectstatic
pause
rem this doesn't happen in Jenkins
manage.py createsuperuser
pause
rem this doesn't happen in Jenkins
manage.py startmonitoring
pause
rem this starts the web service
manage.py runserver
pause