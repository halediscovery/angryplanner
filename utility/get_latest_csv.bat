e:
echo # Start >> e:\csv_importlogtimes.txt
time /t >> e:\csv_importlogtimes.txt

cd \AngryEncodePlanner\angryplanner.dev\angryplanner
set DJANGO_SETTINGS_MODULE=angryplanner.settings.dev
python e:\AngryEncodePlanner\angryplanner.dev\angryplanner\nld\management\latest_episode.py --s3


echo # Midpoint >> e:\csv_importlogtimes.txt

cd \AngryEncodePlanner\angryplanner.uat\angryplanner
set DJANGO_SETTINGS_MODULE=angryplanner.settings.uat
python e:\AngryEncodePlanner\angryplanner.uat\angryplanner\nld\management\latest_episode.py --s3

time /t >> e:\csv_importlogtimes.txt
echo # End >> e:\csv_importlogtimes.txt