-- Database: angryplanner

DROP DATABASE IF EXISTS angryplanner;

CREATE DATABASE angryplanner
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
	
	
CREATE USER angrylibrarian with PASSWORD '12librarians';

ALTER ROLE angrylibrarian SET client_encoding TO 'utf8';
ALTER ROLE angrylibrarian SET default_transaction_isolation TO 'read committed';
ALTER ROLE angrylibrarian SET timezone TO 'UTC';

GRANT ALL PRIVILEGES ON DATABASE angryplanner TO angrylibrarian;