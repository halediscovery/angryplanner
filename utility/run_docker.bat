docker run --name postgres --network postgres-network -e POSTGRES_PASSWORD=********* -d postgres
docker create -v /var/lib/postgresql/data --name PostgresData alpine
docker run -p 5432:5432 --name yourContainerName -e POSTGRES_PASSWORD=yourPassword -d --volumes-from PostgresData postgres