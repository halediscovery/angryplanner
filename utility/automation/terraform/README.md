## Database Creation and Population

The creation of the RDS instance for the Angry Planner application is created using the Terraform templates found in `angryplanner/utility/automation/terraform`. The main files are:

* `database.tf`
* `variables.tf`
* `angryplanner_tags.tf`
* `angryplanner_db.sql`
* `angryplanner_db_pre.sql`

The `angryplanner_db.sql` file is basically the backup SQL file dumped from an existing AngryPlanner Postgres DB. The `angryplanner_db_pre.sql` is required to set up the **postgres** user, which is actually the master user of the RDS instance. This is required since the export of the data sets the owner of all the database objects to **postgres**.

Since no Terraform files (state or otherwise) are checked into the repository, these are the steps to create the RDS instance should there be a need in the future.

1. In the `angryplanner/utility/automation/terraform` directory, initialize the environment for Terrafrom:
> ```$ terraform init```

2. Set verify the plan first (dry-run):
> ```$ terraform plan --var environment=prod --var rds_master_password=pl4n4ngry!```

3. When all checks out, apply the plan:
> ```$ terraform apply --var environment=prod --var rds_master_password=pl4n4ngry!```

4. Sit back and enjoy!

The password provided for the **postgres** user should be the same one found in the `angryplanner/.productionenv` file. If this user is not to be used as the application user, then the `database.tf` file should include any addition SQL scripts to be run to create those users or modify the `angryplanner_db_pre.sql` to include the users/roles.

