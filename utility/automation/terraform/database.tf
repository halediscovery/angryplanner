
# setup a rds instance

resource "aws_db_instance" "angryplanner_db" {
  allocated_storage         = 20
  storage_type              = "gp2"
  engine                    = "postgres"
  engine_version            = "10.6"
  instance_class            = "db.t2.micro"
  identifier                = "${format("%s-%s-angryplanner", var.environment, var.tag_business_unit["value"])}"
  name                      = "angryplanner"
  username                  = "postgres"
  password                  = "${var.rds_master_password}"
  parameter_group_name      = "default.postgres10"
  option_group_name         = "default:postgres-10"
  db_subnet_group_name      = "${lookup(var.db_subnet_group_name, var.environment)}"
  vpc_security_group_ids    = ["${split(",", lookup(var.postgres_vpc_security_group, var.environment))}"]
  final_snapshot_identifier = "${format("%s-%s-angryplanner-backup", var.environment, var.tag_business_unit["value"])}"
  skip_final_snapshot       = true

  tags = {
    Owner        = "${lookup(var.tag_owner, "value")}"
    BusinessUnit = "${lookup(var.tag_business_unit, "value")}"
    Environment  = "${var.environment}"
    Description  = "${lookup(var.tag_description, "value")}"
  }

}


# establish the database user and owner

resource "null_resource" "db_setup_1" {
  depends_on = ["aws_db_instance.angryplanner_db"]

  provisioner "local-exec" {
    command = "PGPASSWORD=${var.rds_master_password} psql -h ${aws_db_instance.angryplanner_db.address} -U ${aws_db_instance.angryplanner_db.username} ${aws_db_instance.angryplanner_db.name} < angryplanner_db_pre.sql"
  }

}

resource "null_resource" "db_setup_2" {
  depends_on = ["aws_db_instance.angryplanner_db", "null_resource.db_setup_1"]

  provisioner "local-exec" {
    command = "PGPASSWORD=${var.rds_master_password} psql -h ${aws_db_instance.angryplanner_db.address} -U ${aws_db_instance.angryplanner_db.username} ${aws_db_instance.angryplanner_db.name} < angryplanner_db.sql"
  }

}



# outputs

output "angryplanner_db_endpoint" {
  value = ["${aws_db_instance.angryplanner_db.endpoint}"]
}

