variable "environment" {}

variable "region" {
  default = "us-east-1"
}

variable "key_name" {
  type = "map"
  default = {
    "dev"   = "kpd16182"
    "int"   = "kpi16182"
    "qa"    = "kpq16182"
    "prod"  = "kpp16182"
    "admin" = "kpa17173"
  }
}

variable "security_groups" {
  type = "map"
  default = {
    "dev"   = "sg-00395478,sg-06b9067e,sg-0f617359950a91c2d"
    "int"   = "sg-084a0f1e3cc3496ee,sg-0cb80774,sg-db036ea3"
    "qa"    = "sg-0affbc71,sg-bdf4b7c6"
    "prod"  = "sg-94ffbcef,sg-d0febdab"
    "admin" = "sg-08d6eb8bc8fcaab18,sg-688d451a,sg-db73baa9"
  }
}

variable "rds_master_password" {}

variable "postgres_vpc_security_group" {
  type = "map"
  default = {
    "dev"   = "sg-419bf639"
    "int"   = "sg-c19af7b9"
    "qa"    = "sg-f5a9988e"
    "prod"  = "sg-44b3b23f"
    "admin" = "sg-5921792b"
  }
}

variable "db_subnet_group_name" {
  type = "map"
  default = {
    "dev"   = ""
    "int"   = ""
    "qa"    = "mcd-qa-vpc-rdsprivatevpcsubnets-tqawwy7yey3b"
    "prod"  = "mcd-prod-vpc-rdsprivatevpcsubnets-r8fmiecilgeq"
    "admin" = "mcd-admin-vpc-0-rdsprivatevpcsubnets-87vgki1q509c"
  }
}

variable "private_subnets" {
  type = "map"
  default = {
    "dev"   = "subnet-bdef03e5,subnet-02c3dd29,subnet-77428801"
    "int"   = "subnet-66ef033e,subnet-304d8746,subnet-46c2dc6d"
    "qa"    = "subnet-f55c6183,subnet-4f6e2017,subnet-c1ce8ceb"
    "prod"  = "subnet-c9635ebf,subnet-39531d61,subnet-20c88a0a"
    "admin" = "subnet-38755862,subnet-969c64a9,subnet-05f7c87b0c8dd406a,subnet-0aacfe06"
  }
}

variable "public_subnets" {
  type = "map"
  default = {
    "dev"   = "subnet-beef03e6,subnet-03c3dd28,subnet-74428802"
    "int"   = "subnet-67ef033f,subnet-45c2dc6e,subnet-334d8745"
    "qa"    = "subnet-f25c6184,subnet-4d6e2015,subnet-c6ce8cec"
    "prod"  = "subnet-c8635ebe,subnet-07531d5f,subnet-21c88a0b"
    "admin" = "subnet-bd7459e7,subnet-b590688a,subnet-fbd684f7"
  }
}

variable "ssl_cert" {
  type = "map"
  default = {
    "dev"    = "arn:aws:iam::447434275168:server-certificate/mamcloud-dev-2018-07"
    "int"    = "arn:aws:iam::447434275168:server-certificate/mamcloud-int-2018-07"
    "qa"     = "arn:aws:iam::250312325083:server-certificate/mamcloud-qa-2018-07"
    "prod"   = "arn:aws:iam::250312325083:server-certificate/mamcloud-2018-07"
    "admin"  = "arn:aws:iam::250312325083:server-certificate/mamcloud-admin-2018-07"
  }
}

variable "public_web_access" {
  type = "map"
  default = {
    "dev"   = "sg-8bd6b4f3"
    "int"   = "sg-1644296e"
    "qa"    = "sg-0532a58660a559c11"
    "prod"  = "sg-07a7439dfaf7acc4e"
    "admin" = "sg-"
  }
}

variable "vpc_id" {
  type = "map"
  default = {
    "dev"   = "vpc-deb820ba"
    "int"   = "vpc-b8871fdc"
    "qa"    = "vpc-a16fcac6"
    "prod"  = "vpc-1262c775"
    "admin" = "vpc-d2868dab"
  }
}

variable "vpc_cidr_block" {
  type = "map"
  default = {
    "dev"    = "10.50.48.0/20"
    "int"    = "10.50.32.0/20"
    "qa"     = "10.50.16.0/20"
    "prod"   = "10.50.0.0/20"
    "admin"  = "10.50.244.0/22"
  }
}

