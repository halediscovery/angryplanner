# tags for the resources

variable "tag_owner" {
  type = "map"
  default = {
    key                 = "Owner"
    value               = "dl-mcdautomation@discovery.com"
    propagate_at_launch = true
  }
}

variable "tag_business_unit" {
  type = "map"
  default = {
    key                 = "BusinessUnit"
    value               = "automation"
    propagate_at_launch = true
  }
}

variable "tag_description" {
  type = "map"
  default = {
    key                 = "Description"
    value               = "AngryPlanner application"
    propagate_at_launch = true
  }
}


