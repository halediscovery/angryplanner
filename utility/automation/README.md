## Overview

The scripting is designed to create a stack for an application, allow for a simple deployment of upgrades to the application, and provides a mechanism to remove the application stack completely.

For the deployment process, a **userdata** file is the component which provides instructions for specific application provisioning (installing dependencies) and for starting the application. The **userdata** file is in a simple BASH script format, however it can be constructed to be any scripting language provided the language is installed on the AMI. In the case of the AngryPlanner, the scripting in BASH.

This process requires a few things to properly execute. The scripting uses a profile which has within it an AWS role with permissions to create resources in the MCD AWS accounts, both dev and prod. Currently, this role is the same one used by our Jenkins housed in our admin VPC. To run these scripts locally requires an AWS account with access to assume this role. Another option would be to use an EC2's instance profile to assume the role to deploy. The MCD Jenkins executors are built with such instance profiles. That means, these scripts can be set up to run within our Jenkins server as a job to both establish and deploy.

Regarding the profile, this is the file that contains AWS resource specific metadata with a few data specific to the AWS accounts. For instance, default security groups, ELB listener mappings, and domain names are specified in this file. Without this file, none of the scripting knows how to create the resources for the stack. A profile is constructed for an application for an AWS account. The `create-stack`, `deploy`, and `delete-stack` tools handle creating those resources fine tuned to the profile details.

The only non-standard Python modules required for these tools are `boto3` and `botocore`.

The tools create the following AWS resources:

* Launch Configuration
* Autoscaling Group
* Elastic Load Balancer (ELB)
* Route53 DNS record
* Security group

The EC2 instances are created by the autoscaling group and assigned to the ELB. The Route53 DNS record is created to point to the ELB DNS name. The security group created is specific to the application for specific use purposes. (Predefined security groups allowing SSH and web access to the application are established in the profile mentioned above.)

In preparation for running these scripts, perform the following to prepare the environment:

```
$ pip install -r requirements.txt
$ export PATH=$PATH:.
```

There are three simple tools:

* `create-stack`
* `build`
* `deploy`
* `delete-stack`

Detailed instructions on use of these is presented below.


## create-stack

This script simulates the actions of a CloudFormation template standing up and assigning AWS resources without a CloudFormation template. It was designed to create these structures without an entry in the CloudFormation registry which could be accidentally deleted. (These were created before AWS added the termination protection to CloudFormation stacks.)

In order to use the `create-stack` script, a project profile file in Python format, and a `user-data` file. Usage of the script can be seen by using the `-h` argument.

```
$ create-stack -h
usage: create-stack [-h] [-d] -n NAME -e ENVIRONMENT -u USER_DATA -p
                    PROJECT_PROFILE

optional arguments:
  -h, --help            show this help message and exit
  -d, --debug           Run in debug mode
  -n NAME, --name NAME  Resource name (i.e. mcd-test-app)
  -e ENVIRONMENT, --environment ENVIRONMENT
                        Environment (i.e. dev, int, qa, prod, admin)
  -u USER_DATA, --user-data USER_DATA
                        User-data filename for Launch Configuration
  -p PROJECT_PROFILE, --project-profile PROJECT_PROFILE
                        Location of the profile defaults (Python formatted)
```


Then `--name` argument needs to include the **business unit** and the **application name**. The `--environment` value will be prepended to the `name` to make the resource name which will be applied to all the AWS resources.

The `--user-data` argument is the filename with full path of the userdata script for the instances. The `--profile-path` value needs only to be the path where the `profile_defaults.py` file is. The file should be called exactly `profile_defaults.py`.

The stack creation script will use the API to build the components for a stack. The elements are:

- launch configuration
- autoscaling group
- elastic load balancer (optional)

The `environment` will control which VPC is the destination for the stack and will fill in all the background bits necessary for the stack.


### project profile

The project profile file needs the most focus in order to correctly stand up a stack and is also used when destroying a stack. This file needs to be Python-formatted file as it is parsed that the values might be easily accessed. Although the project profile file is lightly commented within the file, a more extensive explanation of the parameters is below:

#### `TAGS`
Tags are in a JSON format of `"Name": "Value"` separated by commas. These will be the AWS tags for the resources where applicable.

#### `ROLE_NAME`
A role which has the authority to create and destroy objects in the stack is necessary. This role will be assumed by the scripts to make the necessary connections and API calls.

#### `SECURITY_GROUPS`
Although this isn't something we normally allow our applications to designate, it is available in these scripts asssuming engineers will be using these to create stacks. The values should be a list of strings of the security group names, not the security group ids.

#### `IMAGE_ID`
The AMI id to use as the baseline image.

#### `INSTANCE_TYPE`
This is the size of the instances to spin (t2.small, m5.large).

#### `MIN_SIZE`, `DESIRED_SIZE`, `MAX_SIZE`
These are numeric values which are set on the autoscaling group resource for low, intended, and upper limits of the number of instances which should be running. Refer to the AWS documentation on Autoscaling Groups for more details on how these are implemented.

#### `HEALTH_CHECK_TYPE`
This will either be EC2 or ELB and indicate where to look for the evaluation of the health check.

#### `HEALTH_CHECK_GRACE_PERIOD`
Number of seconds to allow before checking for the health of a stack.

#### `INGRESS_PERMISSIONS`
This is a list of JSON arrays to define permissions to allow for a mounted EFS volume. Because EFS is a type of NFS, the port must be opened for security purposes.

#### `USE_ELB`
A boolean for the create stack script to indicate if the stack will require the creation of an elastic load balancer. The values are python True and False keywords.

#### `ELB_SCHEME`
Sets the ELB to be an internal load balancer, which is the preferred method for our non-public services. Values are "internal" or to comment out the variable.

#### `ELB_LISTENERS`
Define the listeners for the ELB which are the frontend to backend port and protocol assignments. The values are JSON arrays in a list.

#### `REGISTER_ELB_WITH_DNS`
Boolean value to tell create stack to assign the ELB DNS name to a Route 53 DNS entry.

#### `DNS_NAME`
The value for the Route 53 entry if registering the ELB with DNS (above setting set to True). This will be what is registered in the hosted zone matching DOMAIN. Think of it as a subdomain name.

#### `DOMAIN`
The registered hosted zone for the AWS account/environment in Route 53. The DNS_NAME will be prepended to make up the full DNS name fronting the ELB DNS name.

#### `EBS_PROFILE`
This is the construct to allow for attaching EBS volumes. It is a list of JSON arrays defining the volume attributes and the mount points. Omit or comment out this variable if not necessary.

#### `VPC_FILTER_STRING`
This is the starting IP address assigned to the VPC. It is used to filter the list of VPC's as an alternative when identifying the correct VPC in which the stack resides to delete.


## build
This script will build the application in the sense that it will fetch the code from the repository, create a tarball, and upload it to an artifact repository.

```
$ build -h
usage: build [-h] [-d] -n APP_NAME [-r REPOSITORY] [-b BRANCH] -v VERSION
             [-a ARTIFACT_DESTINATION] [-c ARTIFACT_CREDENTIALS]

optional arguments:
  -h, --help            show this help message and exit
  -d, --debug           Run in debug mode
  -n APP_NAME, --app-name APP_NAME
                        Name of the application (without version) for the name
                        of the bundle
  -r REPOSITORY, --repository REPOSITORY
                        Git-based repository url
  -b BRANCH, --branch BRANCH
                        Branch of the code from the repo (--repository)
  -v VERSION, --version VERSION
                        Version of the application to append to the bundle
  -a ARTIFACT_DESTINATION, --artifact-destination ARTIFACT_DESTINATION
                        Url of the artifactory; add custom name or end with
                        slash to use bundle name
  -c ARTIFACT_CREDENTIALS, --artifact-credentials ARTIFACT_CREDENTIALS
                        Username and password joined by a colon (i.e.
                        user:pass)
```

The `build` command will clone the code from the provided repository (`--repository`) into a temporary directory. The immutable default parent path for the temporary directory is `/tmp/build`. The application name (`--app-name`) is appended to that path. The code is cloned into that directory. The default branch will be **master**, but a specific branch can be indicated using the `--branch` argument.

Once the code is cloned, the script will create a tarball in the `/tmp/build` directory making the filename based on the `--app-name` and the `--version` if that is provided with a `.tgz` extension.

This tarball is then uploaded to the artifact repository of your choice. Currently, it has been tested to upload to jfrog's Artifactory. The destination (`--artifact-destination`) is the URL with complete path to the repository folder. The tarball filename is used as the artifact name uploaded. It should be noted that because of the versioning features of Artifactory, special permissions would be required to overwrite an existing artifact. Thus, the use of the version information is important.

This script is designed to work in conjunction with the deploy tool in the sense that the **userdata** script pulls the application from Artifactory for deployment. Of course, the **userdata** script is merely BASH, so that can be adjusted to pull the application from anywhere. (See notes for the `deploy` script below.) The script could be modified with a little bit of testing to upload to an S3 bucket for the **userdata** to pull for deployment.


## deploy
This script will deploy the application running in the stack created by the `create-stack` above. It requires all the same parameters and files that the `create-stack` does.

```
$ deploy -h
usage: deploy [-h] [-d] -n NAME -e ENVIRONMENT -u USER_DATA -p PROJECT_PROFILE

optional arguments:
  -h, --help            show this help message and exit
  -d, --debug           Run in debug mode
  -n NAME, --name NAME  Resource name (i.e. mcd-test-app)
  -e ENVIRONMENT, --environment ENVIRONMENT
                        Environment (i.e. dev, int, qa, prod, admin)
  -u USER_DATA, --user-data USER_DATA
                        User-data filename for Launch Configuration
  -p PROJECT_PROFILE, --project-profile PROJECT_PROFILE
                        Location of the profile defaults (Python formatted)
  -k, --keep-old-launch-config
                        Keeps old Launch Configuration (default is to NOT
                        keep)
```
The process of deploying utilizes the **userdata** file which is what defines the specific application dependencies and indicates where to find the application bundle, how and where to unpack it, and the start up process for the application.

Because the **userdata** file is a key element for defining the application, the Launch Configuration for the stack will be replaced (since updating **userdata** is not an option). The current launch configuration data will be read for a new launch configuration using the **userdata** file specified by the `--user-data` argument. A new launch configuration will be created and assigned to the existing autoscaling group.

Once the new launch configuration is attached, the `deploy` will scale the autoscaling group up to spin up new instances with the latest version of the application. When new instances are running, the process scales the autoscaling group back down again restoring the original size values intended for the stack.

If a rollback is necessary, only specifying an older version of the application in a **userdata** file and repeating the deploy process.

The `--keep` argument is designed to intentionally keep the previous launch configuration. The default is to remove it after a new launch configuration is created and assigned to the autoscaling group. Using this flag will preserve it to manually reassign it back to the autoscaling group for an additional rollback option.



## delete-stack
This script is used to remove a stack created by the `create-stack.py` script. It utilizes the same project profile file to get necessary values based on the `name` and `environment` to pinpoint the exact stack.

Like the `create-stack` script, usage can be viewed by using the `-h` argument.

```
$ delete-stack -h
usage: delete-stack [-h] -n NAME -e ENVIRONMENT -p PROJECT_PROFILE

optional arguments:
  -h, --help            show this help message and exit
  -n NAME, --name NAME  Resource name (i.e. mcd-test-app)
  -e ENVIRONMENT, --environment ENVIRONMENT
                        Environment (i.e. dev, int, qa, prod)
  -p PROJECT_PROFILE, --project-profile PROJECT_PROFILE
                        Location of the profile defaults (Python formatted)
```

It should be noted that the resource name, which is made up from the `name` and `environment` variables as the primary method for searching for the resources to delete. **If any of those original resources have been replaced with names different than originally created, it will fail to delete those objects.** The only exception to this rule is the launch configuration, which is named uniquely for deployments after the stack has been created. (Launch configuration names must be unique and cannot overwrite existing launch configurations.) For example, if the ELB was replaced manually for any reason, it is possible the name is different. When the script attempts to remove the ELB, the name would not be an exact match and the script will raise an exception and halt.

That being stated, it is best to replace an entire stack by deleting the current stack first and creating a new stack than to try to replace individual resources from either the console or using the API.

