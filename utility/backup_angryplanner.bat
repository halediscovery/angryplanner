@echo off
for /f "tokens=1-4 delims=/ " %%i in ("%date%") do (
	set dow=%%i
	set month=%%j
	set day=%%k
	set year=%%l
   )
set datestr=%month%_%day%_%year%
echo datestr is %datestr%
    
set DATADUMP_FILE=angryplanner-uat_%datestr%_datadump.json
set POSTGRES_BACKUP_FILE=angryplanner-uat_%datestr%_backup.sql

e:
cd \AngryEncodePlanner\angryplanner.uat\angryplanner
set DJANGO_SETTINGS_MODULE=angryplanner.settings.uat
python e:\AngryEncodePlanner\angryplanner.uat\angryplanner\manage.py dumpdata nld.Plan >> E:\AngryEncodePlanner\backup\current_datadump.json
Ren E:\AngryEncodePlanner\backup\current_datadump.json %BACKUP_FILE%
cd E:\AngryEncodePlanner\backup\

SET PGPASSWORD=postgres
echo on
"c:\Program Files\PostgreSQL\10\bin\pg_dump.exe" -h localhost -p 5432 -U postgres -b -c -v -f %POSTGRES_BACKUP_FILE% "angryplanner-uat"