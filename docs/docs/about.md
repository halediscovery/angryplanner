# About the Angry Planner

The Angry Planner is a stop gap solution for the DCI Operations and Logistics teams.  It replaces the existing excel based tools (called three Encode Planner) that they use today to track finding sources,  encoding, quality checks, and delivery of packages.  See [workflow.md](workflow.md).  It is stop gap since the plan is to move the DCI scheduling, logistics, and operations teams to the Scripps (formally) tool NLCD

## Authors

- Michael Hale [Michael_Hale@discovery.com](mailto://michael_hale@discovery.com)
- Srinivas Kanuparthy [Srinivas_Kanuparthy@discovery.com](Srinivas_Kanuparthy@discovery.com)
- Sai Singamneni [Sai_Singamneni@discovery.com](Sai_Singamneni@discovery.com)

