# Introduction

There is a need to deploy AngryPlanner to docker.  It should end up:

* 3 instances of django app running behind gunicorn/nginx/idontcare
 * load balenced between 3 (8084, 8085, 8086)
 * postgres (5432:5432)
 * one with csv_import running in cron
 * one with pdsconnector running in cron
* static files being served out of nginx (8081)

## Nginx

To build and start the nginx container - the static files are copied to the container

'''bash
docker build -t ap-static-nginx .
docker run --name ap-static-nginx-c -d -p 8081:80 ap-static-nginx

to stop and remove the running container

'''bash
docker container stop ap-static-nginx
docker prune
'''

## Postgresql


docker create -v /var/lib/postgresql/data --name ap-postgres-data-c alpine
docker run -p 5432:5432 --name ap-postgres-server-c -e POSTGRES_PASSWORD=B0Gu5dayz -d --volumes-from ap-postgres-data-c postgres


docker run --name ap-postgres-c -p 5432:5432 -e POSTGRES_PASSWORD=B0Gu5dayz -d postgres

docker cp ap-postgres-server-c:/var/lib/postgresql/data .\Desktop\



## Docker Compose




to clone git

```
git clone git@github.discovery.com:mhale/angryplanner.git
```

this clone the latest github repo



run postgres docker


run django docker cluster