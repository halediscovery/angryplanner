## Database Migration from Sqlite to PostGres

In the beginning of development, the angry planner was started with SQLite as the database.  It worked fine, and certainly didn't have any issues with development.  But, it started to become an issue when Angry Planner was in beta.  The system would get timeouts from time-to-time.  So, the decision was taken to move from SQlite to Postgres.

The first thing we did was dump out the data to a JSON file.  This was done with the proper settings so that the SQLite database was used.  For the beta system, this was as simple as running __utility\set_feedback.bat__.  That configures the two environment variables needed for a working system.

```
manage.py dumpdata nld.Plan > datadump.json
```

The reader will notice that only the nld.Plan data was dumped out.  This was intentional.  None of the other data (users, OKTA stuff, etc) is persistant or can't be replaced quickly.  As it stands, this file is quite large.  Then, the environment was changed to be running on PostGres.  That is as simple as setting DJANGO_ENV_FILE=.../path/to/.developmentenv, then run:

```
manage.py migrate --run-syncdb
```
this makes sure the Postgres database has the proper structure, then:
```
manage.py loaddata datadump.json
```
And you are done.