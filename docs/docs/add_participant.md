models.py

add participant to participant model section
add participant_date to model section

add participant field to get_shipped_fields(cls)
add participant and particpant date to def participants_datefileds(cls)
add participant_date to get_shipped_fields(cls)
add participant to get_participants(cls)
add participant to get_boolean_fields(cls)
add participant_date to get_date_fields(cls)
add csv heading and participant to get_csv_model_map(cls)

>manage.py makemigrations
Migrations for 'nld':
  nld\migrations\0009_auto_20181010_1352.py
    - Add field participant to plan
    - Add field participant_date to plan

>manage.py migrate


admin.py

add participant_date to "Status" section of PlanAdmin
add participant to "Participants" section of PlanAdmin