# Import of the Excel based Schedule


# CSV import logic and improvements

 * assume download of "latest csv file" from amazon
 * assume that we need to specify "the name" of the CSV
 * now that we have "New to Market, Resends, and Archive" - there is work to be done in the excel doc to export those
  * New to Market is content that has never been scheduled and delivered before
  * Resends are either an added participant to content that has already be delivered or an update to metadata (what is in the CSV) for content that has already been shipped
  * Archive is content that has been shipped and generally doesn't change
  * Scheduling starts "New to Market CSV" -> Archive CSV -> if needs modification, it is copied to the Resend CSV and modified
  * all notes below assume that we are iterating through a CSV row by row, and doing something after every row
  * assume all changes in any field mean "Updated Metadata" and assume all changes in participants mean "New Participant"
  * it is the same logic for all 3 CSV files
  * it is all based on propep and comparing everything else
  * we already do this, but just to say: if there is no existing propep, then create new one



things are 
\\dss-isi-t\\MTPO_Transfer\\svc_vod\\XML

Thank you,

---
Two changes need to be flagged to the users of the AngryPlanner

0. If the Metadata complete column is set is the CSV, then set the metadata_complete boolean field to true 

1. Updated Metadata: after content is shipped, there is a change to metadata, so the XML that GXG(Naveen's) generates is new and needs to be manually resent to all participant
 * CSVImport (your script) compares all metadata (**new attribute list**) to the existing record/object in AngryPlanner, and if there are changes, then sets the "Updated metadata" boolean field to true
 * To check whether something has been shipped, "transfer_complete" date field has a value

The attributes to look for changes for Updated Metadata:

 * delivery_date       
 * market_start_date   
 * linear_premiere     
 * mtod_start_date     
 * apple_start_date    
 * propep              
 * asset_name          
 * asset_series        
 * asset_season        
 * asset_season_id     
 * asset_episode       
 * asset_network       
 * container_position  
 * priority_asset      
 * free_episode        
 * promo               
 * total_run_time      
 * asset_rating        
 * not_in_pds          
 * versions_created    
 * c_title_tbd         
 * c_rating_tbd        
 * c_description_tbd   
 * c_other             
 * c_released          
 * c_xml_complete      
 * c_version_comments  
 * c_manual_xml        
 * c_prov_comments     
 * c_tbd               
 * c_shipped           
 * c_pricing           
 * c_territory         
 * c_closedcaptioned   
 * c_impairment        
 * c_quarter           
 * c_charactercount    
 * c_ep_description    
 * c_start_date        
 * c_prop_id1          
 * c_sony              
 * c_apple_genre       
 * c_sony_genre        
 * c_episode_title     
 * c_container_pos1    
 * c_premier_date      
 * c_sale_type1        
 * c_needs_ca_release  
 * c_complete          
 * c_container_id      
 * c_amazon_season_sku 
 * c_amazon_series_sku 
 * c_studio_name       
 * c_series_start      
 * c_season1           
 * c_seasondescript    
 * c_seriesdescript    
 * c_amazon_genres     
 * c_microsoft_genres  
 * c_ASIN              
 * c_blank1            
 * c_disc_formula      
 * c_blank2            
 * c_blank3            
 * c_ema1              
 * c_ema2              
 * c_ema3              
 * c_ema4              
 * c_blank4            
 * c_vendid_form       
 * c_disc_formula1     
 * c_disc_formula2     
 * c_move_inventory    
 * c_amaz_genre1       
 * c_amaz_genre2       
 * c_amaz_genre3       
 * c_amaz_genre4       
 * c_amaz_genre5       
 * c_amaz_date_xml1    
 * c_amaz_date_xml2    
 * c_seson_num         
 * c_plan_status       


2. Added Participant: after content is shipped, there is a change to the participant list, so the content will be shipped/delivered to a new partner. e.g. we already shipped to iTunes, and then Sony decided that they want the content too
 * CSVImport (your script) compares all participants to the existing record/object in AngryPlanner, and if there are changes, then sets the "Added Participant" boolean field to true
 * To check whether something has been shipped, "transfer_complete" date field has a value
**what do we do if a participant has been removed?**
Participant Fields:
 * amazon
 * apple
 * google
 * vudu
 * sony
 * microsoft
 * fandangonow
 * comcastest
 * mtod
 * kaleidescape

If any of these change to true from false, then New Participant boolean is set to true

Other additions to script:

 * add command line option to  specify a file instead of downloading a the latest one (for testing purposes)
 * make a new branch, call it what every you want


## Command Line Options

--f <file.csv>