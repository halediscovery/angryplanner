# MotorTrend OnDemand Requirements


## Todo

Geo Restrictions
- fix 
- 53 combin

- new view for Kaltura data [**Hale** - Done]

- Inventory import – 3500 titles with their existing metadata [**Richardson**]

Inventory from the existing Kaltura repository brought into the AngryPlanner to update the existing records

- Include air dates, both premiere and reruns (Cox rules compliance) [PDS Connector - **Sai** (1)]
- Fields for start and end date (takedowns) [**Hale** (Done)]
- Added filtering to find and update data [**Hale** (Done)]
- Highlight new content [**Sai** (2)]

Filter that shows a what what added in the last week

- Export data for sending to LA to existing excel process [**Sai** (1)]

using django-import-export define a new view that has csv export of the relevant fields
(need example of the existing CSVs that are sent to Kaltura)

- Create fields and import existing geofilters on all known inventory [**Hale** & **Sai** (model is modified, but we need more options)]
(Richardson to supply list of existing restriction options)

- Add regions drop down that matches the Kaltura restriction options and start/end dates for option selected
(Richardson to supply API to update in AngryPlanner[**Sai** (model is modified, but we need more options)])

- Multi-select by season(title) and apply in bulk [**Sai** (2)]
using "admin actions" with a custom form, make mulitiple changes to records in MTOD Scheduler View it will be used for the MTOD start, MTOD End, Geo Start, and GEO end dates

- Highlight changes [**Hale** - need to talk to **Richardson**]

(filter for records that have been updated - but might conflict with regular AngryPlanner usage)

- Filtering updated to include restriction fields to augment export [**Sai** - (3)]

import-export already obeys current filtering in the Admin view, we shouldn't have to do anything past what is above

- External content updates (MTOD titles, racing) [**Richardson**]
(Richardson needs to get data on existing MTOD - we can wait on this - it will be a repeat of the earlier import)

- Publish to Kaltura – metadata, takedowns, media

export Kaltura XML for new records and publish to Kaltura [**Sai** (3)]

- Messaging on new content, restriction expiring, global takedown 

report and filtering to show what is going on and send an email
**Hale** figure out connecting to email server at Discovery
**Sai** generate filtered report (based on Richardson's feedback and email)

- Additional reporting

We know we'll need to do more, but have to wait until we have some usage


## Done

