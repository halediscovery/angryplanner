# Development and Deployment

There are 4 types of deployment:

- Debug: just a local install for a developer to run
  - it uses sqlite
- Feedback:
- Development: used for QA and CAT 
- Production: day to day use

## Development Configuration

```
set_debug_env.bat
manage.py makemigrations
manage.py migrate
manage.py createsuperuser
manage.py runserver 0.0.0.0:80
```

## Feedback Configuration

This is the configuration that was run on the "planner" laptop to test and get feedback from the logistics and operations team.  It is similar to the debug environment, but with different paths and a different set 



## Beta Configuration

set of docker containers hosted on Kubernets

* postgres database
* database persistance machine or stored on local filesystem
* nginx static file server
* blue machine
* green machine
* nginx load balancer



## Production

* same as above



## Documentation

For full documentation visit [mkdocs.org](http://mkdocs.org).

### Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.


### Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

## Git



git checkout seotweaks    # source name
git merge -s ours master  # target name
git checkout master       # target name
git merge seotweaks       # source name

git push -f <remote> <branch>




# Container build

### Setup the volume for the database


### Run the database container

docker run --net angryplanner_apnet --volume angryplanner_db_volume:/var/lib/postgresql/data -e POSTGRES_PASSWORD=784043f91e51aa195737ed80ce368415 --name ap-db-c -p 5432:5432 postgres

* docker run
* --net angryplanner_apnet - connect to the angryplanner_apnet network
* --volume angryplanner_db_volume:/var/lib/postgresql/data
* -e POSTGRES_PASSWORD=784043f91e51aa195737ed80ce368415
* --name ap-db-c
* -p 5432:5432
* postgres


### Backup the database

Connect to running database container and run pg_dumpall

```
docker exec -it ap-db-c pg_dumpall -c -U postgres > dump_angryplanner.sql
```

or do it on the container and copy off the file

```
docker exec -it ap-db-c /bin/bash
pg_dumpall -c -U postgres > dump_angryplanner.sql
ls
exit
```

then in powershell

```
docker cp ap-db-c:/dump_angryplanner.sql
```




### build the planner app container


### do the migration and user setup


### Populate the database from SQLite










Build the planner from the project (angryplanner) directory.  This makes the image, but does not do the 
```
docker build . -f \deployment\Dockerfile.Planner -t ap-planner-c
```

```
docker run --net angryplanner_apnet -e "DJANGO_SETTINGS_MODULE=angryplanner.settings.quickfix" --name ap-planner-sqlite-c -p 81:82 ap-planner-c
```

do the migration
```
docker exec -it ap-planner-c python3 /opt/services/angryplanner/manage.py migrate
```


```
docker-compose run --rm djangoapp /bin/bash -c "cd hello; ./manage.py migrate"
docker run --net angryplanner_apnet -e "DJANGO_SETTINGS_MODULE=angryplanner.settings.development" -e "DJANGO_ENV_FILE=/opt/services/angryplanner/.developmentenv" --name ap-planner-c -p 9090:9090 angryplanner
docker run --net angryplanner_apnet --volume angryplanner_db_volume:/var/lib/postgresql/data -e POSTGRES_PASSWORD=784043f91e51aa195737ed80ce368415 --name ap-db-c -p 5432:5432 postgres
docker run --net angryplanner_apnet --entrypoint /entrypoint.sh -e DJANGO_SETTINGS_MODULE=angryplanner.settings.development -e DJANGO_ENV_FILE=/opt/services/angryplanner/.developmentenv --name ap-planner-c -p 9090:9090 ap-planner-c
docker run --net angryplanner_apnet -e "DJANGO_SETTINGS_MODULE=angryplanner.settings.development" -e "DJANGO_ENV_FILE=/opt/services/angryplanner/.developmentenv" --name ap-planner-c -p 9090:9090 angryplanner
docker build -f Dockerfile.db .
docker build -f Dockerfile.web .
docker build . -f Dockerfile.AngryPlanner -t angryplanner
docker build --net angryplanner_apnet -e "DJANGO_SETTINGS_MODULE=angryplanner.settings.development" -e "DJANGO_ENV_FILE=/opt/services/angryplanner/.developmentenv" --name ap-planner-c -p 9090:9090 . -f Dockerfile.AngryPlanner -t angryplanner
```



### Build the upstream server image

```
docker build . -f deployment\Dockerfile.Upstream -t ap-upstream-c
```

```
docker run --net angryplanner_apnet --name ap-upstream-c -p 80:80 ap-upstream-c
```






























Docker 

"Our goal is everything is now built as a container and run as container"

Code -> Build an image -> run image as a container

docker build


docker images - list images
docker rmi <image_name> - deletes an image

docker ps -a - list containers

docker run <image_name>

docker container rm <container_name> - stop a container
docker container prune - remove all existed containers


Building an image

docker build .
    this will build an image from the current directory with a config call Dockerfile

docker build . -f \deployment\Dockerfile.Planner -t ap-planner-c
this will build an image call ap-planner-c from the current director with a config call Dockerfile.Planner

Running an image

