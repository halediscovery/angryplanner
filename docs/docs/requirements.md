# AngryPlanner Requirements and Design




## CSV Import

see [Schedule Import](/schedule_import.md)


## PDS Connector

see [PDS Connector](/pds_connector.md)




## Okta Integration








---------------------------------------
planner adoption

    Add final CSV XML fields to Data Model (Hale)

    Update from CSV Script
        download the latest CSV from S3 (done)
        update by comparing row short PAID (Prop.Ep)
            need a field list for this (non duplicated CSV headings) (Hale)
        if change check if all delivery dates are null (Sai)
            if not then set "metadata update boolean field on object"
            then update object
        if all are null
            just update object
        needs to be run on :15 and :45 minute mark by Cron or something like that

    Search & Filter (on Expected Delivery date)
        Date Range Filter (Hale merge)
        Search on Specific Date - have date selector in Filter or Search window (Sri)
        Filter on next 7 days, next 14 days, last 7 days, last 14 days (Sai)
        Filter for Participants - shortene list (Hale)
        Filter on NewMetadata Flag (Hale)

    OKTA integration
        meeting with Mike Lee on Tuesday (Hale and Sri)

    Amazon Deployment
        Setup.py - the original Deployment with Stephen Garlic (Sri)
        Set meeting with Garlic and Sri on Thursday

    REST API for Planner Object

    RSS Feed for Planner Object

    XML out
        Button is admin list view to render EMA XML and iTUNES XML (Sai)
        Custom Admin Object Template for Planner (two buttons - one for EMA and one for iTunes)

SDVI
    Need full round of training from Derby's team (would be remote/webcast)


Future State

    NLCD (Scripps tool): scheduling, reporting, XML and Avails out
    SDVI Rally: media encode, stitching, QC, XML Gen, and delivery out
    Focus for now is NLCD direct to DGo with no Rally yet



----------------------

Logistics

filter by date (range - )
network

planner:search:series title
planner:search:episode title



----------
Operations

filter by date (range - next week, 2 weeks)
planner:filters:playlist posted
noplanner:filter:search: transcoded
planner:filter:search: video_quality_check
planner:filter:search: cc_quality_check
noplanner:filter:search: dumpster
noplanner:filter:search: in_queue - xml won't be generated
planner:filter:checkbox - metadata ready - filter

never filter by participant



---
excel export - multiple types




All
update from the current development or master

Mike
add Thomas as a user
configure beta groups
docker/postgres deployment
set conversation with Garlic and figure out deployment
need migration script.sh
change the order of "Statuses" in admin.py


Sai
updated metadata check
resends vs action
mike to set up call on resends
we need to account for the resends � we have a field called Action that we can use here, but we need to get the data from Scheduling
import check for changes in CSV


Sri
adding Logistics and Ops filters so it not more than two button clicks
 - schedule a meeting with Mike to define what this is
add back in datarange filter
we need to add the logic for transfer complete being calculated off of participant and status
 - in models.py
 - on save of a record, we need some logic to check status against participent and set transfer complete
add motortrend date to models.py
change transfer comptete to readonly and a boolean


Low priority items:

figuring out sending an email from the interface
date order in filter - low priority
remove hidden section from admin.py
xml output


# combine the non-linear scheduling workflow for Scripps and Discovery

# who are the teams?  who is doing tech enablement?


# what is the tech enablement?


HIVE -> eRepository -> NLCD


first "new content" workflow
second "library" workflow
    need to clean up dataset 

what can be automated
    if the content is "known", then the source is known, then the encode can happen automatically
    HIVE feeds eRepo' we're good



phases vs tech stacks vs spines drawing from Tania

need to figure out rights checking and integration







